Event	ID	Name			Wwise Object Path	Notes
	46624002	KeynoteAmbience			\SFX\Ambience\KeynoteAmbience\KeynoteAmbience	
	186852181	Thunder			\SFX\Ambience\KeynoteAmbience\Thunder	

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	59390941	KeynoteAmbiencev2	Y:\Documents\GitHub\graduationgame\graduationgame_WwiseProject\.cache\Android\SFX\KeynoteAmbiencev2_C70A37D7.wem		\Actor-Mixer Hierarchy\Ambience\KeynoteAmbience\KeynoteAmbience\KeynoteAmbiencev2		2415379
	398435968	OutsideRain	Y:\Documents\GitHub\graduationgame\graduationgame_WwiseProject\.cache\Android\SFX\OutsideRain_9603DD45.wem		\Actor-Mixer Hierarchy\Ambience\KeynoteAmbience\KeynoteAmbience\OutsideRain		331417
	441620050	Weather, Thunder Clap, Heavy Impact, Gritty, Distant 5 SND12926 1	Y:\Documents\GitHub\graduationgame\graduationgame_WwiseProject\.cache\Android\SFX\Signal Sounds\Weather, Thunder Clap, Heavy Impact, Gritty, Distant 5 SND12926 1_E865697D.wem		\Actor-Mixer Hierarchy\Ambience\SignalSounds\Thunder\Weather, Thunder Clap, Heavy Impact, Gritty, Distant 5 SND12926 1		26818
	555548654	Weather, Thunder Clap, Heavy Impact, Gritty, Distant 3 SND12928 1	Y:\Documents\GitHub\graduationgame\graduationgame_WwiseProject\.cache\Android\SFX\Signal Sounds\Weather, Thunder Clap, Heavy Impact, Gritty, Distant 3 SND12928 1_1F8EBEA4.wem		\Actor-Mixer Hierarchy\Ambience\SignalSounds\Thunder\Weather, Thunder Clap, Heavy Impact, Gritty, Distant 3 SND12928 1		36082
	985434312	Weather, Thunder Clap, Heavy Impact, Dark, Gritty 1 SND12975 1	Y:\Documents\GitHub\graduationgame\graduationgame_WwiseProject\.cache\Android\SFX\Signal Sounds\Weather, Thunder Clap, Heavy Impact, Dark, Gritty 1 SND12975 1_D1207F87.wem		\Actor-Mixer Hierarchy\Ambience\SignalSounds\Thunder\Weather, Thunder Clap, Heavy Impact, Dark, Gritty 1 SND12975 1		53664

