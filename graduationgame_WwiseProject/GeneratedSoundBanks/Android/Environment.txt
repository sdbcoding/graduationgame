Event	ID	Name			Wwise Object Path	Notes
	758800342	SlitherLoop			\SFX\Ambience\Environment\SlitherLoop	
	3317362560	FireplaceLoop			\SFX\Ambience\Environment\FireplaceLoop	
	4224039081	GrandfatherClockLoop			\SFX\Ambience\Environment\GrandfatherClockLoop	

Effect plug-ins	ID	Name	Type				Notes
	2891462749	Room_Medium_High_Absorbtion	Wwise RoomVerb			

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	328153735	FireplaceLoop	Y:\Documents\GitHub\graduationgame\graduationgame_WwiseProject\.cache\Android\SFX\FireplaceLoop_9603DD45.wem		\Actor-Mixer Hierarchy\Environment\EnvironmentLoops\FireplaceLoop		640948
	722763810	Slither	Y:\Documents\GitHub\graduationgame\graduationgame_WwiseProject\.cache\Android\SFX\Slither_9603DD45.wem		\Actor-Mixer Hierarchy\PlayerCharacter\Takedown\TakedownV2\Slither		1911163
	802964490	GrandfatherClockLoop	Y:\Documents\GitHub\graduationgame\graduationgame_WwiseProject\.cache\Android\SFX\GrandfatherClockLoop_9603DD45.wem		\Actor-Mixer Hierarchy\Environment\EnvironmentLoops\GrandfatherClockLoop		101380

