﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FadeOutController : MonoBehaviour {

	public bool fadeOutActive;
	float alpha = 1.0f;
	public float fadeSpeed = 0.1f;
	[SerializeField]
	GameEvent fadeOutEvent;
	
	void Start () 
	{
		fadeOutActive = false;
	}
	
	void Update () 
	{
		if( fadeOutActive )
		{
			FadeOut();
		}
	}

	void FadeOut()
	{
		//Debug.Log("fading out");
		alpha -= fadeSpeed * Time.deltaTime;  
		foreach (Image e in GetComponentsInChildren<Image>() )
		{
			if( e.color.a > alpha )
				e.color = new Color(e.color.r, e.color.g, e.color.b, alpha);
		}

		foreach (Text e in GetComponentsInChildren<Text>() )
		{
			if( e.color.a > alpha )
				e.color = new Color(e.color.r, e.color.g, e.color.b, alpha);
		}		

		if ( alpha <= 0 )
		{
            //SceneManager.UnloadScene( SceneManager.GetSceneAt(0).name );
			fadeOutEvent.Raise();
            gameObject.SetActive(false);
            fadeOutActive = false;
            alpha = 1.0f;

            foreach (Image e in GetComponentsInChildren<Image>())
            {
                if (e.name == "Background")
                {
                    continue;
                }

                e.color = new Color(e.color.r, e.color.g, e.color.b, alpha);
            }

            foreach (Text e in GetComponentsInChildren<Text>())
            {
                e.color = new Color(e.color.r, e.color.g, e.color.b, alpha);
            }
        }
	}

	public void StartFadeOut()
	{
		fadeOutActive = true;
	}
}
