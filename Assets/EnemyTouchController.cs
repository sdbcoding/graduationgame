﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTouchController : MonoBehaviour {

	
    int layer_mask;
	[SerializeField]
	GameEvent OnEnemyTouched;
	[SerializeField]
	GameObjectVariable publicEnemy;

    void Awake()
    {
        layer_mask = LayerMask.GetMask("Enemy");
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
        foreach (Touch touch in Input.touches)
        {

            Ray ray = Camera.main.ScreenPointToRay(touch.position);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, layer_mask))
            {

                publicEnemy.Value = hit.transform.gameObject;
                OnEnemyTouched.Raise();
                
            }

        }

		
	}
}
