﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionUIController : MonoBehaviour 
{

	[SerializeField]
	ThingSet killableEnemies;
	[SerializeField]
	GameObject actionButton;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if( killableEnemies.GetItems().Count == 0 )
		{
			actionButton.GetComponent<Image>().color = Color.white;
			actionButton.GetComponentInChildren<Text>().text = "Dash";
		}
		else
		{
			actionButton.GetComponent<Image>().color = Color.green;
			actionButton.GetComponentInChildren<Text>().text = "Kill";
		}
	}
}
