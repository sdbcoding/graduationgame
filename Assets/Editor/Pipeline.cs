using System;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEditor;
using UnityEditor.Build.Reporting;

namespace Mindhunter {
	public class Pipeline {
		[MenuItem("Pipeline/Build: Android")]
		public static void BuildAndroid() {

			/*
			 * Add test functionality here. Remember that this is called at editors
			 * runtime, so you might have to use some test class mockups or keep it
			 * in mind when designing your game. If any of tests fails, you need to
			 * exit Unity with a non-zero return code so that Jenkins stop building
			 *
			 * EditorApplication.Exit(1);
			 *
			 */
			 Debug.Log("Running");
			Directory.CreateDirectory(Pathname);
			var result = BuildPipeline.BuildPlayer(new BuildPlayerOptions {
				locationPathName = Path.Combine(Pathname, Filename),
				scenes = EditorBuildSettings.scenes.Where(n => n.enabled).Select(n => n.path).ToArray(),
				target = BuildTarget.Android
			});

			Debug.Log(result);
			/*
			if(result == BuildResult.Succeeded) {
				EditorApplication.Exit(0);
			}
			else {
				EditorApplication.Exit(1);
			}
			*/
		}

		/*
		* This is a static property which will return a string, representing a
		* build folder on the desktop. This does not create the folder when it
		* doesn't exists, it simply returns a suggested path. It is put on the
		* desktop, so it's easier to find but you can change the string to any
		* path really. Path combine is used, for better cross platform support
		*/
		public static string Pathname {
			get {
				int buildPathIndex = Array.IndexOf(Environment.GetCommandLineArgs(), "-mindhunterBuildPath");
				Debug.Log(Environment.GetCommandLineArgs()[buildPathIndex + 1]);
				return "C:\\Users\\dadiu\\Dropbox\\DADIU_Team4\\030_GG\\090_pipeline\\" + Environment.GetCommandLineArgs()[buildPathIndex + 1] + "\\";				
			}
		}

		/*
		* This returns the filename that the build should spit it. For a start
		* this just returns a current date, in a simple lexicographical format
		* with the apk extension appended. Later on, you can customize this to
		* include more information, such as last person to commit, what branch
		* were used, version of the game, or what git-hash the game were using
		*/
		public static string Filename {
			get {
				int buildPathIndex = Array.IndexOf(Environment.GetCommandLineArgs(), "-mindhunterCommit");
				Debug.Log(Environment.GetCommandLineArgs()[buildPathIndex + 1]);
				return (Environment.GetCommandLineArgs()[buildPathIndex + 1] + ".apk");
			}
		}
	}
}
