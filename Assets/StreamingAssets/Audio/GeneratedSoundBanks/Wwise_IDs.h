/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID ACTIVATECHAIR = 591379601U;
        static const AkUniqueID ACTIVATECUPBOARDPLATES = 1281586749U;
        static const AkUniqueID ACTIVATEDOOR = 2118065522U;
        static const AkUniqueID ACTIVATEFIREPLACE = 1815832539U;
        static const AkUniqueID ACTIVATEGRANDFATHERCLOCK = 4166361112U;
        static const AkUniqueID ACTIVATEPIANO = 3893844633U;
        static const AkUniqueID ACTIVATETABLEBOTTLE = 2667821090U;
        static const AkUniqueID ACTIVATETABLEGLASSES = 2573548510U;
        static const AkUniqueID COMBATMUSIC = 3733692670U;
        static const AkUniqueID EDGEOFAGGELOS = 3497246341U;
        static const AkUniqueID ELDRITCHPRESENCE = 3075401749U;
        static const AkUniqueID EXPLOREMUSIC = 2687048523U;
        static const AkUniqueID FIREPLACELOOP = 3317362560U;
        static const AkUniqueID GAMEOBJECTPAUSEALL = 1136721469U;
        static const AkUniqueID GAMEOBJECTRESUMEALL = 699957356U;
        static const AkUniqueID GAMEOBJECTSTOPALL = 1537172403U;
        static const AkUniqueID GRANDFATHERCLOCKLOOP = 4224039081U;
        static const AkUniqueID KEYNOTEAMBIENCE = 46624002U;
        static const AkUniqueID MENUMUSIC = 679636833U;
        static const AkUniqueID NPCFSMEDIUM = 114952126U;
        static const AkUniqueID NPCSCARE = 3008586474U;
        static const AkUniqueID NPCSEARCH = 3992001690U;
        static const AkUniqueID NPCTERRORLEVEL = 2512247594U;
        static const AkUniqueID PAUSEALL = 4091047182U;
        static const AkUniqueID PAUSEELDRITCHPRESENCE = 730096693U;
        static const AkUniqueID PAUSEMENUMUSIC = 2478971905U;
        static const AkUniqueID PCDEATH = 183865528U;
        static const AkUniqueID PCSTOPTAKEDOWNBUILDUP = 1471721702U;
        static const AkUniqueID PCTAKEDOWNBUILDUP = 2459381774U;
        static const AkUniqueID PCTAKEDOWNBUILDUP2 = 3523257912U;
        static const AkUniqueID PCTAKEDOWNBUILDUP3 = 3523257913U;
        static const AkUniqueID PCTAKEDOWNFINISH = 1033186554U;
        static const AkUniqueID PCTERRORLEVEL = 542172134U;
        static const AkUniqueID PCWIN = 3012753236U;
        static const AkUniqueID PLAYTRAILERAUDIO = 2986554774U;
        static const AkUniqueID PUFF = 2076776060U;
        static const AkUniqueID RESUMEALL = 3240900869U;
        static const AkUniqueID RESUMEELDRITCHPRESENCE = 470801948U;
        static const AkUniqueID RESUMEMENUMUSIC = 2226500770U;
        static const AkUniqueID SLITHERLOOP = 758800342U;
        static const AkUniqueID STOPELDRITCHPRESENCE = 483455629U;
        static const AkUniqueID STOPMENUMUSIC = 1973898857U;
        static const AkUniqueID STOPNPCTERRORLEVEL = 4050076226U;
        static const AkUniqueID THUNDER = 186852181U;
        static const AkUniqueID UICLICKBACK = 2807303240U;
        static const AkUniqueID UICLICKFORWARD = 4101637124U;
        static const AkUniqueID UINOCLICK = 1403571118U;
        static const AkUniqueID UIQUITGAME = 2104332644U;
        static const AkUniqueID UISTARTGAME = 3230769895U;
        static const AkUniqueID WHOOSH = 1323846727U;
        static const AkUniqueID WHOOSHSTOP = 687955623U;
        static const AkUniqueID WORMMOUTHKILL = 2056691465U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace PCSTATE
        {
            static const AkUniqueID GROUP = 651748789U;

            namespace STATE
            {
                static const AkUniqueID COMBAT = 2764240573U;
                static const AkUniqueID DEATH = 779278001U;
                static const AkUniqueID EXPLORE = 579523862U;
                static const AkUniqueID WIN = 979765101U;
            } // namespace STATE
        } // namespace PCSTATE

    } // namespace STATES

    namespace SWITCHES
    {
        namespace MATERIAL
        {
            static const AkUniqueID GROUP = 3865314626U;

            namespace SWITCH
            {
                static const AkUniqueID CARPET = 2412606308U;
                static const AkUniqueID WOOD = 2058049674U;
            } // namespace SWITCH
        } // namespace MATERIAL

        namespace NPCTERRORLEVEL
        {
            static const AkUniqueID GROUP = 2512247594U;

            namespace SWITCH
            {
                static const AkUniqueID ALARMED = 1296396487U;
                static const AkUniqueID ALARMEDTOSCARED = 283250744U;
                static const AkUniqueID BROKEN = 231230354U;
                static const AkUniqueID NORMAL = 1160234136U;
                static const AkUniqueID NORMALTOALARMED = 558881045U;
                static const AkUniqueID SCARED = 820940617U;
                static const AkUniqueID SCAREDTOBROKEN = 2594764549U;
            } // namespace SWITCH
        } // namespace NPCTERRORLEVEL

        namespace TERRORLEVEL
        {
            static const AkUniqueID GROUP = 676776959U;

            namespace SWITCH
            {
                static const AkUniqueID ALARMED = 1296396487U;
                static const AkUniqueID ALARMEDTOSCARED = 283250744U;
                static const AkUniqueID BROKEN = 231230354U;
                static const AkUniqueID NORMAL = 1160234136U;
                static const AkUniqueID NORMALTOALARMED = 558881045U;
                static const AkUniqueID SCARED = 820940617U;
                static const AkUniqueID SCAREDTOBROKEN = 2594764549U;
            } // namespace SWITCH
        } // namespace TERRORLEVEL

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID RTPCMUSICVOLUME = 3943409783U;
        static const AkUniqueID RTPCSFXVOLUME = 272358683U;
        static const AkUniqueID RTPCTAKEDOWNBUILDUP = 2153737672U;
        static const AkUniqueID RTPCTERRORLEVEL = 1959656992U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID ACTIVATIONNOISES = 1008149224U;
        static const AkUniqueID CINEMATICS = 877642339U;
        static const AkUniqueID ENVIRONMENT = 1229948536U;
        static const AkUniqueID GLOBAL = 1465331116U;
        static const AkUniqueID INTERACTIVEMUSIC = 2279279248U;
        static const AkUniqueID KEYNOTEAMBIENCE = 46624002U;
        static const AkUniqueID MENUMUSIC = 679636833U;
        static const AkUniqueID NPCFEEDBACK = 75783175U;
        static const AkUniqueID NPCLOCOMOTION = 1217116541U;
        static const AkUniqueID PCFEEDBACK = 193461595U;
        static const AkUniqueID PCLOCOMOTION = 858107329U;
        static const AkUniqueID UIMENU = 1846620334U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID ACTIVATIONNOISES = 1008149224U;
        static const AkUniqueID AMBIENCE = 85412153U;
        static const AkUniqueID ENVIRONMENTLOOPS = 3369188037U;
        static const AkUniqueID INTERACTIVEMUSIC = 2279279248U;
        static const AkUniqueID KEYNOTEAMBIENCE = 46624002U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MENUMUSIC = 679636833U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID NPC = 662417162U;
        static const AkUniqueID NPCFEEDBACK = 75783175U;
        static const AkUniqueID NPCLOCOMOTION = 1217116541U;
        static const AkUniqueID PCLOCOMOTION = 858107329U;
        static const AkUniqueID PLAYERCHARACTER = 592691923U;
        static const AkUniqueID SFX = 393239870U;
        static const AkUniqueID STINGERS = 2940432316U;
        static const AkUniqueID TAKEDOWN = 2577812776U;
        static const AkUniqueID UI = 1551306167U;
    } // namespace BUSSES

    namespace AUX_BUSSES
    {
        static const AkUniqueID DISTANCEREVERB = 2334881412U;
    } // namespace AUX_BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
