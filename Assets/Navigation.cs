﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Navigation : MonoBehaviour {

	NavMeshAgent navMeshAgent;

	public FloatReference MaxSpeed;

	void Start () {
		navMeshAgent = GetComponent<NavMeshAgent>();		
	}
	
	void Update () {
		Vector3[] path = navMeshAgent.path.corners;
		var currentPos = gameObject.transform.position;
		Debug.DrawRay(currentPos, path[0], Color.red, 1, false);
	}
}
