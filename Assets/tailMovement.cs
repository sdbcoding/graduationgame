﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tailMovement : MonoBehaviour {

    public SpringJoint Spring;
    public Rigidbody rb;

	// Use this for initialization
	void Start () {

        Spring = GetComponent<SpringJoint>();
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        // Spring.spring =50000* (1+Mathf.Sin(Time.time));
        //rb.drag =40 * (1 + Mathf.Sin(Time.time));

        if (Mathf.Sin(Time.time*10) < -0.7)
        {
            Spring.spring = 500;
            rb.drag = 10;
        }
        else
        {
            Spring.spring = 5000;
            rb.drag = 40;
        }
    }
}
