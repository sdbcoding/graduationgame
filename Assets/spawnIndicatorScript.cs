﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnIndicatorScript : OnUpdate
{
    public bool shouldBeRemoved;

    public override void UpdateEventRaised()
    {
      
        transform.Translate(0, Mathf.Sin(Time.time*2)*0.008f,0);

        if (shouldBeRemoved)
        {
            // return;
            transform.Rotate(0, 3, 0);
            transform.localScale *= 0.95f;
            if (transform.localScale.x < 10)
            {
                gameObject.SetActive(false);
            }
        }

	}

    public void Remove()
    {
        shouldBeRemoved = true;
        // Destroy(gameObject);
      
    }
}
