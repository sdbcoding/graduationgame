﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictimHeadAnimatorControl : MonoBehaviour {

    Animator anim;
    public float percentageCompleted = 0;
    private bool animatin = false;
   

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        anim.SetBool("startanim", false);
       // anim.SetFloat("percentage", percentageCompleted);
    }
	
	public void startAnimation()
    {
        if (animatin)
            transform.localEulerAngles = new Vector3(26.4f, 23f, 19.5f);
        anim.SetBool("startanim", true);
        anim.SetFloat("percentage", percentageCompleted);
        animatin = true;
    }

    public void endAnimation()
    {
        animatin = false;
        transform.localEulerAngles = new Vector3(-6.505f, -90f, -38.158f);
        anim.SetBool("startanim", false);
        anim.SetFloat("percentage", 0);
    }



}
