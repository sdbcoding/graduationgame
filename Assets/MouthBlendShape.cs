﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouthBlendShape : MonoBehaviour {

    public SkinnedMeshRenderer smr1;
    public SkinnedMeshRenderer smr2;
    public GameObject g1;
    public GameObject g2;
    public GameObject g3;
    public GameObject g4;
    public GameObject g5;
    public GameObject g6;
    public Vector3Variable pOIPos;

    private void OnEnable()
    {

        pOIPos.Value = transform.position;
        pOIPos.Raise();
   
       
       
        GameObject temp5 = Instantiate(g5, transform.position, Quaternion.identity);  
        temp5.GetComponent<Rigidbody>().AddForce(new Vector3(60, Random.Range(140f, 180f), 70));
        GameObject temp6 = Instantiate(g6, transform.position, Quaternion.identity);  
        temp6.GetComponent<Rigidbody>().AddForce(new Vector3(-100, Random.Range(140f, 180f), -80));
    }
        // Update is called once per frame
        void Update () {


        if ( smr1.GetBlendShapeWeight(0) <= 100)
        {
            smr1.SetBlendShapeWeight(0, smr1.GetBlendShapeWeight(0) + 1.5f);
            smr2.SetBlendShapeWeight(0, smr2.GetBlendShapeWeight(0) + 1.5f);

        }

        if(smr1.GetBlendShapeWeight(0) == 9)
        {
            GameObject temp1 = Instantiate(g1, transform.position, Quaternion.identity);
            temp1.GetComponent<Rigidbody>().AddForce(new Vector3(60, Random.Range(140f, 180f), -100));
            pOIPos.Value = transform.position;
            pOIPos.Raise();
        }

        if (smr1.GetBlendShapeWeight(0) == 30)
        {
            GameObject temp2 = Instantiate(g2, transform.position, Quaternion.identity);
            temp2.GetComponent<Rigidbody>().AddForce(new Vector3(-70, Random.Range(140f, 180f), -100));
            pOIPos.Value = transform.position;
            pOIPos.Raise();
        }

        if (smr1.GetBlendShapeWeight(0) == 60)
        {
            GameObject temp3 = Instantiate(g3, transform.position, Quaternion.identity);
            temp3.GetComponent<Rigidbody>().AddForce(new Vector3(100, Random.Range(140f, 180f), 80));
            pOIPos.Value = transform.position;
            pOIPos.Raise();
        }

        if (smr1.GetBlendShapeWeight(0) == 90)
        {
            GameObject temp4 = Instantiate(g4, transform.position, Quaternion.identity);
            temp4.GetComponent<Rigidbody>().AddForce(new Vector3(-70, Random.Range(140f, 180f), 80));
            pOIPos.Raise();
        }

    }
}
