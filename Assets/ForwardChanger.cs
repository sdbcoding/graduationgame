﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ForwardChanger : MonoBehaviour {
	[Range(1,10)]
	public float distance = 3;
	public Slider slider;

	public Camera cameraObject;
	
	
	// Update is called once per frame
	void Update () {
		cameraObject.GetComponent<Camera>().orthographicSize = distance;
	}
	public void UpdateSlider()
	{
		distance = slider.value;
	}
}
