﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinPanelController : MonoBehaviour {

	public GameObject[] objects;
	public float timerToReach = 1;
	float currentTime;
	
	
	// Update is called once per frame
	void Update () {
		currentTime += Time.deltaTime;
		if (currentTime> timerToReach)
		{
			ShowAllObjects();
		}
	}
	void ShowAllObjects()
	{
		for (int i = 0; i < objects.Length; i++)
		{
			objects[i].SetActive(true);
		}
	}
}
