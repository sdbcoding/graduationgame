﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Unlit/Diffuse"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_TintColor("Color Tint", Color) = (1,1,1,1)
		_Attenuation("Attenuation", Range(0,2)) = 1




	}
		SubShader
		{
			 Tags { "LightMode" = "ForwardBase" }
			LOD 100

			
			


			Pass
			{
				CGPROGRAM

				#pragma vertex vert
				#pragma fragment frag
				#pragma fullforwardshadows
				#include "UnityCG.cginc"
				#include "Flow.cginc"
				#include "Random.cginc"
				  #pragma multi_compile_fwdbase 

				sampler2D _MainTex;
				float4 _MainTex_ST;
				uniform float4 _LightColor0;
				float4 _TintColor;
				float _Attenuation;


				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
					float3 normals : NORMAL;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					float4 vertex : SV_POSITION;
					float4 color1:  TEXCOORD3;
					float4 color2:  TEXCOORD2;
					float3 normalDir :TEXCOORD1;

				};


				v2f vert(appdata v)
				{
					v2f o;

					float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
					float3 normalDir = normalize(mul(float4(v.normals, 0.0), unity_WorldToObject).xyz);//

					float3 lightPos1 = float3(unity_4LightPosX0.x - worldPos.x, unity_4LightPosY0.x - worldPos.y, unity_4LightPosZ0.x - worldPos.z);
					float3 lightPos2 = float3(unity_4LightPosX0.y - worldPos.x, unity_4LightPosY0.y - worldPos.y, unity_4LightPosZ0.y - worldPos.z);


					float3 lightDir1 = normalize(lightPos1);
					float3 diffuseRef1 = max(0, dot(normalDir, lightDir1));
					float3 diffuseRef1c = max(0, dot(normalDir, lightDir1))*unity_LightColor[0];

					float3 lightDir2 = normalize(lightPos2);
					float3 diffuseRef2 = max(0, dot(normalDir, lightDir2));
					float3 diffuseRef2c = max(0, dot(normalDir, lightDir2))*unity_LightColor[1];

					float3 diffuseRefInverted =  ((1 - diffuseRef1)* (1 - diffuseRef2))*_TintColor;

					o.color1 = float4(diffuseRef1 + diffuseRef2,1);
					float3 combAdded =  diffuseRef1c + diffuseRef2c+ diffuseRefInverted;
					
					o.color2 =  float4(combAdded,1);



					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = TRANSFORM_TEX(v.uv, _MainTex);



					return o;
				}

				fixed4 frag(v2f i) : SV_Target//
				{


					fixed4 colstd = tex2D(_MainTex, i.uv);


				return (i.color1*colstd)+i.color2;

				}
				ENDCG
			}

		}

}
