﻿Shader "Unlit/WarpedPerlinInq"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
		SubShader
	{
		Tags { "RenderType" = "Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"
		  
			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				
				return o;
			}
			float2 powC(float2 Z, float2 W)
			{
				float arg = atan(Z.y / Z.x);
				float a = exp(-W.y*arg)*pow(dot(Z, Z), W.x / 4.0); //wihtout scaling animation
				float b = W.x*arg + 0.5*W.y*log(dot(Z, Z));

				return a * float2(cos(b), sin(b));
			}

			float2 cmult(float2 q1, float2 q2) {
				return float2
				(q1.x * q2.x - q1.y * q2.y
					, q1.x * q2.y + q1.y * q2.x
				);
			}

			float smoke(float2 uv, float param)
			{
				float frac = 1.0;
				float2 z = float2(0.0, 0.0);
				float2 c = uv;

				float timefact = cos(_Time + param * 0.001);
				float2 constant = (powC(cos(c), float2(33.8 - param * 0.01, 10.8)));
				for (int i = 0; i < 20; i++)
				{
		
					z = cmult(sin(z - c), cos(z*z + float2(param, param)) - c) + timefact * 1.0*z+ constant;

					if (length(z) > 2.0)
					{
						frac = float(i) / 20.0;
						break;
					}
				}

				return frac;
			}


			
			fixed4 frag (v2f i) : SV_Target
			{
				 float2 uv = float2(i.uv.x,i.uv.y);
				uv -= float2(0.5,0.5);
	
				float smokes = 0.0;
				for (int i = 0; i < 15; i++)
				{
				smokes += (smoke(uv,float(i)*30.0))*(1.0 / float(15));
					}

				 float4 fragColor =float4(1.0, 1.0, 1.0, 1.0)*smokes;
				return fragColor*0.3;
			}
			ENDCG
		}
	}
}
