﻿Shader "Unlit/transparencyShaderRadius"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_NoiseTex("Noise Texture", 2D) = "white" {}
		_VoidTex("void Texture", 2D) = "white" {}
		_Radius("Radius", Range(0,10)) = 5
			_Attenuation("Attenuation", Range(0,2)) = 1


		
	}
	SubShader
	{
		 Tags { "IgnoreProjector" = "True" }
		LOD 100

		
		
	

		Pass
		{
			CGPROGRAM
		
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			#include "Flow.cginc"
			#include "Random.cginc"

			sampler2D _MainTex;
			sampler2D _NoiseTex;
			sampler2D _VoidTex;
			float4 _MainTex_ST;
			float4 _NoiseTex_ST;
			float4 _VoidTex_ST;
			float3 _Position; // from scrip
			float _RadiusMod; // from scrip
			float _Radius;
			float _Attenuation;


			uniform float4 _LightColor0;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normals : NORMAL;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
				fixed4 color2 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
			

			};

		
			
			v2f vert (appdata v)
			{
				v2f o;
				//diffuse light
				float3 normalDir = normalize(mul(float4(v.normals, 0.0), unity_WorldToObject).xyz);
				float3 lightDir = normalize(_WorldSpaceLightPos0);
				float3 diffuseRef =  dot(normalDir, lightDir);


				//distortion

				float3 random = rand(mul(unity_ObjectToWorld, v.vertex).xyz);
				float random4 = random.x;

				
				float _radius = (_Radius + random4)*(1 - _RadiusMod);

				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;

				float distanceWorld = distance(_Position, o.worldPos);

				float3 sphere = saturate(distanceWorld / _radius);

				v.vertex += float4(v.normals, 1)*(random4*(1-float4(sphere,1)));

				o.vertex = UnityObjectToClipPos(v.vertex);

				
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				
				o.color =float4(sphere,sphere.x);
				o.color2= float4(diffuseRef,0.0);


				
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{

				fixed4 colstd = tex2D(_MainTex, i.uv);
				fixed4 coldVoid = tex2D(_VoidTex, i.uv);
		
			float distToCam = distance(_Position, _WorldSpaceCameraPos) ;
			float distToVertex = distance(i.worldPos, _WorldSpaceCameraPos);

			float3 random = rand(i.worldPos);
			float random4 = random.x; 


				float4 col1 = colstd *smoothstep(0.5, 0.6, i.color);
				float4 col2 = (coldVoid -float4(0,0,0,0.5)) * smoothstep(0.6, 0.5, i.color);

				
				float4 col3 = float4(1, 1, 1, 1) * smoothstep(0.5, 0.45, i.color);
				float4 col4 = float4(1, 0, 0, 1) * smoothstep(0.65, 0.45, i.color);


				float4 colRadius = (col1+col2+(col4-col3)); // i.color2 is diffuse light

				//"square based"
				//float4 square1 = colstd *  smoothstep(0.01, 0.25 ,(max(_Position.x, i.worldPos.x) - _Position.x ));
				//float4 square2 = colstd *  smoothstep(0.01, 0.25 ,(max(_Position.z, i.worldPos.z) - _Position.z ));

				//float4 square3 = colRadius *  smoothstep(0.25, 0.01, (max(_Position.x, i.worldPos.x) - _Position.x ));
				//float4 square4 = colRadius *  smoothstep(0.25, 0.01, (max(_Position.z, i.worldPos.z) - _Position.z ));
				
				
				//Depth based
				//float4 colTrans1 = float4(1.0, 1.0, 1.0, 1.0) * smoothstep(0.001,0.9,(max(distToCam, distToVertex) - distToCam));
				//float4 colTrans2 = colRadius * smoothstep(0.9, 0.001, (max(distToCam, distToVertex) - distToCam));

				
				return colRadius* float4(_Attenuation, _Attenuation, _Attenuation,1);// (square1 + square2 + square3 + square4)*(i.color2 + float4(0, 0, 0, 1));

			}
			ENDCG
		}
		
	}
	
}
