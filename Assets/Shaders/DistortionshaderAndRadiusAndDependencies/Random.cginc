﻿#if !defined(RANDOM_INCLUDED)
#define RANDOM_INCLUDED

float rand(float3 co)
{
	return frac(sin(dot(co.xyz, float3(12.9898, 78.233, 45.5432))) * 43758.5453);
}

#endif






