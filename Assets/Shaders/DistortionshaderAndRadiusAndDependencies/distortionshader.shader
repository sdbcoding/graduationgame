﻿Shader "Unlit/distortionshader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_NoiseTex("Noise Texture", 2D) = "white" {}
		_Contrast("Contrast", Range(0,1)) = 0.2

	}
		SubShader
	{
		Tags { "RenderType" = "Opaque"  }
		LOD 100

		Pass
		{
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			#include "Flow.cginc"

			sampler2D _MainTex;
			sampler2D _NoiseTex;
			float4 _MainTex_ST;
			float4 _NoiseTex_ST;
			float3 _Position; // from scrip
			float _Contrast;

			uniform float4 _LightColor0;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normals : NORMAL;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
				fixed4 color2 : TEXCOORD1;

			};



			v2f vert(appdata v)
			{
				v2f o;
				//diffuse
				float3 normalDir = normalize(mul(float4(v.normals, 0.0), unity_WorldToObject).xyz);
				float3 lightDir = normalize(_WorldSpaceLightPos0);
				float3 diffuseRef = dot(normalDir, lightDir);
				//distortion
			
				o.vertex = UnityObjectToClipPos(v.vertex);

				o.uv = TRANSFORM_TEX(v.uv, _MainTex);

				o.color2 = float4(diffuseRef,0.0);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				float2 flowVector = tex2D(_NoiseTex, i.uv).rg * 2 - 1;
				float noise = tex2D(_NoiseTex, i.uv).a;
				float time = _Time.y/6 + noise;
				float3 uvw = FlowUV(i.uv, flowVector,time);
				fixed4 c = tex2D(_NoiseTex, i.uv);
				fixed4 coldis = tex2D(_MainTex, uvw.xy)*uvw.z;
				fixed4 colstd = tex2D(_MainTex, i.uv);



				return coldis-float4(_Contrast, _Contrast, _Contrast,0);
				//return float4(flowVector,0.0,0.0);
				//return col;
				}
				ENDCG
			}

	}

}
