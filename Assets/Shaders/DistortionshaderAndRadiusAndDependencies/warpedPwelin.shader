﻿Shader "Unlit/warpedPwelin"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
		
			
			#include "UnityCG.cginc"

			float2x2 _mat = float2x2(0.8, 0.6, -0.6, 0.8);

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
			
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv =v.uv;
				
				return o;
			}

			float noise(in float2 x)
			{
				return sin(1.5*x.x)*sin(1.5*x.y);
			}

			float fbm(float2 p)
			{
				float f = 0.0;
				f += 0.5000*noise(p);

				p = mul(p,_mat)*2.02;

				f += 0.2500*noise(p); 

				p = mul(p,_mat)*2.03;

				f += 0.1250*noise(p); 

				p = mul(p,_mat)*2.01;

				f += 0.0625*noise(p);

				return f / 0.9375;
			}

			float pattern(in float2 p, out float2 q, out float2 r)
			{
				q.x = fbm(p * sin(_Time / 30.0) + float2(0.0, 0.0));
				q.y = fbm(p + float2(5.2, 1.3));

				r.x = fbm(p * cos(_Time / 4.0) + 4.0*q + float2(1.7, 9.2));
				r.y = fbm(p + 4.0*q + float2(8.3, 2.8));

				return fbm(p + 4.0*r);
			}



			
			fixed4 frag (v2f i) : SV_Target
			{

				//fixed4 col = tex2D(_MainTex, i.uv);

			

			float2 p = float2(i.uv.x,i.uv.y);
				
			float2 q;
			float2 r;
			
			float c = pattern(p*3.5, q, r);
			float3 col = float3(1.0,1.0,1.0);

			col = lerp(col, float3(0.8, 0.2, 0.9), length(q) );
			col = lerp(col, float3(0.3, 0.4, 0.55), length(r));

			float4 fragColor =  float4(col, 1.0);
			

			
				return fragColor;
			}
			ENDCG
		}
	}
}
