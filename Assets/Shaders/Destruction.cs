﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destruction : MonoBehaviour {

    private bool destroyed = false;
    public GameObject destroyedObject;
    public GameObject player1;

    void Start () {
		

	}
    // private void OnMouseDown()
    // {
    //     GameObject tempDestruct = Instantiate(destroyedObject, transform.position, transform.rotation);
    //     tempDestruct.transform.localScale = transform.localScale;
    //     Destroy(gameObject);
    // }

    // void OnCollisionEnter()
    // {
    //    GameObject tempDestruct =  Instantiate(destroyedObject, transform.position,transform.rotation);
    //     tempDestruct.transform.localScale = transform.localScale;
    //     Destroy(gameObject);
    // }

    public GameObject DestroyMe()
    {
        if( !destroyed )
        {
            GameObject tempDestruct =  Instantiate(destroyedObject, transform.position,transform.rotation);
            tempDestruct.transform.localScale = transform.localScale;
            Destroy(gameObject);
            destroyed = true;

            return tempDestruct;
        }
        else
        {
            return null;
        }
    }


}
