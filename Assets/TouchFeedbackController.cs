﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchFeedbackController : MonoBehaviour 
{

	[SerializeField]
	GameObject mouseParticles;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void EnableParticles()
	{
		mouseParticles.SetActive(true);
	}

	public void DisableParticles()
	{
		mouseParticles.SetActive(false);
	}
}
