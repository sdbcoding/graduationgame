﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceInFrontOfCamera : MonoBehaviour {

	
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown(0))
		{
			mouseClick();
		}
	}
	void mouseClick()
	{
		Vector3 newPosition;
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		newPosition = ray.origin + ray.direction;



		//newPosition = Camera.main.ScreenToViewportPoint( Input.mousePosition);
		//newPosition += (Camera.main.transform.forward * 2) + Camera.main.transform.position;
		//Debug.Log("Mouse: " + Input.mousePosition + " camera: " +newPosition);
		transform.position = newPosition;
		transform.forward = -Camera.main.transform.forward;
		GetComponent<ParticleSystem>().Play();
	}
}
