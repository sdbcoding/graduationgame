﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchParticleControl : MonoBehaviour {

    public Vector3Variable groundPos;
    public ParticleSystem ps;

	// Use this for initialization
	void Start () {
        ps = GetComponent<ParticleSystem>();
	}



   public void OnGroundClick()
    {
        if (transform.position != groundPos.Value)
        {
            transform.position = groundPos.Value;
            ps.Emit(5);
        }

    }
}
