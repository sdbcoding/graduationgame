﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushFuniture : MonoBehaviour {
	public ThingSet enemies;
	public float forceApplied = 200;
	void OnTriggerEnter(Collider other)
	{
		//Debug.Log("Entry accepted: " + other.gameObject.name);
		Rigidbody rigidbody = other.GetComponent<Rigidbody>();
		if (rigidbody == null){return;}

		//Check for enemy

		Thing[] thingsOnCollider = other.GetComponents<Thing>();
		if (thingsOnCollider.Length != 0)
		{
			return;
		}
		/* 
		for (int j = 0; j < thingsOnCollider.Length; j++)
		{
			if (ThingSet.Equals(thingsOnCollider[j].RunTimeSet,enemies))
			{
				return;
			}
		}

		*/
		//Debug.Log("Adding force");
		Vector3 direction = other.transform.position - transform.position;
		direction.y = 0.0f;
		direction = direction.normalized;
		rigidbody.AddForce(direction * forceApplied,ForceMode.Impulse);
	}
}
