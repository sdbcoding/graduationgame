﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsControllerVibration : MonoBehaviour {

	public Toggle vibrationToggle;
	void OnEnable()
	{
		vibrationToggle.isOn = (PlayerPrefs.GetInt("shouldVibrate",1) == 1);
		Vibration.ChangeVibration(PlayerPrefs.GetInt("shouldVibrate",1) == 1);
	}
	public void ChangedToggleState()
	{
		if (vibrationToggle.isOn)
		{
			PlayerPrefs.SetInt("shouldVibrate",1);
		} else {
			PlayerPrefs.SetInt("shouldVibrate",0);
		}
		UpdateVibrationController();
	}
	void UpdateVibrationController()
	{
		PlayerPrefs.Save();
		Vibration.ChangeVibration(PlayerPrefs.GetInt("shouldVibrate",1) == 1);
		Vibration.Vibrate(100); 
	}
}
