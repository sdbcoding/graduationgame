﻿//Parallax occlusion mapping shader that creates a landscape in a quad
Shader "Custom/ParrallaxFloor"
{
	Properties
	{
		_NoiseTex("Noise texture", 2D) = "white" {}
		_GrassTex("Grass (RGB)", 2D) = "white" {}
		_StoneTex("Stone (RGB)", 2D) = "white" {}
		_Height("Height", Range(0.0001,5)) = 1.0
		_Lerp("Lerp", Range(0.0001,5)) = 1.0
			_Radius("Radius", Range(0.0001,100)) = 1.0
	}

		SubShader
		{
			Tags { "RenderType" = "Opaque" }
			LOD 200

			CGPROGRAM

			#pragma surface surf Lambert vertex:vert
			#pragma target 3.5


			//Input
			sampler2D _NoiseTex;
			sampler2D _GrassTex;
			sampler2D _StoneTex;
			float3 _Position; // from script
			float _RadiusMod; //from script
			float _Height;
			float _Radius;
			float _Lerp;


			struct Input
			{
				//What Unity can give you
				float2 uv_NoiseTex;

				//What you have to calculate yourself
				fixed3 tangentViewDir;

				fixed4 distanceSphere : SV_POSITION;
			};


			void vert(inout appdata_full i, out Input o)
			{
				UNITY_INITIALIZE_OUTPUT(Input, o);

				//Transform the view direction from world space to tangent space            
				float3 worldVertexPos = mul(unity_ObjectToWorld, i.vertex).xyz;
				float3 worldViewDir = worldVertexPos - _WorldSpaceCameraPos;

				//To convert from world space to tangent space we need the following
				//https://docs.unity3d.com/Manual/SL-VertexFragmentShaderExamples.html
				float3 worldNormal = UnityObjectToWorldNormal(i.normal);
				float3 worldTangent = UnityObjectToWorldDir(i.tangent.xyz);
				float3 worldBitangent = cross(worldNormal, worldTangent) * i.tangent.w * unity_WorldTransformParams.w;
				
				//Use dot products instead of building the matrix
				o.tangentViewDir = float3(
					dot(worldViewDir, worldTangent),
					dot(worldViewDir, worldNormal),
					dot(worldViewDir, worldBitangent)
					);

				float _radius = _Radius;
				float distanceWorld = distance(_Position, worldVertexPos);
				float3 sphere = saturate(distanceWorld / _radius);
				o.distanceSphere = fixed4(sphere, sphere.x);

		
			}


			//Animate the uv coordinates so the landscape is moving
			float2 animateUV(float2 texturePos)
			{
				texturePos.x +=1.8;
				texturePos.y -= 1.5;

				return texturePos;
			}


			//Get the height from a uv position
			float getHeight(float2 texturePos, fixed4 distanceSphere)
			{
			   texturePos = animateUV(texturePos);

			   //Multiply with 0.2 to make the landscape flatter
			   //float4 colorNoise = -1 * tex2Dlod(_StoneTex, float4(texturePos * 0.2, 0, 0))*tex2Dlod(_GrassTex, float4(texturePos * 0.2, 0, 0))*_Lerp;
			   float4 colorNoise = -1 * tex2Dlod(_GrassTex, float4(texturePos * 0.2, 0, 0))*(1 - distanceSphere)*_Lerp;

			   //Calculate the height at this uv coordinate
			   //Just use r because r = g = b  because color is grayscale
			   //(1-color.r) because black should be low
			   //-1 because the ray is going down so the ray's y-coordinate will be negative
			   float height = (1 - colorNoise.r) * -1 * _Height*(1-distanceSphere);

			   return height;
		   }


			//Combine stone and grass depending on grayscale color
			float4 getBlendTexture(float2 texturePos, float height, fixed4 distanceSphere)
			{
				texturePos = animateUV(texturePos);

				//To make it look nice by making the texture a little bigger
				float textureSize = 0.4;

				float4 colorNoise = tex2Dlod(_NoiseTex, float4(texturePos * textureSize, 0, 0));
				float4 colorGrass = tex2Dlod(_GrassTex, float4(texturePos * textureSize, 0, 0));
				float4 colorStone = tex2Dlod(_StoneTex, float4(texturePos * textureSize, 0, 0));

				//Height is negative so convert it to positive, also invert it so mountains are high and not the grass
				//Divide with _Height because this height is actual height and we need it in 0 -> 1 range
				float colorGrayscale = 1- (abs(height) / _Height+1);

				//Combine grass and stone depending on height
				//float4 mixedColor = lerp(colorNoise, colorStone, colorGrayscale);
				//float4  mixedColor = tex2Dlod(_NoiseTex, float4(texturePos * 0.2, 0, 0)) - tex2Dlod(_GrassTex, float4(texturePos * 0.2, 0, 0))*colorGrayscale;// -tex2Dlod(_StoneTex, float4(texturePos * 0.2, 0, 0))*colorGrayscale;
				float4 mixedColor = lerp(colorNoise, 1-colorGrass*colorNoise, colorGrayscale);
			
			
				return mixedColor;
			}


			//Get the texture position by interpolation between the position where we hit terrain and the position before
			float2 getWeightedTexPos(float3 rayPos, float3 rayDir, float stepDistance, fixed4 distanceSphere)
			{
				//Move one step back to the position before we hit terrain
				float3 oldPos = rayPos - stepDistance * rayDir;

				float oldHeight = getHeight(oldPos.xz, distanceSphere);

				//Always positive
				float oldDistToTerrain = abs(oldHeight - oldPos.y);

				float currentHeight = getHeight(rayPos.xz, distanceSphere);

				//Always negative
				float currentDistToTerrain = rayPos.y - currentHeight;

				float weight = currentDistToTerrain / (currentDistToTerrain - oldDistToTerrain);

				//Calculate a weighted texture coordinate
				//If height is -2 and oldHeight is 2, then weightedTex is 0.5, which is good because we should use
				//the exact middle between the coordinates
				float2 weightedTexPos = oldPos.xz * weight + rayPos.xz * (1 - weight);

				return weightedTexPos;
			}


			void surf(Input IN, inout SurfaceOutput o)
			{


				fixed4 DistanceSphere = IN.distanceSphere;
				fixed4 colgrass = tex2D(_GrassTex,  IN.uv_NoiseTex);
				fixed4 coldstone = tex2D(_StoneTex,  IN.uv_NoiseTex);
				fixed4 colNoise = tex2D(_NoiseTex,  IN.uv_NoiseTex);


				//Where is the ray starting? y is up and we always start at the surface
				float3 rayPos = float3(IN.uv_NoiseTex.x, 0, IN.uv_NoiseTex.y);

				//What's the direction of the ray?
				float3 rayDir = normalize(IN.tangentViewDir);

				//Find where the ray is intersecting with the terrain with a raymarch algorithm
				int STEPS = 300;
				float stepDistance = 0.01;

				//The default color used if the ray doesnt hit anything
				float4 finalColor = 1;

				for (int i = 0; i < STEPS; i++)
				{
					//Get the current height at this uv coordinate
					float height = getHeight(rayPos.xz, DistanceSphere);

					//If the ray is below the surface
					if (rayPos.y < height)
					{
						//Get the texture position by interpolation between the position where we hit terrain and the position before
						float2 weightedTex = getWeightedTexPos(rayPos, rayDir, stepDistance, DistanceSphere);

						float height = getHeight(weightedTex, DistanceSphere);

						finalColor = getBlendTexture(weightedTex, height, DistanceSphere);

						//We have hit the terrain so we dont need to loop anymore  
						break;
					}

					//Move along the ray
					rayPos += stepDistance * rayDir;
				}
				//o.Albedo = (coldstone*_Height)*(colgrass);
				//Output

				float3 stepVal= step(2.9,finalColor.g+ finalColor.r+ finalColor.b);
				o.Albedo = finalColor.rgb - stepVal;
			}
			ENDCG
		}
			FallBack "Diffuse"
}