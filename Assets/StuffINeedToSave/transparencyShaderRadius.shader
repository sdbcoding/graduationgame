﻿Shader "Custom/transparencyShaderRadius"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_NoiseTex("Noise Texture", 2D) = "white" {}
		_VoidTex("void Texture", 2D) = "white" {}
		_Radius("Radius", Range(0,30)) = 5
			_Attenuation("Attenuation", Range(0,2)) = 1
			_Height("Height", Range(0.0001,5)) = 1.0
			_LerpVal("Lerpval", Range(0,1)) = 1.0


		
	}
	SubShader
	{
		 Tags { "RenderType" = "Opaque" }
		LOD 200

	
		
	

		Pass
		{
			CGPROGRAM
		
			#pragma vertex vert lambert
			#pragma fragment frag
			#pragma target 3.0
			#include "UnityCG.cginc"
			#include "Flow.cginc"
			#include "Random.cginc"

			sampler2D _MainTex;
			sampler2D _NoiseTex;
			sampler2D _VoidTex;
			float4 _MainTex_ST;
			float4 _NoiseTex_ST;
			float4 _VoidTex_ST;
			float3 _Position; // from scrip
			float _RadiusMod; // from scrip
			float _Radius;
			float _Attenuation;
			float _Height;
			float _LerpVal;
			


			uniform float4 _LightColor0;

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normals : NORMAL;
				float4 tangent : TANGENT;
				
			};

			struct v2f
			{

				float2 uv : TEXCOORD0;
				float2 uv_NoiseTex : TEXCOORD0;
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
				fixed4 color2 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float3 tangentViewDir :TEXCOORD3;

			};

		
			
			v2f vert (appdata v)
			{
				v2f o;
				//diffuse light
				float3 normalDir = normalize(mul(float4(v.normals, 0.0), unity_WorldToObject).xyz);
				float3 lightDir = normalize(_WorldSpaceLightPos0);
				float3 diffuseRef =  dot(normalDir, lightDir);


				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				float3 worldVertexPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				float3 worldViewDir = worldVertexPos - _WorldSpaceCameraPos;

				float3 worldNormal = UnityObjectToWorldNormal(v.normals);
				float3 worldTangent = UnityObjectToWorldDir(v.tangent.xyz);
				float3 worldBitangent = cross(worldNormal, worldTangent) * v.tangent.w * unity_WorldTransformParams.w;

				o.tangentViewDir = float3(
					dot(worldViewDir, worldTangent),
					dot(worldViewDir, worldNormal),
					dot(worldViewDir, worldBitangent)
					);




				//distortion

				float3 random = rand(mul(unity_ObjectToWorld, v.vertex).xyz);
				float random4 = random.x;

				
				float _radius = (_Radius + random4)*(1 - _RadiusMod);
				float distanceWorld = distance(_Position, o.worldPos);
				float3 sphere = saturate(distanceWorld / _radius);
				//v.vertex += float4(v.normals, 1)*(random4*(1-float4(sphere,1)));
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.color =float4(sphere,sphere.x);
				o.color2= float4(diffuseRef,0.0);


				
				return o;
			}

			float getHeight(float2 texturePos, float heightInput)
			{
				//float4 colorNoise = tex2Dlod(_NoiseTex, float4(texturePos * 0.2, 0, 0));

				float4 colorNoise = -1 * tex2Dlod(_VoidTex, float4(texturePos * 0.2, 0, 0))*tex2Dlod(_NoiseTex, float4(texturePos * 0.2, 0, 0))*_LerpVal;
				float height = (1 - colorNoise.r) * -1 * _Height*heightInput;
				return height;
			}

			float2 getWeightedTexPos(float3 rayPos, float3 rayDir, float stepDistance, float heightInput)
			{
				float3 oldPos = rayPos - stepDistance * rayDir;
				float oldHeight = getHeight(oldPos.xz, heightInput);
				float oldDistToTerrain = abs(oldHeight - oldPos.y);
				float currentHeight = getHeight(rayPos.xz, heightInput);

				float currentDistToTerrain = rayPos.y - currentHeight;
				float weight = currentDistToTerrain / (currentDistToTerrain - oldDistToTerrain);

				float2 weightedTexPos = oldPos.xz * weight + rayPos.xz * (1 - weight);

				return weightedTexPos;
			}

		

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 colstd = tex2D(_MainTex, i.uv);
				fixed4 coldVoid = tex2D(_VoidTex, i.uv);
				fixed4 colNoise = tex2D(_NoiseTex, i.uv);

				float lerpVal =i.color;
				

				float3 rayPos = float3(i.uv_NoiseTex.x, 0, i.uv_NoiseTex.y);
				float3 rayDir = normalize(i.tangentViewDir);
				int STEPS = 300;
				float stepDistance = 0.01;
				float4 finalColor = 1;
				for (int i = 0; i < STEPS; i++)
				{
					float height = getHeight(rayPos.xz, 1 - lerpVal);

					
				

				

					if (rayPos.y < height)
					{
						float2 weightedTex = getWeightedTexPos(rayPos, rayDir, stepDistance, 1-lerpVal);
						float height = getHeight(weightedTex, 1 - lerpVal);
					
			
						finalColor =lerp( tex2Dlod(_VoidTex, float4(weightedTex, 0, 0)), tex2Dlod(_MainTex, float4(weightedTex, 0, 0)),lerpVal*1.4);
						break;
					}
					rayPos += stepDistance * rayDir;
				}
				
				float4 col1 = colstd * smoothstep(0.5, 0.6, lerpVal);
				float4 col2 = finalColor * smoothstep(0.6, 0.5, lerpVal);


				float4 col3 = float4(1, 1, 1, 1) * smoothstep(0.5, 0.45, lerpVal);
				float4 col4 = float4(1, 0, 0, 1) * smoothstep(0.65, 0.45, lerpVal);


				float4 colRadius = (col1 + col2+ (col4 - col3)); // i.color2 is diffuse light

				return  finalColor;// colRadius * float4(_Attenuation, _Attenuation, _Attenuation, 1);
			
				
			}
			ENDCG
		}
		
	}
	
}
