﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsControllerCinematics : MonoBehaviour 
{
	[SerializeField]
	Toggle cinematicToggle;

	void OnEnable()
	{
		cinematicToggle.isOn = (PlayerPrefs.GetInt("showCinematics",1)==1);
	}
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ToggleState()
	{
		if( cinematicToggle.isOn )
		{
			PlayerPrefs.SetInt("showCinematics",1);
		}
		if( !cinematicToggle.isOn )
		{
			PlayerPrefs.SetInt("showCinematics",0);
		}
	}
}
