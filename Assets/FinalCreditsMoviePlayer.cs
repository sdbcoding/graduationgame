﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class FinalCreditsMoviePlayer : MonoBehaviour {
	public Camera cam;
	public VideoClip clip;
	public AudioClip audioClip;
	public RenderTexture rendertexture;
	public RectTransform rectTransform;
	public RectTransform endOfCredits;
	public GameEvent creditsFinished;
	bool haveStarted = false;
	float currentTime = 0;
	public GameEvent startGame;



	Vector3 finalPos; Vector3 finalScale; bool rescale = false; bool move = false; float screenHeight;
	// Use this for initialization
	void Start () 
	{
		AkSoundEngine.PostEvent("PauseAll",gameObject);
		startGame.Raise();

		StartCoroutine(playVideo());
		finalPos = new Vector2(rectTransform.anchoredPosition.x/2, rectTransform.anchoredPosition.y);
		finalScale = new Vector3(rectTransform.localScale.x/2, rectTransform.localScale.y, rectTransform.localScale.z);
		screenHeight = Screen.height;
		// Debug.Log("Screen Height : " + Screen.height);
	}


	IEnumerator playVideo()
	{
		var videoPlayer = cam.gameObject.AddComponent<UnityEngine.Video.VideoPlayer>();
		var audioSource = cam.gameObject.AddComponent<AudioSource>();
		videoPlayer.playOnAwake = false;
		audioSource.playOnAwake = false;
		
		videoPlayer.clip = clip;

		//audioSource.clip = clip.aui;
        // By default, VideoPlayers added to a camera will use the far plane.
        // Let's target the near plane instead.
        videoPlayer.renderMode = UnityEngine.Video.VideoRenderMode.RenderTexture;
		videoPlayer.targetTexture = rendertexture;

        // This will cause our scene to be visible through the video being played.

        // Set the video to play. URL supports local absolute or relative paths.
        // Here, using absolute.
        //videoPlayer.url = "/Users/graham/movie.mov";

        // Skip the first 100 frames.

        // Restart from beginning when done.
        videoPlayer.isLooping = true;
		videoPlayer.audioOutputMode = VideoAudioOutputMode.None;
		//videoPlayer.EnableAudioTrack(0,true);
		//videoPlayer.SetTargetAudioSource(0,audioSource);
        // Each time we reach the end, we slow down the playback by a factor of 10.
        //videoPlayer.loopPointReached += EndReached;
		videoPlayer.Prepare();
		while(!videoPlayer.isPrepared)
		{
			// Debug.Log("Preparing video");
			yield return null;
		}
			// Debug.Log("Done Preparing video");

        // Start playback. This means the VideoPlayer may have to prepare (reserve
        // resources, pre-load a few frames, etc.). To better control the delays
        // associated with this preparation one can use videoPlayer.Prepare() along with
        // its prepareCompleted event.
        
		AkSoundEngine.PostEvent("EdgeOfAggelos",gameObject);
		videoPlayer.Play();
		StartCoroutine(ScaleVideo());

		//audioSource.Play();

	}

	IEnumerator ScaleVideo()
	{
		yield return new WaitForSeconds(1f);
		// Debug.Log("Rescale");
		rescale = true;
		StartCoroutine(MoveVideo());
	}

	IEnumerator MoveVideo()
	{
		yield return new WaitForSeconds(2f);
		// Debug.Log("Move");
		move = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if( rescale )
		{
			// Debug.Log("Rescaling");
			rectTransform.localScale = Vector3.Lerp( rectTransform.localScale, finalScale, 0.5f );
		}
		if( move )
		{
			// Debug.Log("Moving");
			rectTransform.anchoredPosition = Vector2.Lerp( rectTransform.anchoredPosition, finalPos, 0.5f );
		}

		// Debug.Log("Position" + endOfCredits.position.y);
		// Debug.Log("Local position" + endOfCredits.localPosition.y);
		// Debug.Log("Anchor position" + endOfCredits.anchoredPosition.y);
		if( endOfCredits.position.y > screenHeight )
			creditsFinished.Raise();

		if (haveStarted) {return;}
		currentTime += Time.deltaTime;

		if (currentTime > 1.0f)
		{
			//StartCoroutine(playVideo());
			haveStarted = true;
		}
	}
}
