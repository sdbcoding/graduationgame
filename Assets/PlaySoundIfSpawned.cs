﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundIfSpawned : MonoBehaviour 
{
	[SerializeField]
	PlaySoundFromEvent playSound;
	private bool playerSpawned = false;
	

	public void playSoundIfSpawned()
	{
		if( playerSpawned )
			playSound.PlaySound();
	}

	public void enableCombatMusic()
	{
		playerSpawned = true;
	}
}
