﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceCameraScript : MonoBehaviour {

	[SerializeField]
	GameObject terrorLevelObject;
	public float yPositionOfObjects;
	public ThingSet enemies;
	private Camera mainCamera;
	Transform[] forwardSettingObjects;
	// Use this for initialization
	void Start () {
		forwardSettingObjects = new Transform[enemies.GetItems().Count];
		mainCamera = Camera.main;
		transform.forward = -mainCamera.transform.forward;
		for (int i = 0; i < enemies.GetItems().Count; i++)
		{
			GameObject temp = Instantiate(terrorLevelObject);
			Vector3 newPosition = enemies.GetItems()[i].transform.position;
			newPosition.y = yPositionOfObjects;
			temp.transform.position = newPosition;
			forwardSettingObjects[i] = temp.transform;
		}
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < forwardSettingObjects.Length; i++)
        {
            forwardSettingObjects[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < enemies.GetItems().Count; i++)
        {
			
            forwardSettingObjects[i].gameObject.SetActive(true);
			forwardSettingObjects[i].forward = -mainCamera.transform.forward;
			Vector3 newPosition = enemies.GetItems()[i].transform.position;
			newPosition.y += yPositionOfObjects;
			forwardSettingObjects[i].position = newPosition;
			EnemyTerrorLevel terrorLevel = enemies.GetItems()[i].GetComponent<EnemyTerrorLevel>();
			if (terrorLevel != null)
			{
				forwardSettingObjects[i].GetChild(0).gameObject.SetActive(false);
				forwardSettingObjects[i].GetChild(1).gameObject.SetActive(false);
				forwardSettingObjects[i].GetChild(2).gameObject.SetActive(false);
				if (terrorLevel.GetTerrorLevel() == 1)
				{
				forwardSettingObjects[i].GetChild(0).gameObject.SetActive(true);

				} else if (terrorLevel.GetTerrorLevel() == 2)
				{
				forwardSettingObjects[i].GetChild(1).gameObject.SetActive(true);

				} else if (terrorLevel.GetTerrorLevel() == 3)
				{
				forwardSettingObjects[i].GetChild(2).gameObject.SetActive(true);

				}
			}
		}
	}
}
