﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;
using Newtonsoft.Json;

public class MMLoader : Manager<MMLoader> {
    
    public Frame[] Normal;
    public Frame[] Terrified;

    void Awake() {
        TextAsset NormalText = Resources.Load("Animations/Normal") as TextAsset;
        Normal = JsonConvert.DeserializeObject<Frame[]>(NormalText.text);
        TextAsset TerrifiedText = Resources.Load("Animations/Terrified") as TextAsset;
        Terrified = JsonConvert.DeserializeObject<Frame[]>(TerrifiedText.text);
	}

}

public class Frame
{
    public float[] dataPoint;
    public float timeOffset;
    public string animFile;
    public float[] angles;
}