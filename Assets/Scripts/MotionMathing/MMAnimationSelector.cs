﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;
using Utility;
//using C5;
public enum State
{ 
    StandStill,
    Normal,
    Terrified,
    PushBack
}

public enum Speed
{
    Fast,
    Normal
}
public class MMAnimationSelector : OnUpdate
{
    public bool PushB;
    public State State;
    public bool ShowPoints;
    private Animator Animator;
    public int NumberOfNearestNeighbors = 10;
    public float AnimSpeed = 1;
    public Speed Speed = Speed.Normal;
    private float MaxSpeed = 1.2f;
    public List<Vector3> pointsList;
    public EnemyTerrorLevel EnemyTerrorLevel;
    private float elapsedTime = 0f;
    private float facingAngle;
    private Vector3 standingPoint;
    private readonly int dimensions = 3;
    private HashSet<int> TakenFrames;
    private State[] WalkingStates;
    private float TimeToWait;
    private float TimeWaited;
    float responsiveness;
    private Action ActionWhenDone;
    KDTree NormalKDTree;
    KDTree TerrifiedKDTree;

    private Frame[] Frames;
    private MaxHeap<DistanceIndex> Neighbors;

    Transform t;
    private Transform LeftLeg;
    private Transform RightLeg;

    private float[] CurrentAngles;
    private float[] PreviousAngles;
    private float[] PPreviousAngles;

    private bool leftFootIsGrounded = true;
    private bool leftFootIsInAir = false;

    private bool rightFootIsGrounded = true;
    private bool rightFootIsInAir = false;

    private bool playSound = false;

    // Use this for initialization
    void Start () {
        State = State.Normal;
        Frames = MMLoader.Instance().Normal;
        Animator = GetComponent<Animator>();

        t = GetComponent<Transform>();

        Transform hips = t.Find("Hips");

        LeftLeg = hips.Find("LeftThigh");
        if (LeftLeg == null)
            LeftLeg = hips.Find("LeftUpLeg");

        RightLeg = hips.Find("RightThigh");
        if (RightLeg == null)
            RightLeg = hips.Find("RightUpLeg");

        CurrentAngles = new float[2];
        PreviousAngles = new float[2];

        facingAngle = t.eulerAngles.y;
        standingPoint = t.position;

        TimeToWait = -1;
        TimeWaited = 0f;
        ActionWhenDone = null;

        WalkingStates = new State[] { State.Normal, State.Terrified, State.StandStill };

        responsiveness = 2f / 3f / AnimSpeed;

        PushB = false;
    }
    public void Pause()
    {
        Animator.speed = 0f;
    }

    public void Resume()
    {
        Animator.speed = AnimSpeed;
    }
    public void StandStill(float time = -1, Action whenDone = null)
    {
        TimeToWait = time;
        TimeWaited = TimeWaited < TimeToWait ? TimeWaited : 0f;
        State = State.StandStill;
        Frames = MMLoader.Instance().Normal;
        ActionWhenDone = whenDone;
        facingAngle = t.eulerAngles.y;
        standingPoint = t.position;
    }
    public void Walk()
    {
        Frames = MMLoader.Instance().Normal;
        State = State.Normal;
        MaxSpeed = 1.2f;
    }

    public void Run()
    {
        Frames = MMLoader.Instance().Normal;
        State = State.Normal;
        MaxSpeed = 1.2f;
    }
    public void Terrified()
    {
        Frames = MMLoader.Instance().Terrified;
        State = State.Terrified;
        MaxSpeed = 1.0f;
    }

    public void PushBack(float time = -1, Action whenDone = null)
    {
        Animator.CrossFadeInFixedTime("PushBack", 0.3f / AnimSpeed, 0, 0.05f / AnimSpeed);
        State = State.PushBack;
        TimeToWait = time;
        ActionWhenDone = whenDone;
        TimeWaited = 0f;
    }

    public void RotateTo(float angle)
    {
        facingAngle = angle;
    }
    private bool IsFading()
    {
        return Animator.GetAnimatorTransitionInfo(0).anyState;
    }

    private void UpdateAngles()
    {
        PPreviousAngles = (float[])PreviousAngles.Clone();
        PreviousAngles = (float[])CurrentAngles.Clone();

        CurrentAngles[0] = LeftLeg.localEulerAngles.z;
        CurrentAngles[1] = RightLeg.localEulerAngles.z;
    }


    // Update is called once per frame
    public override void UpdateEventRaised() {
        UpdateAngles();

        if (ShowPoints)
            DrawPoints();

        if (PushB)
        {
            PushBack(1);
            PushB = false;
        }

        if (TimeToWait != -1 && ActionWhenDone != null && TimeWaited > TimeToWait)
        {
            TimeToWait = -1;
            ActionWhenDone.Invoke();
            ActionWhenDone = null;
        }

        if (!IsFading() && elapsedTime >= responsiveness && Animator.speed != 0f && InArray(WalkingStates, State))
        {
            Animator.speed = AnimSpeed;

            responsiveness = 2f / 3f / AnimSpeed;
            float[] datapoint = GetDatapoint();
            /*if (Mathf.Abs(datapoint[2]) > 0.8f)
            {
                t.eulerAngles = new Vector3(t.eulerAngles.x, t.eulerAngles.y + 180);
                datapoint = GetDatapoint();
            }*/
            Frame nextFrame = FindFrame(datapoint);

            Animator.CrossFadeInFixedTime(nextFrame.animFile, 0.3f / AnimSpeed, 0, nextFrame.timeOffset / AnimSpeed);
            //Animator.PlayInFixedTime(nextFrame.animFile, 0, nextFrame.timeOffset);

            elapsedTime = 0;
        } else if (State == State.PushBack && elapsedTime > 4.3f / AnimSpeed)
        {
            Terrified();
        }
        elapsedTime += Time.deltaTime;
        TimeWaited += Time.deltaTime;

    }

    void OnAnimatorIK(int layerIndex)
    {
        SoundLeftLeg();
        SoundRightLeg();

        if (playSound)
        {
            AkSoundEngine.PostEvent("NPCFsMedium", gameObject);
            playSound = false;
        }
    }

    void DrawPoints()
    {
        foreach(Vector3 point in pointsList)
        {
            GameObject circle = new GameObject("Circle");
            circle.transform.position = point;
            circle.transform.eulerAngles = new Vector3(90f, 0f);
            SpriteRenderer sprite = circle.AddComponent<SpriteRenderer>();
            sprite.sprite = Resources.Load("Clicked", typeof(Sprite)) as Sprite;
            GameObject.Destroy(circle, 0.03f);
        }
    }

    void SoundLeftLeg()
    {
        Vector3 leftFootT = Animator.GetIKPosition(AvatarIKGoal.LeftFoot);
        Quaternion leftFootQ = Animator.GetIKRotation(AvatarIKGoal.LeftFoot);

        Vector3 leftFootH = new Vector3(0, -Animator.leftFeetBottomHeight, 0);

        Vector3 pos = leftFootT + leftFootQ * leftFootH;


        if (pos.y < 0.04)
        {
            if (!leftFootIsGrounded && leftFootIsInAir)
            {
                playSound = true;
            }

            leftFootIsGrounded = true;
            leftFootIsInAir = false;
        }
        else
        {
            leftFootIsGrounded = false;
            leftFootIsInAir = true;
        }        
    }

    void SoundRightLeg()
    {
        Vector3 rightFootT = Animator.GetIKPosition(AvatarIKGoal.RightFoot);
        Quaternion rightFootQ = Animator.GetIKRotation(AvatarIKGoal.RightFoot);

        Vector3 rightFootH = new Vector3(0, -Animator.rightFeetBottomHeight, 0);

        Vector3 pos = rightFootT + rightFootQ * rightFootH;

        if (pos.y < 0.04)
        {
            if (!rightFootIsGrounded && rightFootIsInAir)
            {
                playSound = true;
            }

            rightFootIsGrounded = true;
            rightFootIsInAir = false;
        }
        else
        {
            rightFootIsGrounded = false;
            rightFootIsInAir = true;
        }
    }

    private float NormalizeAngle(float angle)
    {
        /*
        while (angle < 0)
            angle += 360;
        while (angle > 360)
            angle -= 360;
        return angle;*/
        return NormalizeAngle2(angle);
    }

    private float NormalizeAngle2(float angle)
    {
        while (angle < -180)
            angle += 360;
        while (angle > 180)
            angle -= 360;
        return angle;
    }

    private float StandardizeAngle(float angle)
    {
        return angle / 180;
    }

    private float Distance(Vector3 first, Vector3 second)
    {
        return Mathf.Sqrt(Mathf.Pow(second.x - first.x, 2) + Mathf.Pow(second.z - first.z, 2));
    }

    public Vector3 FindNextPoint()
    {
        int pointsToPass = 0;
        float distance = Distance(t.transform.position, pointsList[0]);
        while (distance < 1f && pointsList.Count > pointsToPass + 1)
        {
            pointsToPass++;
            distance = Distance(t.transform.position, pointsList[pointsToPass]);
        }

        pointsList.RemoveRange(0, pointsToPass);
        if (pointsList.Count > 0)
            return pointsList[0];
        else
            return t.position;
    }

    private float[] GetDatapoint()
    {
        float distance = 0f;
        float angle = 0f;
        float angleToFace = 0f;

        if (pointsList.Count > 0 && State != State.StandStill)
        {
            Vector3 nextPoint = FindNextPoint();
            distance = Distance(t.transform.position, nextPoint);

            distance = distance <= MaxSpeed ? distance : MaxSpeed;

            angle = Mathf.Rad2Deg * Mathf.Atan2(nextPoint.x - t.position.x, nextPoint.z - t.position.z);
            angle = StandardizeAngle(NormalizeAngle((angle - t.eulerAngles.y) * -1));
            angleToFace = angle;
            
            if (distance < 0.4f)
            {
                facingAngle = t.eulerAngles.y;
                standingPoint = t.position;
                pointsList.Clear();
            }

            // Slow down a bit when turning (angle is between -1 and 1)
            distance *= (1 - Mathf.Abs(angle) * 1f);
        } else // We are standing still
        {
            angle = Mathf.Rad2Deg * Mathf.Atan2(standingPoint.x - t.position.x, standingPoint.z - t.position.z);
            angle = StandardizeAngle(NormalizeAngle((angle - t.eulerAngles.y) * -1));
            distance = Vector3.Distance(t.transform.position, standingPoint);

            angleToFace = StandardizeAngle(NormalizeAngle((angleToFace - t.eulerAngles.y) * -1));
        }

        return new float[] { angle, distance, angleToFace };
    }

    private Frame FindFrame(float[] datapoint)
    {
        Frame bestFrame = null;
        float lowestCost = -1;
        TakenFrames = new HashSet<int>();

        Neighbors = new MaxHeap<DistanceIndex>(new DistanceIndexComparer());

        TreeLookUpNew(datapoint);

        while (Neighbors.Count != 0)
        {
            DistanceIndex di = Neighbors.ExtractDominating();
            Frame f = Frames[di.frameIndex];
            float cost = Cost(f);
            if (cost < lowestCost || lowestCost == -1)
            {
                bestFrame = f;
                lowestCost = cost;
            }

        }

        //DistanceIndex[] slow = TreeLookUpSlow(datapoint);

        /*for (int i = 0; i < NumberOfNearestNeighbors; i++)
        {
            frameIndex = TreeLookUp(datapoint);
            TakenFrames.Add(frameIndex);
            Frame frame = Frames[frameIndex];
            float cost = Cost(frame);
            if (cost < lowestCost || lowestCost == -1)
            {
                bestFrame = frame;
                lowestCost = cost;
            }
        }*/

        return bestFrame;
    }

    private float Cost(Frame frame)
    {
        float cost = 0f;
        int leftDirection = NormalizeAngle(CurrentAngles[0] - PPreviousAngles[0]) < 0 ? -1 : 1;
        int rightDirection = NormalizeAngle(CurrentAngles[1] - PPreviousAngles[1]) < 0 ? -1 : 1;
        cost += Mathf.Abs(NormalizeAngle2(frame.angles[0]-CurrentAngles[0]));
        cost += Mathf.Abs(NormalizeAngle2(frame.angles[1]-CurrentAngles[1]));
        if (leftDirection != frame.angles[2])
            cost *= 2;
        if (rightDirection != frame.angles[3])
            cost *= 2;
        return cost;
    }

    private float Distance(float[] p1, int p2)
    {
        float distance = 0;
        for (int i = 0; i < dimensions; i++)
        {
            distance += Mathf.Pow(p1[i] - Frames[p2].dataPoint[i], 2);
        }
        return distance;
    }

    private int CloserDistance(float[] pivot, int p1, int p2)
    {
        if (p1 == -1 || TakenFrames.Contains(p1))
            return p2;
        if (p2 == -1 || TakenFrames.Contains(p2))
            return p1;

        float d1 = Distance(pivot, p1);
        float d2 = Distance(pivot, p2);
        return d1 < d2 ? p1 : p2;
    }

    private DistanceIndex CloserDistanceNew(DistanceIndex p1, DistanceIndex p2)
    {
        if (p1 == null)
            return p2;
        if (p2 == null)
            return p1;

        return p1.distance < p2.distance ? p1 : p2;
    }

    private int TreeLookUp(float[] datapoint, int indexRoot = 0, int depth = 0)
    {
        if (indexRoot >= Frames.Length || Frames[indexRoot] == null)
            return -1;

        int axis = depth % dimensions;

        int nextBranch;
        int oppositeBranch;

        if (datapoint[axis] < Frames[indexRoot].dataPoint[axis])
        {
            nextBranch = 2 * indexRoot + 1;
            oppositeBranch = 2 * indexRoot + 2;
        }
        else
        {
            nextBranch = 2 * indexRoot + 2;
            oppositeBranch = 2 * indexRoot + 1;
        }

        int next = TreeLookUp(datapoint, nextBranch, depth + 1);
        int best = CloserDistance(datapoint, next, indexRoot);

        if (Distance(datapoint, best) > Mathf.Abs(datapoint[axis] - Frames[indexRoot].dataPoint[axis]))
        {
            next = TreeLookUp(datapoint, oppositeBranch, depth + 1);
            best = CloserDistance(datapoint, next, best);
        }
        return best;
    }

    private DistanceIndex TreeLookUpNew(float[] datapoint, int indexRoot = 0, int depth = 0)
    {
        if (indexRoot >= Frames.Length || Frames[indexRoot] == null)
            return null;

        DistanceIndex current = new DistanceIndex(indexRoot, Distance(datapoint, indexRoot));

        if (Neighbors.Count < NumberOfNearestNeighbors)
        {
            Neighbors.Add(current);
        } else
        {
            DistanceIndex worst = Neighbors.GetMin();
            if (current.distance < worst.distance)
            {
                Neighbors.ExtractDominating();
                Neighbors.Add(current);
            }
        }

        int axis = depth % dimensions;

        int nextBranch;
        int oppositeBranch;

        if (datapoint[axis] < Frames[indexRoot].dataPoint[axis])
        {
            nextBranch = 2 * indexRoot + 1;
            oppositeBranch = 2 * indexRoot + 2;
        }
        else
        {
            nextBranch = 2 * indexRoot + 2;
            oppositeBranch = 2 * indexRoot + 1;
        }
        DistanceIndex next = TreeLookUpNew(datapoint, nextBranch, depth + 1);
        DistanceIndex best = CloserDistanceNew(next, current);

        if (Neighbors.GetMin().distance > Mathf.Abs(datapoint[axis] - Frames[indexRoot].dataPoint[axis]))
        {
            next = TreeLookUpNew(datapoint, oppositeBranch, depth + 1);
            best = CloserDistanceNew(next, best);
        }
        return best;
    }

    private DistanceIndex[] TreeLookUpSlow(float[] datapoint)
    {
        DistanceIndex[] nearest = new DistanceIndex[NumberOfNearestNeighbors];

        for (int i = 0; i < NumberOfNearestNeighbors; i++)
            nearest[i] = new DistanceIndex(-1, -1f);

        for(int i = 0; i < Frames.Length; i++)
        {
            if (Frames[i] == null)
                continue;
            float distance = Distance(datapoint, i);
            int index;
            float max = findMax(nearest, out index);
            if (distance < max || max == -1)
            {
                nearest[index].distance = distance;
                nearest[index].frameIndex = i;
            }
        }

        return nearest;
    }

    private float findMax(DistanceIndex[] nearest, out int index)
    {
        float max = -1f;
        index = -1;

        for (int i = 0; i < NumberOfNearestNeighbors; i++)
        {
            if (nearest[i].distance == -1)
            {
                index = i;
                return -1;
            }
            else if (nearest[i].distance > max)
            {
                max = nearest[i].distance;
                index = i;
            }
        }
        return max;
    }

    private bool InArray<T>(T[] array, T element)
    {
        foreach (T e in array)
        {
            if (e.Equals(element))
                return true;
        }
        return false;
    }
}

public class DistanceIndexComparer : Comparer<DistanceIndex>
{
    public override int Compare(DistanceIndex x, DistanceIndex y)
    {
        return x.distance < y.distance ? -1 : (x.distance > y.distance ? 1 : 0);
    }
}

public class DistanceIndex
{
    public int frameIndex;
    public float distance;
    public DistanceIndex(int i, float d)
    {
        frameIndex = i;
        distance = d;
    }
}