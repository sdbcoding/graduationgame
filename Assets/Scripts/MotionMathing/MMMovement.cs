﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class MMMovement : EnemyMovement
{
    /*public NavMeshAgent agent;
    public Action movementDoneRespons;
    public FloatReference yValue;*/
    public MMAnimationSelector AnimSelector;

    void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    public override void UpdateEventRaised()
    {
        if (isWaiting)
        {
            currentTime += Time.deltaTime;
            if (currentTime > timeToReach)
            {
                isWaiting = false;
                SetDestination(savedPosition);
            }
            return;
        }
        if (isRotating)
        {
            AnimSelector.RotateTo(angle);
            isRotating = false;
            return;
        }
        if (isWalkingBackwards)
        {
            currentTime += Time.deltaTime;
            Vector3 newPosition = Vector3.MoveTowards(transform.position, backWardsDestination, backwardsSpeed);
            newPosition.y = transform.position.y;
            transform.position = newPosition;
            Vector3 transformPositionForBackwards = transform.position;
            transformPositionForBackwards.y = yValue;

            float distanceForBackwards = Vector3.Distance(backWardsDestination, transformPositionForBackwards);
            if (distanceForBackwards <= 0.2f || distanceForBackwards == Mathf.Infinity)
            {
                isWalkingBackwards = false;
                DestinationReached();
            }
            if (currentTime > maxBackwardsTime)
            {
                isWalkingBackwards = false;
                DestinationReached();
            }
            return;
        }
        float distance = DistanceToLast();
        if (distance <= 0.4f || distance == Mathf.Infinity)
        {
            DestinationReached();
        }
    }
    float DistanceToLast()
    {
        List<Vector3> points = AnimSelector.pointsList;
        if (points.Count == 0)
        {
            return 0f;
        } else
        {
            Vector3 lastPoint = points[points.Count - 1];
            return Vector3.Distance(lastPoint, transform.position);
        }
    }
    void DestinationReached()
    {
        //Debug.Log("Destination reached");
        timeoutTimer = 0;
        StandStill();

        //PauseNavAgent();
        if (movementDoneRespons != null)
        {
            Action tempAction = movementDoneRespons;
            movementDoneRespons = null;
            tempAction.Invoke();
        }

    }
    public override void SetDestination(Vector3 position)
    {
        StartMoving();
        ResumeNavAgent();
        NavMeshPath path = new NavMeshPath();
        agent.CalculatePath(position, path);
        AnimSelector.pointsList = new List<Vector3>(path.corners);
        timeoutTimer = 0;
    }
    public override void SetDestination(Vector3 position, Action newResponse)
    {
        ResumeNavAgent();
        StartMoving();
        NavMeshPath path = new NavMeshPath();
        agent.CalculatePath(position, path);
        AnimSelector.pointsList = new List<Vector3>(path.corners);
        NewReturnResponse(newResponse);
        timeoutTimer = 0;

    }
    public override void SetDestination(Vector3 position, Action newResponse, float timeBeforeBeginning)
    {
        isWaiting = true;
        
        timeToReach = timeBeforeBeginning;
        savedPosition = position;
        currentTime = 0;
        NewReturnResponse(newResponse);
        NavMeshPath path = new NavMeshPath();
        agent.CalculatePath(transform.position, path);
        AnimSelector.pointsList = new List<Vector3>(path.corners);
        agent.isStopped = true;
        timeoutTimer = 0;

    }
    public override void TurnTowards(Vector3 newFacing, Action newResponse, float timeToTurn)
    {
        NewReturnResponse(newResponse);
        angle = Vector3.Angle(Vector3.forward, newFacing);
        isRotating = true;
        direction = newFacing;
        startRotation = transform.rotation;
        currentTime = 0;
        timeToReach = timeToTurn;
        timeoutTimer = 0;

    }
    void NewReturnResponse(Action newResponse)
    {
        if (movementDoneRespons != null)
        {
            Action tempAction = movementDoneRespons;
            movementDoneRespons = null;
            tempAction.Invoke();
        }
        movementDoneRespons = newResponse;
    }
    public override void WalkBackwards(float distance, Action newReponse)
    {
        //RaycastBackwards the distance. Walk to -X from what you hit if you hit anythingn
        NewReturnResponse(newReponse);
        currentTime = 0;
        isWalkingBackwards = true;
        RaycastHit hit;
        Physics.Raycast(transform.position, -transform.forward, out hit, distance);
        if (hit.collider == null)
        {
            backWardsDestination = (-transform.forward * distance) + transform.position;
        }
        else
        {
            float distanceToPoint = Vector3.Distance(transform.position, hit.point);
            backWardsDestination = (-transform.forward * (distanceToPoint - 0.1f)) + transform.position;
            Debug.Log(distanceToPoint);
        }
        backWardsDestination.y = yValue;
        //Debug.Log("Walking backwards to: " + backWardsDestination + " from: " +transform.position);
        timeoutTimer = 0;

    }

    public void StandStill()
    {
        AnimSelector.StandStill();

    }
    public void StartMoving()
    {
        if(AnimSelector.State == State.Terrified)
        {
            AnimSelector.Terrified();
        }
        else{
            AnimSelector.Walk();
        }
    }

    public override void PauseNavAgent()
    {
        AnimSelector.Pause();
        agent.isStopped = true;
    }
    public override void ResumeNavAgent()
    {
        AnimSelector.Resume();
        agent.isStopped = false;
    }
    public void ClearResponse(Action formerResponse)
    {
        if (Action.Equals(formerResponse, movementDoneRespons))
        {
            movementDoneRespons = null;
            //Debug.Log("Actions match, canceling");
        }
        else
        {
            //Debug.Log("Actions mismatch");

        }
    }

}
