﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class EnemySpecificObjectSpotter : OnUpdate {

	public FloatReference visionAngle;
	public FloatReference visionLenght;
	public FloatReference yValue;
	//public GameObject gameObjectToLookFor;
	Vector3 noiseOrigin;
	Action objectSpottedResponse;
	public override void UpdateEventRaised()
	{
		if (objectSpottedResponse != null)
		{
			CheckVision();
		}
	}

	void CheckVision()
	{
		Vector3 forward = transform.forward;
		
		float  distance = Vector3.Distance(transform.position,noiseOrigin);

		if (distance> visionLenght)
		{
			return;
		}
		Vector3 raycastDirection = (noiseOrigin - transform.position).normalized;
		float angle = Vector3.Angle(transform.forward,raycastDirection);
		
		if(Mathf.Abs(angle) > visionAngle.Value /2)
		{
			return;
		}
		
		RaycastHit hit;
		Physics.Raycast(transform.position,raycastDirection,out hit,distance +0.5f);
		if (hit.collider == null)
		{
			//Theres nothing between you and the origin
			ObjectSpotted();
		}
	}
	
	void ObjectSpotted()
	{
		Debug.Log("Object spotted");
		if (objectSpottedResponse != null)
		{
			Action tempAction = objectSpottedResponse;
			objectSpottedResponse = null;
			tempAction.Invoke();
		}
	}

	public void LookFor(Vector3 noisePosition, Action response)
	{
		objectSpottedResponse = response;
		noiseOrigin = noisePosition;
	}

	public void CancelSpotting(Action prevResponseAction)
	{	
		if (Action.Equals(prevResponseAction,objectSpottedResponse))
		{
			objectSpottedResponse = null;
			//Debug.Log("Actions match, canceling");
		} else {
			//Debug.Log("Actions mismatch");

		}
	}
	
}
