﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyExploding : OnUpdate {

	public EnemyVision enemyVision;
	public FloatReference timeBeforeExplosion;
	EnemyDisabler enemyDisabler;
	bool isExploding;
	float currentTime;
    public Vector3Variable killPosition;
    public VictimHeadAnimatorControl victimHeadControl;
    public SkinnedMeshRenderer headSkinnedMeshRender;
    Color[] headColor;
    public FloatReference explodeColorLerpSpeed;
    public Vector3Variable headExplodeColor;
    Color32 purple;

    private void Start()
    {
        headColor = new Color[3];
        headColor[0] = headSkinnedMeshRender.materials[0].color;
        headColor[1] = headSkinnedMeshRender.materials[1].color;
        headColor[2] = headSkinnedMeshRender.materials[2].color;
        //headMaterial[3] = headSkinnedMeshRender.materials[3];
        purple = new Color32((byte)headExplodeColor.Value.x, (byte)headExplodeColor.Value.y, (byte)headExplodeColor.Value.z, 255);
    }

    public override void UpdateEventRaised() 
	{
		if (isExploding)
		{
			Exploding();
			ShouldExplosionTimerStop();
		}
	}

	void Exploding()
	{
		currentTime += Time.deltaTime;
        
        victimHeadControl.startAnimation();
        victimHeadControl.percentageCompleted = currentTime/ timeBeforeExplosion.Value;
        //Debug.Log("Current Time: " + currentTime);
        //Debug.Log("Until Exposion: " + timeBeforeExplosion.Value);
        //Color32 purple = new Color32(138, 43, 226, 255);        

        for (var i=0; i< headColor.Length; i++)
        {
            headSkinnedMeshRender.materials[i].color = Color.Lerp(headSkinnedMeshRender.materials[i].color, purple, (victimHeadControl.percentageCompleted/explodeColorLerpSpeed.Value));
        }
        
        if (currentTime > timeBeforeExplosion.Value)
		{
			//Debug.Log("COmpete Exp");
			CompletedExploding();
		}
	}
	void CompletedExploding()
	{
		//Debug.Log("STOP EXP");
        AkSoundEngine.PostEvent("StopNPCTerrorLevel",gameObject);
        AkSoundEngine.PostEvent("PCTakedownFinish",gameObject);
        killPosition.SetValue( transform.position);
        gameObject.SetActive(false);
	}
	void ShouldExplosionTimerStop()
	{
		//Debug.Log(enemyVision.GetSpottedObjects().Length);
		if(enemyVision.GetSpottedObjects().Length == 0)
		{
			StopExploding();
		}
		EnemyRigigbodyPushBack rigigbodyPushBack = GetComponent<EnemyRigigbodyPushBack>();
		if (rigigbodyPushBack != null)
		{
			StopExploding();
		}
	}	
	public void StartExploding()
	{
		if( !isExploding )
		{
			isExploding = true;	
			// Debug.Log("Sound: BuildUp 1 ");
			AkSoundEngine.PostEvent("PCTakedownBuildUp",gameObject);
		}

		if (enemyDisabler == null)
		{
			enemyDisabler = gameObject.AddComponent<EnemyDisabler>();
		}
	}
	public void StopExploding()
	{
        //Debug.Log("StopsExploding");
		AkSoundEngine.PostEvent("PCStopTakedownBuildUp",gameObject);
        victimHeadControl.endAnimation();
        isExploding = false;
		currentTime = 0;
		if (enemyDisabler != null)
		{
			enemyDisabler.ActivateEnemy();
		}
        ResetColor();
	}

    private void ResetColor()
    {
        for (var i=0; i< headColor.Length; i++)
        {
            headSkinnedMeshRender.materials[i].color = headColor[i];
        }
    }
}
