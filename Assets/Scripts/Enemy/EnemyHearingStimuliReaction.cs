﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class EnemyHearingStimuliReaction : OnUpdate {
	public EnemyTerrorLevel terrorLevelScript;
	public EnemyMovement enemyMovement;
	public EnemyHead enemyHead;
	public EnemySpecificObjectSpotter objectSpotter;
	Action stimuliReactionDoneResponse;	
	
	
	public FloatReference beginningWaitTime;
	public FloatReference lookingAtSoundOriginTime;
	public FloatReference LookingLeftTime;
	public FloatReference LookingRightTime;
	public FloatReference leftAndRightAngle;
	public FloatReference walkBackwardsDistance;

	Vector3 stimuliPosition;



	bool isScaredReaction = false;
	int reactionStage;
	Vector3 originPosition;
	bool isFirstReaction = true;
	void Awake()
	{
		if (terrorLevelScript == null)
		{
			Debug.Log("Terrorlevel not set, no stimuli reaction will be possible");
		}
		if (enemyMovement == null)
		{
			Debug.Log("enemyMovement not set, no stimuli reaction will be possible");
		}
		if (enemyHead == null)
		{
			Debug.Log("enemyHead not set, no stimuli reaction will be possible");
		}
		
	}
	public void ReactToStimuli(Vector3 stimuliOrigin,Action responseWhenDone )
	{
		//Debug.Log("Reacting to stimuli");
		StartReacting();
		stimuliPosition = stimuliOrigin;
		int terrorLevel = (int)terrorLevelScript.GetTerrorLevel();
		if (terrorLevel < 2)
		{
			DoNormalReaction();
		} else {
			//TODO
			DoScaredReaction();
		}
		if (stimuliReactionDoneResponse != null)
		{
			Action tempAction = stimuliReactionDoneResponse;
			stimuliReactionDoneResponse = responseWhenDone;
			tempAction.Invoke();
		} else {
			stimuliReactionDoneResponse = responseWhenDone;

		}
	}
	void StartReacting()
	{
		if (isFirstReaction)
		{
			originPosition = transform.position;
		}
		enemyHead.ClearResponse(ReactionStageDone);
		enemyMovement.ClearResponse(ReactionStageDone);
		isFirstReaction = false;
		reactionStage = 0;
	}
	void DoNormalReaction()
	{
		isScaredReaction = false;
		DoNextStageOfNormalReaction();
	}
	void DoScaredReaction()
	{
		isScaredReaction = true;
		DoNextStageOfScaredReaction();
	}
	void FinishedReacting()
	{
		isFirstReaction = true;
		if (stimuliReactionDoneResponse != null)
		{
			Action tempAction = stimuliReactionDoneResponse;
			stimuliReactionDoneResponse = null;
			tempAction.Invoke();
		}

	}
	void DoNextStageOfNormalReaction()
	{
		reactionStage++;
		//Debug.Log(reactionStage);
		switch (reactionStage)
		{
			case 1:
			//Stop Moving
			enemyMovement.SetDestination(transform.position,ReactionStageDone,beginningWaitTime);
			break;
			case 2:
			//Turn to face the sound
			//TODO: Implement face turning system
			//break;
			//DoNextStageOfNormalReaction();
			enemyMovement.SetDestination(transform.position,ReactionStageDone);
			break;
			case 3:
			//Walk until sound origin is within vision
			enemyMovement.SetDestination(stimuliPosition,NoResponse);
			objectSpotter.LookFor(stimuliPosition,ReactionStageDone);
			enemyHead.FocusOnPosition(stimuliPosition);
			break;
			case 4:
			//Look straing for X
			enemyMovement.PauseNavAgent();
			enemyHead.LookAt(0,lookingAtSoundOriginTime /3,(lookingAtSoundOriginTime /3)*2,ReactionStageDone);
			//TODO: Implement turning here
			break;
			case 5:
			//Look left for x
			enemyHead.LookAt(-leftAndRightAngle,LookingLeftTime /3,(LookingLeftTime /3)*2,ReactionStageDone);
			break;
			case 6:
			//Look right for seconds
			enemyHead.LookAt(leftAndRightAngle,LookingRightTime /3,(LookingRightTime /3)*2,ReactionStageDone);
			break;
			case 7:
			//Look straigt for seconds
			enemyHead.LookAt(0,lookingAtSoundOriginTime /3,(lookingAtSoundOriginTime /3)*2,ReactionStageDone);
			break;
			case 8:
			//Return to original position before stimuli
			enemyMovement.SetDestination(originPosition,ReactionStageDone);

			break;
			case 9:
			//Give power back to brain
			FinishedReacting();
			break;
		}
	}
	
	
	void DoNextStageOfScaredReaction()
	{
		reactionStage++;
		//Debug.Log("Reaction stage scared: " + reactionStage);
		switch (reactionStage)
		{
			case 1:
			//Stop moving
			enemyMovement.SetDestination(transform.position,ReactionStageDone,beginningWaitTime);
			break;
			case 2:
			//Turn to face the sound
			enemyHead.FocusOnPosition(originPosition);
			enemyMovement.TurnTowards(originPosition,ReactionStageDone,1);
			break;
			case 3:
			//Walk backwards
			enemyMovement.WalkBackwards(walkBackwardsDistance,ReactionStageDone);
			break;
			case 4:
			//Wait for X amount of seconds
			enemyHead.LookAt(0,1,1,ReactionStageDone);
			break;
			case 5:
			enemyHead.LookAt(60,1,1,ReactionStageDone);

			break;
			case 6:
			enemyHead.LookAt(-60,1,1,ReactionStageDone);

			break;
			case 7:
			enemyHead.LookAt(0,0.5f,0.5f,ReactionStageDone);

			break;
			case 8:
			FinishedReacting();
			break;
		}
	}
	
	public void ReactionStageDone()
	{
		//Debug.Log("Reaction stage done");
		if (isScaredReaction)
		{
			DoNextStageOfScaredReaction();
		} else {
			DoNextStageOfNormalReaction();
		}
	}
	public void NoResponse()
	{
		//Debug.Log("movement done");
		objectSpotter.CancelSpotting(ReactionStageDone);
		ReactionStageDone();
		//TODO implement backup, if this is ever called the AI moved in a too weird pattern and never saw the sound origin. Implement a backup system so the ai will still continue
	}
	public override void UpdateEventRaised()
	{

	}
}
