﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyTerrorLevel : OnUpdate {

    public int terrorLevel;
    public FloatReference maxTerrorLevel;
    public FloatReference timeUntilTerrorLevelDecrease;
    public FloatReference unImpressedTimer;
    public GameEvent maxTerrorLevelReached;
    float decreaseCurrentTime;
    float unImpressedCurrentTimer;
    bool isScareable = true;
    public bool terrorLevelShouldDecrease = true;
    public UnityEvent terrorChange;

    void Awake()
    {
        if (timeUntilTerrorLevelDecrease.Value == 0)
        {
            terrorLevelShouldDecrease = false;
        }
    }

    public override void UpdateEventRaised()
    {
        if (terrorLevel != 0 && terrorLevelShouldDecrease)
        {
            decreaseCurrentTime -= Time.deltaTime;
            if (decreaseCurrentTime > timeUntilTerrorLevelDecrease)
            {
                DecreaseTerrorLevel();
                decreaseCurrentTime = 0;
            }
        }
        if (!isScareable)
        {
            unImpressedCurrentTimer += Time.deltaTime;
            if (unImpressedCurrentTimer > unImpressedTimer.Value)
            {
                unImpressedCurrentTimer = 0;
                isScareable = true;
            }
        }

    }


    public float GetTerrorLevel()
    {
        return terrorLevel;
    }

    public void SetTerrorLevel( int p_TerrorLevel )
    {
        terrorLevel = p_TerrorLevel;
        if (terrorLevel == 0)
        {
            AkSoundEngine.SetSwitch("TerrorLevel","Normal",gameObject);

        } else if (terrorLevel == 1)
        {
            AkSoundEngine.SetSwitch("TerrorLevel","Alarmed",gameObject);
        } else if (terrorLevel == 2)
        {
            AkSoundEngine.SetSwitch("TerrorLevel","Scared",gameObject);

        }else if (terrorLevel == 2)
        {
            AkSoundEngine.SetSwitch("TerrorLevel","Broken",gameObject);
        }
        terrorChange.Invoke();
        //Debug.Log("Increasing terror level to: " + terrorLevel);
        //TODO change behaviaur when this is raised
    }

    public void IncreaseTerrorLevel()
    {
        if (isScareable)
        {
            if( terrorLevel < maxTerrorLevel.Value )
                SetTerrorLevel(terrorLevel + 1);
            else{
                //maxTerrorLevelReached.Raise();
            }
            decreaseCurrentTime = 0;
            isScareable = false;
        }
        //Debug.Log(terrorLevel);
    }
    

    public void DecreaseTerrorLevel()
    {
        if( terrorLevel > 0 )
            SetTerrorLevel(terrorLevel - 1);

        decreaseCurrentTime = 0;
        
    }
}
