﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.AI;

public class DetatchedHead : EnemyHead {

	
	private List<Vector3> pointList;
	public Vector3 CurrentCorner;
	private Vector3 currentHeadRotation;
    public Transform neck;

	public override void ClearResponse(Action formerResponse)
	{
		base.ClearResponse(formerResponse);
	}
	public override void ReturnToNormal()
	{}
	public override void LookAt(float angle,float timeUntil, float timeThere, Action responseWhenDone)
	{}
	public override void LookAt(Vector3 direction,float timeUntil, float timeThere, Action responseWhenDone)
	{}
	public override void FocusOnPosition(Vector3 position)
	{}
	public override void UpdateEventRaised()
	{}
	// Use this for initialization
	void Start () {
		// VictimHead = gameObject.transform.root.GetComponent<NavMeshAgent>();
		// VictimHeadTransForm = transform;
		// gameObject.transform.position = VictimHeadTransForm.transform.position;
		// currentHeadRotation = transform.localEulerAngles;
		// pointList = gameObject.transform.root.transform.GetComponent<MMAnimationSelector>().pointsList;
	}
	
	// Update is called once per frame
	void Update () {
		// if(navMeshAgent.hasPath){
		// 	Vector3 firstCorner = navMeshAgent.path.corners[0];
		// 	Debug.Log(firstCorner.ToString());
		// 	LookAt(firstCorner);
		// 	if(firstCorner != CurrentCorner)
		// 	{
		// 		CurrentCorner = firstCorner;
		// 	}
		// }
		// else{
		// 	LookAt(Vector3.zero);
		// }
	}

	/// <summary>
	/// LateUpdate is called every frame, if the Behaviour is enabled.
	/// It is called after all Update functions have been called.
	/// </summary>
	void LateUpdate()
	{
        transform.position = neck.position;
		//LookAt(Vector3.right);
	}

	void LookAt(Vector3 target)
	{
		float angleOffset = Vector3.Angle(transform.forward, target);
		transform.eulerAngles = Vector3.zero;

		// transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.AngleAxis(angleOffset, transform.up), Time.deltaTime);
	}
}
