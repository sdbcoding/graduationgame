﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class EnemyVisionConeDrawer : MonoBehaviour {

    public Transform PatrollingEnemy;
	public FloatReference visionAngle;
	public FloatReference visionLenght;
    public MMAnimationSelector AnimationSelector;
	//public Transform enemy;
	public Image visionConeImage;
	RectTransform rect;
    public float dist;
    public int coneIndex =0;
    public int totalAmountOfCones = 5;
    private float y;
    private float z;
    private float posY;
    private float PrevAngle;



    void Start () 
	{
        rect = GetComponent<RectTransform>();

        rect.RotateAround(rect.position,rect.forward,visionAngle /2-(visionAngle/ totalAmountOfCones) *coneIndex);

        y = rect.localEulerAngles.y;
        z = rect.localEulerAngles.z;

        posY = rect.position.y;

        PrevAngle = PatrollingEnemy.eulerAngles.y;

    }

	void Update() 
	{
        rect.position = new Vector3(PatrollingEnemy.position.x, posY, PatrollingEnemy.position.z);
        //rect.localEulerAngles = new Vector3(-90, y, z);
        List<Vector3> points = AnimationSelector.pointsList;
        float angle = 0f;
        if (points.Count > 0)
        {
            Vector3 point = AnimationSelector.FindNextPoint();
            angle = Mathf.Rad2Deg * Mathf.Atan2(point.x - PatrollingEnemy.position.x, point.z - PatrollingEnemy.position.z);
        }
        float difference = FixAngle(angle - PatrollingEnemy.eulerAngles.y);

        // Firstly we don't want the visioncone to be more than 40 dergrees from the looking direction
        if (difference > 40f)
            angle = PatrollingEnemy.eulerAngles.y + 40;
        else if (difference < -40f)
            angle = PatrollingEnemy.eulerAngles.y - 40;

        // Secondly, we don't want it to rotate more than 2 degrees per frame
        if (FixAngle(angle - PrevAngle) > 2f)
        {
            angle = PrevAngle + 2f;
        } else if (FixAngle(angle - PrevAngle) < -2f)
        {
            angle = PrevAngle - 2f;
        }
            
        rect.eulerAngles = new Vector3(-90, angle, rect.eulerAngles.z);
        rect.localEulerAngles = new Vector3(rect.localEulerAngles.x, rect.localEulerAngles.y + y, rect.localEulerAngles.z);
        PrevAngle = angle;

        RaycastHit hit;
        Physics.Raycast(transform.position, Quaternion.AngleAxis(-(visionAngle / 2 / totalAmountOfCones), rect.forward) * (-rect.up), out hit, 50);
        dist = hit.distance;

        float doubleLenght = visionLenght.Value * 2;
        if (hit.distance < visionLenght.Value)
        {
            doubleLenght = hit.distance * 2;
        }
        rect.sizeDelta = new Vector2(doubleLenght, doubleLenght);
        float newFillAmount = visionAngle / totalAmountOfCones / 360;
        visionConeImage.fillAmount = newFillAmount;
    }

    private float FixAngle(float angle)
    {
        if (angle > 180f)
            angle -= 360f;
        else if (angle < -180f)
            angle += 360f;
        return angle;
    }
}
