﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour {

	public bool isPatrolling = true;
	Vector3 currentDestination;
	public EnemyMovement enemyMovement;
	public FloatReference yValue;
	bool haveSendDestination;
	public EnemyPatrolLookResponse lookResponse;
	[Header("Drop a patrol here")]
	public Patrol patrol;
	bool reachedDestination = false;

	bool lookResponseActive = false;

	//Patrol control stuff
	int currentWaypoint;
	int waypointSign = 1;
	bool patrolIsLooping;
	VectorSet waypointSet;
	float potentialWaitTimer = 0;
	

	
	void Start()
	{
		if (patrol == null)
		{
			Debug.Log("No patrol attached");
		} else {
			//currentDestination = patrol.GetNextWaypointLocation();
			patrolIsLooping = patrol.patrolIsLooping;
			waypointSet = patrol.GetPatrol();
			currentDestination = GetNextWaypointLocation();

		}
	}
	public bool WantControl()
	{
		if (!isPatrolling)
		{
			return false;
		}
		if (patrol == null)
		{
			return false;
		}
		if (patrol.GetPatrol().GetItems().Count == 0)
		{
			return false;
		}
		return true;
	}
	public void UpdateLoop()
	{
		if (lookResponseActive)
		{
			if (lookResponse.LookResponse())
			{
				//Debug.Log("Look reponse is returning true");
				lookResponseActive = false;
			}
			return;
		}
		if (haveSendDestination)
		{
			return;
		}else if (reachedDestination)
		{
			ReachedDestination();
		}
		SendDestionation();
	}
	public void MovementCompletionResponse()
	{
		haveSendDestination = false;
		currentDestination.y = yValue;
		Vector3 transformPosition = transform.position;
		transformPosition.y = yValue;
		float distance = Vector3.Distance(currentDestination,transformPosition);
		reachedDestination = true;
		if(distance <= 0.4f || distance == Mathf.Infinity)
		{
			//currentDestination = patrol.GetNextWaypointLocation();
		}
	}
	void SendDestionation()
	{
		//TODO calculate if the path is possible and then set current destination as the last possible position if path is not possible
		if(potentialWaitTimer != 0)
		{
            Debug.Log("Waittimer applied");
			enemyMovement.SetDestination(currentDestination,MovementCompletionResponse,potentialWaitTimer);

		} else {
			enemyMovement.SetDestination(currentDestination,MovementCompletionResponse);

		}
		haveSendDestination = true;
	}

	void ReachedDestination()
	{
		PatrolWaypoint waypoint = patrol.GetPatrolWaypoint(currentWaypoint);
		switch (waypoint.response)
		{
			case PatrolWaypoint.PatrolResponse.Wait:
			potentialWaitTimer = waypoint.waitTime;
			currentDestination = GetNextWaypointLocation();
			break;
			case PatrolWaypoint.PatrolResponse.Nothing:
			currentDestination = GetNextWaypointLocation();
			break;
			case PatrolWaypoint.PatrolResponse.Look:
			lookResponse.StartLookResponse(waypoint.lookResponse);
			lookResponseActive = true;
			currentDestination = GetNextWaypointLocation();
			break;
		}
		reachedDestination = false;
	}
	//Patrol controls
	public Vector3 GetNextWaypointLocation()
	{
		currentWaypoint += waypointSign;
		if(currentWaypoint == waypointSet.GetItems().Count)
		{
			if (patrolIsLooping)
			{
				currentWaypoint = 0;
			} else {
				waypointSign = -1;
				currentWaypoint -= 2;
			}
		} else if (currentWaypoint == -1)
		{
			waypointSign = 1;
			currentWaypoint = 1;
		}
		return waypointSet.GetItems()[currentWaypoint];
	}

	public void StartPatrol()
	{
		isPatrolling = true;
	}
	public void StopPatrol()
	{
		isPatrolling = false;
	}
	

}
