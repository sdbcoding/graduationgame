﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyHearing : NoiseReceiver {



//Formerly from the Scriptable Object
	public FloatReference yValue;
	public EnemyMovement enemyMovement;
	public EnemyTerrorLevel terrorLevel;
	public EnemyHearingStimuliReaction stimuliReaction;
	
	Vector3 positionOfSound;
	public bool heardSound;
	float timeOfSound;
	
	public float backpedalingDistance = 2;
	bool haveSendDestination = false;
	bool isLookingAtOrigin = false;
	bool newPositionFound = false;
	bool isNewNoise;
	int noiseID;
	void Awake()
	{
		if (enemyMovement == null)
		{
			Debug.Log("Enemy hearing is missing enemy movement");
		}
		if (terrorLevel == null)
		{
			Debug.Log("Enemy hearing is missing terrorLevel");
		}
	}


	public override void ReceiveNoise(Vector3 location,float noiseIntensity)
	{
		positionOfSound = location;
		positionOfSound.y = yValue;
		heardSound = true;
		timeOfSound = Time.fixedUnscaledTime;
		isNewNoise = true;
		//Debug.Log("Noise Received without ID");
	}
	public override void ReceiveNoise(Vector3 location,float noiseIntensity,int id)
	{
		positionOfSound = location;
		positionOfSound.y = yValue;
		heardSound = true;
		timeOfSound = Time.fixedUnscaledTime;
		if (noiseID != id)
		{
			isNewNoise = true;
		}
		//Debug.Log("Noise Received, ID: " + id + " old id was: " + noiseID);
		noiseID = id;
	}
	
	

    public bool WantControl()
    {
		if (haveSendDestination)
		{
			return true;
		}
		if(heardSound && timeOfSound > Time.fixedUnscaledTime -0.5f)
		{
			return true;
		}
		return false;
    }

    public void UpdateLoop()
    {
		if (haveSendDestination)
		{
			 
			if (heardSound)
			{
				ReactToStimuli();
			}
			
			return;
		}
		//SendDestination();
		ReactToStimuli();
		//originOfSound = FindNewVector3ToMoveTo
    }
	void ReactToStimuli()
	{
		if (isNewNoise)
		{
			stimuliReaction.ReactToStimuli(positionOfSound,MovementDoneResponse);
			haveSendDestination = true;
			heardSound = false;
			//Debug.Log("new noise");
			isNewNoise = false;
			terrorLevel.IncreaseTerrorLevel();
		}
	}
	void SendDestination()
	{
		enemyMovement.SetDestination(positionOfSound,MovementDoneResponse);
		haveSendDestination = true;
		heardSound = false;
		terrorLevel.IncreaseTerrorLevel();
	}
	public void MovementDoneResponse()
	{
		haveSendDestination = false;
		//Debug.Log("Sound movement done");
	}
}
