﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDisabler : MonoBehaviour {

	public EnemyVisionConeDrawer[] enemyVisionCones;
	void Start()
	{
		DisableEnemy();
	}
	void DisableEnemy()
	{
		GetComponent<EnemyBrain>().PauseEnemy();
		GetComponent<EnemyMovement>().PauseNavAgent();
		GetComponent<EnemyMouth>().PauseEnemy();
		GetComponent<MMAnimationSelector>().StandStill();
		GetComponent<ReactionController>().ClearCurrentReaction();

		EnemyDisabler[] enemyDisablers = GetComponents<EnemyDisabler>();
		if (enemyDisablers.Length != 1)
		{
			for (int i = 0; i < enemyDisablers.Length; i++)
			{
				if (enemyDisablers[i].enemyVisionCones != null)
				{
					enemyVisionCones = enemyDisablers[i].enemyVisionCones;
					break;
				}
			}
		} else {
			enemyVisionCones = transform.GetComponentsInChildren<EnemyVisionConeDrawer>();
			for (int i = 0; i < enemyVisionCones.Length; i++)
			{
				enemyVisionCones[i].gameObject.SetActive(false);
			}
		}		
	}
	public void ActivateEnemy()
	{
		if (isThereOtherDisablers())
		{
			Destroy(this);
			return;
		}
		GetComponent<EnemyBrain>().ResumeEnemy();
		GetComponent<EnemyMovement>().ResumeNavAgent();
		GetComponent<EnemyMouth>().ResumeEnemy();
		if(enemyVisionCones != null)
		{
			for (int i = 0; i < enemyVisionCones.Length; i++)
			{
				enemyVisionCones[i].gameObject.SetActive(true);
			}
		}
		Destroy(this);
		return;
	}

	bool isThereOtherDisablers()
	{
		EnemyDisabler[] enemyDisablers = GetComponents<EnemyDisabler>();
		//Debug.Log(enemyDisablers.Length);
		if (enemyDisablers.Length == 1)
		{
			return false;
		}
		return true;
	}
}
