﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPushedBack : OnUpdate {

	EnemyDisabler enemyDisabler;
	Vector3 directionToMoveTo;
	Vector3 spotToMoveToo;

	bool isMoving;
	bool isTimeBased;
	float distance;
	float speed;
	float movingCurrentTime;
	Vector3 startPosition;
	LayerMask maskToUse;
    ParticleSystem particleSystemForPushBack;

    //Stunning
    float currentTime;
	float stunTime;	


	//Late update system
	bool doLateUpdateMovement;
	public Vector3 latestPosition;
	public Vector3 nextPosition;

	void Awake () {
		//enemyDisabler = GetComponent<EnemyDisabler>();
		if (enemyDisabler == null)
		{
			enemyDisabler = gameObject.AddComponent<EnemyDisabler>();
		}
	}
	
	public void SetupPushBack(UpdateEvent update, Vector3 direction, float distance, float speed, float newStunTime, bool isTimeBased,LayerMask mask,ParticleSystem particleSystemForPushBack)
	{
		maskToUse = mask;
		updateEvent = update;
		updateEvent.RegisterListener(this);
		directionToMoveTo = direction;
		Vector3 newPosition = transform.position +(directionToMoveTo * distance);
		spotToMoveToo = RaycastNewPosition(newPosition);
		//Debug.Log("Spot to move to: " +spotToMoveToo);
		isMoving = true;
		this.isTimeBased = isTimeBased;
		stunTime = newStunTime;
		this.speed = speed;
        this.particleSystemForPushBack = particleSystemForPushBack;
		this.distance = distance;
		//GetComponent<MMAnimationSelector>().PushBack(stunTime,Nothing);
		ResetValues();
	}
	void ResetValues()
	{
		currentTime = 0;
		movingCurrentTime = 0;
		startPosition = transform.position;
		latestPosition = startPosition;
	}

	Vector3 RaycastNewPosition(Vector3 newPosition)
	{
		RaycastHit hit ;
		Vector3 raycastPosition = transform.position;
		raycastPosition.y = 0.5f;
		Physics.Raycast(raycastPosition,directionToMoveTo,out hit,distance,maskToUse);
		//Debug.DrawRay(raycastPosition,directionToMoveTo,Color.red,5f);
		if (hit.collider == null)
		{
			return newPosition;
		}
		//Debug.Log(hit.collider.gameObject.name + " y: " + transform.position.y);
		return hit.point - directionToMoveTo; 
	}

	public override void UpdateEventRaised()
	{
		if (isMoving)
		{
			Moving();
           
            particleSystemForPushBack.Emit(10);
        } else {
			Stunned();
		}
	}
	void Moving()
	{
		Vector3 newPosition;
		bool isFinished = false;
		if (isTimeBased)
		{
			newPosition = Vector3.Lerp(startPosition,spotToMoveToo,movingCurrentTime / speed);
			if (movingCurrentTime > speed)
			{
				isFinished = true;
			}
		} else {
			newPosition = Vector3.MoveTowards(latestPosition,spotToMoveToo,speed);
			float distanceCheck = Vector3.Distance(newPosition,spotToMoveToo);
			//Debug.Log(distanceCheck);
			if (distanceCheck < 0.02f)
			{
				isFinished = true;
			}
		}
		nextPosition = newPosition;
		doLateUpdateMovement = true;
		//Debug.Log(newPosition + " speed: " + speed);
		//transform.position = newPosition;
		if (isFinished)
		{
			isMoving = false;
		}
	}
	void Stunned()
	{
		currentTime += Time.deltaTime;
		if (currentTime > stunTime)
		{
			FinishedPushBack();
		}
	}
	void FinishedPushBack()
	{
		if(gameObject.activeSelf == true)
		{
			enemyDisabler.ActivateEnemy();
		}
		Destroy(this);
	}
	void LateUpdate()
	{
		if (!doLateUpdateMovement) {return;}

		transform.position = nextPosition;
		latestPosition = nextPosition;
		doLateUpdateMovement = false;

		
	}
	public void Nothing()
	{

	}
}
