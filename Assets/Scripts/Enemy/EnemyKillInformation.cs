﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyKillInformation : MonoBehaviour {
	public FloatReference takedownTime;
	public FloatReference noiseRadius;
	
	public float GetTakedownTime()
	{
		return takedownTime.Value;
	}
	public float GetNoiseRadius()
	{
		return noiseRadius.Value;
	}
}
