﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMouth : MonoBehaviour {

	public EnemyTerrorLevel terrorLevel;
	int prevTerror;

	void Start()
	{
		prevTerror = (int)terrorLevel.GetTerrorLevel();
		ChangeSoundTerrorLevel(prevTerror);
		AkSoundEngine.PostEvent("NPCTerrorLevel",gameObject);
	}
	public void TerrorChange()
	{
		int newTerrorLevel = (int)terrorLevel.GetTerrorLevel();
		ChangeSoundTerrorLevel(newTerrorLevel);
		if (prevTerror < newTerrorLevel)
		{
			IncreaseInTerror();
		} else if (prevTerror >newTerrorLevel)
		{
			DecreaseInTerror();
		}
		prevTerror = newTerrorLevel;
	}
	void ChangeSoundTerrorLevel(int terrorLevel)
	{
		if (terrorLevel == 0)
        {
            AkSoundEngine.SetSwitch("NPCTerrorLevel","Normal",gameObject);

        } else if (terrorLevel == 1)
        {
            AkSoundEngine.SetSwitch("NPCTerrorLevel","Alarmed",gameObject);
        } else if (terrorLevel == 2)
        {
            AkSoundEngine.SetSwitch("NPCTerrorLevel","Scared",gameObject);

        }else if (terrorLevel == 2)
        {
            AkSoundEngine.SetSwitch("NPCTerrorLevel","Broken",gameObject);
        }
	}
	void IncreaseInTerror()
	{
		//Debug.Log("SCARRED");
		AkSoundEngine.PostEvent("NPCScare" ,gameObject); 
	}
	void DecreaseInTerror()
	{

	}
	public void PauseEnemy()
	{
		//TODO: Call a pause event;
	}
	public void ResumeEnemy()
	{
		//TODO: Call a resume event;

	}
}
