﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrolLookResponse : MonoBehaviour {

	public EnemyHead enemyHead;
	public EnemyMovement enemyMovement;
	PatrolWaypointLookResponse lookResponse;
	int currentResponse;
	bool sentDestination;
	bool isFinished;

	public void StartLookResponse(PatrolWaypointLookResponse response)
	{
		lookResponse = response;
		ResetValues();
		enemyMovement.SetDestination(transform.position,HeadResponseDone,lookResponse.waitTimeBeforeLook);
	}

	public bool LookResponse()
	{
		//Will return true when lookResponseIsDone
		if (isFinished)
		{
			return true;
		}
		if (!sentDestination)
		{
			StartNextStage();
		}
		return false;
	}
	void ResetValues()
	{
		currentResponse =-1;
		isFinished = false;
		sentDestination = false;
	}
	
	void StartNextStage()
	{
		
		//Debug.Log("Next stage");
		sentDestination = true;
		currentResponse++;
		enemyHead.LookAt(lookResponse.lookAngles[currentResponse],lookResponse.timeToLookAtAngle[currentResponse],lookResponse.waitTimesBeforeNextLookAngle[currentResponse],HeadResponseDone);
		
	}
	public void HeadResponseDone()
	{
		//Debug.Log("Current response: " + currentResponse + " with lenght being: " + (lookResponse.lookAngles.Length -1));
		if (currentResponse >= lookResponse.lookAngles.Length -1)
		{
			isFinished = true;
		}
		sentDestination = false;
	}
}
