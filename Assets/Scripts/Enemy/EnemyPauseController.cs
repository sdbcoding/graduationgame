﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPauseController : MonoBehaviour {
	public ThingSet enemies;
	
	public void PauseEnemies()
	{
		for (int i = 0; i < enemies.GetItems().Count; i++)
		{
			enemies.GetItems()[i].GetComponent<EnemyMovement>().PauseNavAgent();
		}
	}
	public void StartEnemeies()
	{
		for (int i = 0; i < enemies.GetItems().Count; i++)
		{
			enemies.GetItems()[i].GetComponent<EnemyMovement>().ResumeNavAgent();
		}
	}
}
