﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRigigbodyPushBack : MonoBehaviour {
	Rigidbody rb;
	EnemyDisabler enemyDisabler;

	bool forceHaveBeenAdded = false;
	float minVelocity = 0.01f;
	float currentTime = 0;
	float timeToReach = 0.3f;
	Vector3 prevLocation;

	//Send stuff
    ParticleSystem particleSystemForPushBack;
	
	void Awake () {
		rb = gameObject.AddComponent<Rigidbody>();
		SetupRigigbody();
		prevLocation = transform.position;
		if (enemyDisabler == null)
		{
			enemyDisabler = gameObject.AddComponent<EnemyDisabler>();
		}
	}
	
	public void SetupPushBack(Vector3 force, ParticleSystem particleSystemForPushBack)
	{
		//Debug.Log("Getting new force: " + force);
		GetComponent<Animator>().applyRootMotion = false;
		forceHaveBeenAdded = true;
		rb.velocity = Vector3.zero;
		rb.AddForce(force,ForceMode.Impulse);
        this.particleSystemForPushBack = particleSystemForPushBack;
        particleSystemForPushBack.Emit(10);


    }

	void SetupRigigbody()
	{
		rb.drag = 3;
		rb.mass = 5;
		rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
	}

	void RemoveThis()
	{
		enemyDisabler.ActivateEnemy();
		GetComponent<Animator>().applyRootMotion = true;

		Destroy(rb);
		Destroy(this);
	}
	// Update is called once per frame
	void Update () {
		if (!forceHaveBeenAdded){return;}
		float distance = Vector3.Distance(transform.position,prevLocation);


		if (distance < minVelocity)
		{
			currentTime += Time.deltaTime;
			if( currentTime > timeToReach)
			{
				// Debug.Log("Curret time reached");
				RemoveThis();

			}
		} else {
			currentTime = 0;
		}
		prevLocation = transform.position;

		//float distance = rb.velocity.magnitude;
		//Debug.Log("Magnitude: " + distance);
		
		
	}
}
