﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointOfInterest : MonoBehaviour {

    public float radius = 5;
    public Vector3Variable killPosition;
    public bool challengeCompleted = false;
    public FloatVariable score;
    public ThingSet pointOfInterestSet;
    public RitualUIObject ritualImage;

	// Use this for initialization
	void Start () {
		
	}


    public void OnKill()
    {

        if (Vector3.Distance(transform.position, killPosition.Value) <= radius && !challengeCompleted)
        {
            GetComponent<Renderer>().material.color = Color.green;
            challengeCompleted = true;
            score.ApplyChange(30);
            pointOfInterestSet.Raise();
            ritualImage.Activate();
        }


    }

    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, radius);
    }

}
