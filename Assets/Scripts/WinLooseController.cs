﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinLooseController : MonoBehaviour {

	[SerializeField]
	ThingSet enemies;
	private bool allDead;

	void Awake() 
	{
		allDead = false;	
	}
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		allDead = true;
		foreach (Thing enemy in enemies.GetItems())
		{
			if( enemy.gameObject.activeSelf )
				allDead = false;
		}	
		if( allDead )
			SceneManager.LoadScene("MainMenu");

	}
}
