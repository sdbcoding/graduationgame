﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodePhysics : MonoBehaviour 
{
	public float power = 40.0f;
	public float radius = 10.0f;
	public float upForce = 2f;
	Collider[] colliders;
	
	[SerializeField]
	Vector3Variable explosionPosition;

	Rigidbody rb;
	Destruction destruction;
	GameObject combinedObject;
	public void Explode() 
	{
		// Debug.Log("Collision detected");
		// Vector3 explosionPos = transform.position;
		colliders = Physics.OverlapSphere(explosionPosition.Value, radius);
		foreach (Collider item in colliders)
		{	
			// Debug.Log("Exploding");
			rb = item.GetComponent<Rigidbody>();
			if( rb )
			{
				// Debug.Log("RB: "+ rb.name);
				rb.AddExplosionForce(power, explosionPosition.Value, radius, upForce, ForceMode.Impulse);
			}
		}
	}

	private int LayerToNumber(int layer)
	{
		int layerNumber = 0;
		while(layer > 0)
		{
			layer = layer >> 1;
			layerNumber++;
		}
		return layerNumber-1; 
	}
}
