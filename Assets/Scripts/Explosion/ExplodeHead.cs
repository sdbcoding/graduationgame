﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeHead : MonoBehaviour {

	[SerializeField]
	GameObject head;
	[SerializeField]
	GameObject brokenHead;
	[SerializeField]
	ThingSet player;
	[SerializeField]
	float explodingRadius;

	[SerializeField]
	float power = 5.0f;
	[SerializeField]
	float radius = 10.0f;
	[SerializeField]
	float upForce = 2f;
	bool exploded;
	// Use this for initialization
	void Start () {
		exploded = false;
	}
	
	// Update is called once per frame
	void Update () {

		// if( !exploded )
		// 	if( Vector3.Distance(player.Items[0].transform.position, transform.position) < explodingRadius )
		// 	{
		// 		Explode();
		// 		exploded = true;
		// 	}
		
	}

	public void Explode()
	{
		Debug.Log("Explode");
		if( !exploded )
		{
			Debug.Log("Exploding!");
			head.SetActive(false);
			GameObject temp = Instantiate(brokenHead);
			temp.transform.Translate(head.transform.position) ;

			foreach (Rigidbody rb in temp.GetComponentsInChildren<Rigidbody>())
			{
				Debug.Log("RigidBody: "+ rb.name);
				// rb.AddForce( transform.up * power, ForceMode.Force);
				rb.AddExplosionForce(power, transform.up, radius, upForce, ForceMode.Impulse);	
			}
			exploded = true;
		}

	}


}
