﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class AlphaChanger : MonoBehaviour {

	float step;
	Action responseWhenDone;
	float currentAlpha;
	Image image;
	public void StartChange(int sign,float timeToReach, Action response)
	{
		image = GetComponent<Image>();
		step = Time.fixedDeltaTime /timeToReach;
		step = step * sign;
		//Debug.Log("Step: " +step);
		currentAlpha = image.color.a;
		responseWhenDone = response;
		InvokeRepeating("ChangeAlpha", Time.fixedDeltaTime,Time.fixedDeltaTime);
	}
	void ChangeAlpha()
	{
		Color newColor = image.color;
		currentAlpha += step;
		newColor.a = currentAlpha;
		image.color = newColor;
		if (currentAlpha < 0)
		{
			Stop();
		} else if (currentAlpha > 1)
		{
			Stop();
		}
	}
	void Stop()
	{
			CancelInvoke("ChangeAlpha");
			responseWhenDone.Invoke();
			Destroy(this);
	}
}
