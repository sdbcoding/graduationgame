﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class UpdateImageFillFromFloatVariable : MonoBehaviour {

	Image imageComponent;
	public FloatReference maxValue;
	public FloatReference currentValue; 

	// Use this for initialization
	void Awake () {
		imageComponent = GetComponent<Image>();
	}
	
	public void UpdateImage()
	{
		imageComponent.fillAmount = currentValue / maxValue;
	}
}
