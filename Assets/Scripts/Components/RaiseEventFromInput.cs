﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
[Serializable]
public class RaiseEventFromInput : MonoBehaviour {

	public bool UseGameEvent = true;
	public UnityEvent unityEvent;
	public GameEvent Event;
	public bool UseConstant = true;
	public string ConstantValue;
	public KeyCode Variable;

	void Update()
	{
		if (UseConstant)
		{
			if (Input.GetKeyDown(ConstantValue))
			{
				Raise();
			}
		} else {
			if (Input.GetKeyDown(Variable))
			{
				Raise();
			}
		}
	}
	void Raise()
	{
		if (UseGameEvent)
		{
				Event.Raise();
		} else 
		{
			unityEvent.Invoke();
		}
	}
}
