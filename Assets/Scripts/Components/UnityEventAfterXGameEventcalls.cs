﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UnityEventAfterXGameEventcalls : MonoBehaviour {
	int numberOfCalls;
	public int callsToReach;
	public UnityEvent response;
	public void Call()
	{
		numberOfCalls ++;
		if (numberOfCalls == callsToReach)
		{
			response.Invoke();
		}
	}
}
