﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IncreaseDecreaseAlpha : MonoBehaviour {
	Image currentImageBeingChanged;
	public void DecreaseAlpha(Image image)
	{
		image.gameObject.AddComponent<AlphaDecreaser>();
	}
	public void IncreaseAlpha(Image image)
	{
		image.gameObject.AddComponent<AlphaIncreaser>();
		
	}

	
}
