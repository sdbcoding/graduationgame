﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextAlphaIncreaser : MonoBehaviour {

	public float speed = 1;
	Text textToChange;
	// Use this for initialization
	void Start () {
		textToChange = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		Color newColor = textToChange.color;
		newColor.a += speed * Time.deltaTime;
		textToChange.color = newColor;
		if (newColor.a > 1f)
		{
			newColor.a = 1;
			textToChange.color = newColor;
			Destroy(this);
		}
	}
}

