﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlphaDecreaser : MonoBehaviour {

	Image imageToChange;
	// Use this for initialization
	void Start () {
		imageToChange = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		Color newColor = imageToChange.color;
		newColor.a -= 1 * Time.deltaTime;
		imageToChange.color = newColor;
		if (newColor.a < 0.0f)
		{
			newColor.a = 0;
			imageToChange.color = newColor;
			Destroy(this);
		}
	}
}
