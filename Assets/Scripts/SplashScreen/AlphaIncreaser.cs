﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlphaIncreaser : MonoBehaviour {
	public float speed = 1;
	Image imageToChange;
	// Use this for initialization
	void Start () {
		imageToChange = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		Color newColor = imageToChange.color;
		newColor.a += speed * Time.deltaTime;
		imageToChange.color = newColor;
		if (newColor.a > 1f)
		{
			newColor.a = 1;
			imageToChange.color = newColor;
			Destroy(this);
		}
	}
}
