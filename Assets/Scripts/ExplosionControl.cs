﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionControl : MonoBehaviour {

    public ParticleSystem particleLauncher;
    ParticleSystem impactParticle;
    public ParticleDecalPool splatDecalPool;
    public bool fire = false;
    public Vector3Variable killPosition;
    public Vector3Variable pOIPos;
    public GameObjectVariable enemyToKill;
    bool haveEmitted = false;

    List<ParticleCollisionEvent> collisionEvents;

	// Use this for initialization
	void Start () {

        collisionEvents = new List<ParticleCollisionEvent> ();
	}

    private void OnParticleCollision(GameObject other)
    {
        ParticlePhysicsExtensions.GetCollisionEvents(particleLauncher, other, collisionEvents);

        for (int i = 0; i < collisionEvents.Count; i++)
        {
            splatDecalPool.ParticleHit(collisionEvents[i]);
        }
    }

    public void EmitBurst()
    {
        if (haveEmitted) {return;}
        Invoke("SetHaveEmittedToFalse",0.5f);
        haveEmitted = true;
        transform.position = killPosition.Value+ new Vector3(0,2.8f,0);
        particleLauncher.Emit(60);
        // Debug.Log("Sound: Explosion");
        AkSoundEngine.PostEvent("StopNPCTerrorLevel",enemyToKill.Value);	
        
        AkSoundEngine.PostEvent("PCTakedownFinish",enemyToKill.Value);	
        AkSoundEngine.PostEvent("CombatMusic",gameObject);
    }


    public void EmitSmallBurst()
    {
        if (haveEmitted) { return; }
        Invoke("SetHaveEmittedToFalse", 0.01f);
        haveEmitted = true;
        transform.position = pOIPos.Value + new Vector3(0, 2, 0);
        particleLauncher.Emit(5);
       
        //AkSoundEngine.PostEvent("StopNPCTerrorLevel", gameObject);

       AkSoundEngine.PostEvent("PCTakedownFinish", gameObject);
       // AkSoundEngine.PostEvent("CombatMusic", gameObject);
    }


    void SetHaveEmittedToFalse()
    {
        haveEmitted = false;
    }

   
}
