﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPlayerSpawning : OnUpdate {

	public Vector3Variable offset;
	public Vector3Variable playerSpawnPosition;
	public Vector3Variable overviewPosition;
	public FloatReference playerSpawnTime;
	float currentTime;
	Vector3 endPosition;
	bool isRunning;

	
	
	void Update () {
		if (!isRunning) {return;}
		currentTime += Time.deltaTime;
		Moving();
		if(currentTime > playerSpawnTime.Value)
		{
			Destroy(this);
		}
	}
	void Moving()
	{
		Vector3 newPosition = Vector3.Lerp(overviewPosition.Value,endPosition,currentTime / playerSpawnTime.Value);
		transform.position = newPosition;
	}

	public void PlayerSpawning()
	{
		endPosition = playerSpawnPosition.Value + offset.Value;
		isRunning = true;
	}
}
