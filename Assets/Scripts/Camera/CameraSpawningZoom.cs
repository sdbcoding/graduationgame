﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSpawningZoom : OnUpdate {
	// public Vector3Variable overviewPosition;
	public Vector3Variable offset;
	public Vector3Variable playerSpawnPosition;
	public GameObjectVariable enemyToKill;
	public Camera mainCamera;
	public GameEvent playerSpawned;
	float currentTime;
	Vector3 endPosition;
	Vector3 closeUpPosition;
	Vector3 direction;

	bool isRunning;
	int stage = 0;
	float standardFOV;
	//timings
	public float zoomInDistance = 10;
	public float fovGrowTime = 0.5f;
	public float fovNewValue = 45;
	public float fovLowerTime = 0.8f;
	public float timeToMoveCloser = 1.2f;
	bool startSpawning = true;

	public void PlayerSpawning()
	{
		endPosition = enemyToKill.Value.transform.position + offset.Value;
		//direction = (endPosition - transform.position).normalized;
		direction = (enemyToKill.Value.transform.position - endPosition).normalized;
		closeUpPosition = endPosition + (direction * zoomInDistance);
		isRunning = true;
		if( startSpawning )
		{
			// Debug.Log("Sound: BuildUp 1 ");
			// AkSoundEngine.PostEvent("PCTakedownBuildUp",gameObject);
			startSpawning = false;
		}
	}

	// Use this for initialization
	void Start () {
		standardFOV = mainCamera.fieldOfView;
	}
	
	public override void UpdateEventRaised()
	{
		if (!isRunning) {return;}
		DoStage();
	}
	void DoStage()
	{
		currentTime += Time.deltaTime;
		switch (stage)
		{
			case 0:
			GrowFov();
			break;
			case 1:
			LowerFov();
			MoveCameraCloserExponential();
			break;
			case 2:
			playerSpawnPosition.SetValue(enemyToKill.Value.transform.position);
			playerSpawned.Raise();
			Destroy(this);
			return;
			/* break;
			case 4:
			break;
			case 5:
			break;
			case 6:
			break;
			*/
		}
	}
	void GrowFov()
	{
		mainCamera.fieldOfView = standardFOV + ((fovNewValue-standardFOV) * (currentTime /fovGrowTime));
		if (currentTime > fovGrowTime)
		{
			currentTime = 0;
			ChangeStage();
		}
	}
	void LowerFov()
	{
		if (currentTime < fovLowerTime)
		{
			mainCamera.fieldOfView = fovNewValue - ((fovNewValue-standardFOV) * (currentTime /fovLowerTime));
		}
	}
	void MoveCameraCloserExponential()
	{
		Vector3 newPosition = Vector3.Slerp(transform.position,closeUpPosition,currentTime / timeToMoveCloser);
		transform.position = newPosition;
		if (currentTime > timeToMoveCloser)
		{
			transform.position = newPosition;
			ChangeStage();
		}
	}
	void ChangeStage()
	{
		stage++;
	}
}
