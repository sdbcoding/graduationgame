﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraWholeLevelVisible : MonoBehaviour {

	public Vector3Variable overviewPosition;
	void Awake () {
		overviewPosition.SetValue(transform.position);
	}	
}
