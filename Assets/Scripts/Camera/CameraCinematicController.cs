﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraCinematicController : MonoBehaviour
{
	[SerializeField]
	ThingSet poinstOfInterest; 
	[SerializeField] 
	MouseInputGetter mouseInput; 
	[SerializeField]
	TouchInputGetter touchInput; 

	[SerializeField]
	float panTime = 3.0f; 
	[SerializeField]
	float waitTime = 1.0f;
	[SerializeField]
	bool isWaiting = true; 
	[SerializeField] 	
	bool isRunning = true;
	bool doubleTapped = false;
	[SerializeField]
	Vector3Variable camOffsetFromPoint; 
	[SerializeField]
	Vector3Variable prespawnPosition;
	
	[SerializeField]
	Text doubleTapText;
	
	[SerializeField]
	float flashSpeed = 0.02f;
	// [SerializeField]
	// BooleanVariable showCinematicBoolVar;
	// [SerializeField]
	// BooleanVariable neverShowCinematicBoolVar;

	float t = 0.0f; Vector3 nextCameraPos; bool finalMoveStart = true; int pointIndex = 0; bool sleepRunning = false; 
	Color doubleTapTextColor; bool alphaUpwards = true; float alphaValue;


	// Use this for initialization
	void Start ()
	{
		// doubleTapText = doubleTapCanvas.GetComponentInChildren<Text>();
		doubleTapTextColor = doubleTapText.color;
		alphaValue = doubleTapTextColor.a;
		mouseInput.enabled = false;
		touchInput.enabled = false;
		nextCameraPos = poinstOfInterest.GetItems()[pointIndex].gameObject.transform.position + camOffsetFromPoint.Value;	
	}

	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if( IsDoubleTap() )
		{
			doubleTapped = true;
		}
		if( PlayerPrefs.GetInt("showCinematics",1)==0 || doubleTapped )
		{	
			GoToPrespawnPosition();
		}
		else{
			if( isRunning )
			{
				// Wait until camera has adjusted initial position
				if( isWaiting )
				{
					if( !sleepRunning )
						StartCoroutine( Sleep(waitTime) );
				}
				else
				{
					// Debug.Log( "PI: " + pointIndex +" and Set size: "+ poinstOfInterest.GetItems().Count);
					if( pointIndex < poinstOfInterest.GetItems().Count )
					{
						if( Vector3.Distance(transform.position, nextCameraPos) < 0.1f )
						{
							// Debug.Log("Stop");	
							isWaiting = true;
							pointIndex++;
							t = 0.0f;
							if( pointIndex < poinstOfInterest.GetItems().Count )
								nextCameraPos = poinstOfInterest.GetItems()[pointIndex].gameObject.transform.position + camOffsetFromPoint.Value;
						}
							
						t += Time.deltaTime/panTime;		
						// Debug.Log("Moving time: "+ t);
						transform.position = Vector3.Lerp( transform.position, nextCameraPos, Mathf.SmoothStep(0.0f, 1.0f, t ));
					}
					else
					{
						// Reset time
						if ( finalMoveStart ){t = 0.0f;finalMoveStart = false;}
						GoToPrespawnPosition();
					}
				}
			}
		}
		UpdateTextAlpha();
	}

	private void GoToPrespawnPosition()
	{
		if( isRunning )
		{
			if( Vector3.Distance(transform.position, prespawnPosition.Value) > 0.1f )
			{
				t += Time.deltaTime/panTime;			
				transform.position = Vector3.Lerp( transform.position, prespawnPosition.Value, Mathf.SmoothStep(0.0f, 1.0f, t ));
			}
			else
			{
				// Debug.Log("Finsish");	
				mouseInput.enabled = true;
				touchInput.enabled = true;
				isRunning = false;
				doubleTapText.transform.parent.gameObject.SetActive(false);
				Destroy(this); // Finish camera movement
			}
		}
	}

	private void UpdateTextAlpha()
	{
		if( alphaValue <= 0f ){alphaUpwards = true;}
		else if( alphaValue >= 1f) {alphaUpwards = false;}

		alphaValue = alphaUpwards ? alphaValue+flashSpeed : alphaValue-flashSpeed;
		
		doubleTapTextColor.a = alphaValue;
		doubleTapText.color = doubleTapTextColor;
	}

	IEnumerator Sleep( float pWaitTime )
    {
		sleepRunning = true;
		// Debug.Log("Start time: " +Time.time);
        yield return new WaitForSeconds(pWaitTime);
		// Debug.Log("Finish time: " +Time.time);
		isWaiting = false;
		sleepRunning = false;
    }

	public void StopCinematic()
	{
		isRunning = false;
	}
	public void ResumeCinematic()
	{
		isRunning = true;
	}
	
	private static bool IsDoubleTap()
	{
		bool result = false;
		float MaxTimeWait = 2;
		if( Input.touchCount == 1  && Input.GetTouch(0).phase == TouchPhase.Began)
		{
			float DeltaTime = Input.GetTouch (0).deltaTime;
			float DeltaPositionLenght=Input.GetTouch (0).deltaPosition.magnitude;

			if ( DeltaTime> 0 && DeltaTime < MaxTimeWait )
				result = true;                
		}
		return result;
     }


}
