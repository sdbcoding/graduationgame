﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CameraClickParticleEffects : MonoBehaviour {

	public Camera mainCamera;
	public ParticleSystem particleSystem;
	public List<ParticleSystem> activeSystems;
	public GraphicRaycaster uiRaycaster; 
	public EventSystem eventSystem;
	public PointerEventData pointerEventData;

	void Start()
	{
		if (mainCamera == null)
		{
			mainCamera = Camera.main;
		}
		if (uiRaycaster == null)
		{
			uiRaycaster = GetComponent<GraphicRaycaster>();
		}
	}
	// Update is called once per frame
	void Update () {
		
		if (Input.GetMouseButtonDown(0) || Input.touchCount != 0)
		{
			//Debug.Log("Firering");
			FindParticleSystem();
			if (!RayCastHitLevel())
			{
				if (!UIRaycasting())
				{
					AkSoundEngine.PostEvent("UINoClick", gameObject);

				}
			}
			
		}
	}


	bool UIRaycasting()
	{
		List<RaycastResult> results = new List<RaycastResult>();
		pointerEventData = new PointerEventData(eventSystem);
		pointerEventData.position = Input.mousePosition;
		uiRaycaster.Raycast(pointerEventData,results);
		if (results.Count == 0)
		{
			return false;
		}
		return true;
	}
	bool RayCastHitLevel()
	{
		Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		Physics.Raycast(ray,out hit);
		if (hit.collider == null)
		{
			return false;
		}
		if (hit.collider.gameObject.GetComponent<Level>() == null)
		{
			return false;
		}
		return true;
	}
	void mouseClick()
	{
		Vector3 newPosition;
		Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
		newPosition = ray.origin + ray.direction;



		//newPosition = Camera.main.ScreenToViewportPoint( Input.mousePosition);
		//newPosition += (Camera.main.transform.forward * 2) + Camera.main.transform.position;
		//Debug.Log("Mouse: " + Input.mousePosition + " camera: " +newPosition);
		transform.position = newPosition;
		transform.forward = -mainCamera.transform.forward;
		particleSystem.Play();
	}

	void FindParticleSystem()
	{
		
		for (int i = 0; i < activeSystems.Count; i++)
		{
			if (!activeSystems[i].isPlaying)
			{
				PlayParticleSystem(activeSystems[i]);
				return;
			}
		}
		GameObject temp = Instantiate(particleSystem.gameObject);
		activeSystems.Add(temp.GetComponent<ParticleSystem>());
		PlayParticleSystem(activeSystems[activeSystems.Count-1]);
	}
	void PlayParticleSystem(ParticleSystem system)
	{
		Vector3 newPosition;
		Ray ray;
		if (Input.touchCount == 0)
		{
			ray = mainCamera.ScreenPointToRay(Input.mousePosition);

		} else {
			ray = mainCamera.ScreenPointToRay(Input.GetTouch(0).position);
		}
		newPosition = ray.origin + ray.direction;



		//newPosition = Camera.main.ScreenToViewportPoint( Input.mousePosition);
		//newPosition += (Camera.main.transform.forward * 2) + Camera.main.transform.position;
		//Debug.Log("Mouse: " + Input.mousePosition + " camera: " +newPosition);
		system.transform.position = newPosition;
		system.transform.forward = -mainCamera.transform.forward;
		system.Play();
	}
}
