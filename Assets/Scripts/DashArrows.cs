﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DashArrows : OnUpdate
{
    public Image leftTop;
    public Image rightTop;
    public Image leftBottom;
    public Image rightBottom;
    public DashReloadedGrow topImageFull;
    public DashReloadedGrow bottomImageFull;
    public FloatReference currentDashPercentage;

    bool topReady = false;
    bool bottomReady = false;
    void Start()
    {
        if (currentDashPercentage == 100f)
        {
            topReady = true;
            bottomReady = true;
        }
    }

    public override void UpdateEventRaised()
    {
        if (currentDashPercentage.Value < 50f)
        {
            leftTop.fillAmount = 0;
            rightTop.fillAmount = 0;
            leftBottom.fillAmount = 2*(currentDashPercentage.Value / 100f);
            rightBottom.fillAmount = 2*(currentDashPercentage.Value / 100f);

            leftTop.color = Color.grey;
            rightTop.color = Color.grey;
            leftBottom.color = Color.grey;
            rightBottom.color = Color.grey;
            bottomReady = false;
        }
        else
        {
            if (!bottomReady)
            {
                bottomReady = true;
                bottomImageFull.Activate();
            }
            leftTop.fillAmount = 2*((currentDashPercentage.Value/100f)-0.5f);
            rightTop.fillAmount = 2 * ((currentDashPercentage.Value / 100f) - 0.5f);
            leftBottom.fillAmount = 1f;
            rightBottom.fillAmount = 1f;
            leftTop.color = Color.grey;
            rightTop.color = Color.grey;
            leftBottom.color = Color.white;
            rightBottom.color = Color.white;
            if (currentDashPercentage.Value == 100f)
            {
                if (!topReady)
                {
                    topReady = true;
                    topImageFull.Activate();
                }
                leftTop.color = Color.white;
                rightTop.color = Color.white;
            } else {
                topReady = false;
            }
        }
        
    }
}
