﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ColorChanger 
{
	public static void ChangeColor( GameObject pElement, Color pColor)
	{
		foreach (Renderer r in pElement.GetComponentsInChildren<Renderer>())
		{
			r.material.color = pColor;
		} 
		if( pElement.GetComponent<Renderer>() )
		{
			pElement.GetComponent<Renderer>().material.color = pColor;
		}
	}
}
