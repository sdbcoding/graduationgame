﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour {

	public List<Transform> patrolPoints;
	public VectorSet waypointSet;
	GameObject waypoint;
	public bool patrolIsLooping = true;
	int currentWaypoint;
	int waypointSign = 1;
	
	public void SpawnWayPoint()
	{
		waypoint = Resources.Load("EditorResources/Waypoint") as GameObject;
		if (patrolPoints == null)
		{
			patrolPoints = new List<Transform>();
		}

		patrolPoints.Add(Instantiate(waypoint,transform.position + transform.forward,Quaternion.identity).transform);
		patrolPoints[patrolPoints.Count -1 ].SetParent(transform);
	}
	public void DeleteLastWayPoint()
	{
		if (patrolPoints == null)
		{
			return;
		}
		if (patrolPoints.Count == 0)
		{
			return;
		}
		patrolPoints.RemoveAt(patrolPoints.Count - 1);
	}
	void Awake()
	{
		UpdateVectorSetFromTransforms();
	}
	void UpdateVectorSetFromTransforms()
	{
		if (patrolPoints == null || patrolPoints.Count == 0) 
		{
			//Debug.Log("No patrol transform created");
			for (int i = 0; i < transform.childCount; i++)
			{
				patrolPoints.Add(transform.GetChild(i));
			}
			if (patrolPoints == null)
			{
				return;
			}
			//Debug.Log("Found children, adding them");
		}
		if (waypointSet == null)
		{
			waypointSet = ScriptableObject.CreateInstance("VectorSet") as VectorSet;
		}
		waypointSet.SetItems(new List<Vector3>());
		for (int i = 0; i < patrolPoints.Count; i++)
		{
			waypointSet.GetItems().Add(patrolPoints[i].position);
		}
	}
	public VectorSet GetPatrol()
	{
		return waypointSet;
	}
	public Vector3 GetNextWaypointLocation()
	{
		currentWaypoint += waypointSign;
		if(currentWaypoint == waypointSet.GetItems().Count)
		{
			if (patrolIsLooping)
			{
				currentWaypoint = 0;
			} else {
				waypointSign = -1;
				currentWaypoint -= 2;
			}
		} else if (currentWaypoint == -1)
		{
			waypointSign = 1;
			currentWaypoint = 1;
		}
		return waypointSet.GetItems()[currentWaypoint];
	}
	public PatrolWaypoint GetPatrolWaypoint(int patrolWaypointIndex)
	{
		PatrolWaypoint waypoint = patrolPoints[patrolWaypointIndex].GetComponent<PatrolWaypoint>();
		if (waypoint != null)
		{
			return waypoint;
		}
		return new PatrolWaypoint();
	}
}
