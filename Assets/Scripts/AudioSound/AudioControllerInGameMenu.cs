﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioControllerInGameMenu : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void PauseAll()
	{
		AkSoundEngine.PostEvent( "PauseAll", gameObject);
	}
	public void ResumeAll()
	{
		AkSoundEngine.PostEvent( "ResumeAll", gameObject);
		AkSoundEngine.PostEvent( "PauseMenuMusic", gameObject);
	}
}
