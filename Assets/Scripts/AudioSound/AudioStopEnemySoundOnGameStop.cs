﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioStopEnemySoundOnGameStop : MonoBehaviour {

	public ThingSet enemies;
	GameObject[] enemyObjs;

	
	void Start()
	{
		enemyObjs = new GameObject[enemies.GetItems().Count];
		for (int i = 0; i < enemies.GetItems().Count; i++)
		{
			enemyObjs[i] = enemies.GetItems()[i].gameObject;
		}
	}

	public void StopSounds()
	{
		for (int i = 0; i < enemyObjs.Length; i++)
		{
        	AkSoundEngine.PostEvent("StopNPCTerrorLevel",enemyObjs[i]);	
		}
	}
}
