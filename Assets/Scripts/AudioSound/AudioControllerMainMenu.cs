﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioControllerMainMenu : MonoBehaviour 
{
	public static AudioControllerMainMenu instance;
	[SerializeField]
	GameObject menuMusic;
	void Awake()
	{	
		if(instance == null)
		{
			DontDestroyOnLoad(menuMusic);
			instance = this;
		} 
		else 
		{
			Destroy(gameObject);
		}
	}

	// Use this for initialization
	void Start () 
	{
		//AkSoundEngine.PostEvent("MenuMusic", gameObject);
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void PauseMenuMusic()
	{
		AkSoundEngine.PostEvent("PauseMenuMusic", gameObject);
	}

	public void ResumeMenuMusic()
	{
		AkSoundEngine.PostEvent("ResumeMenuMusic", gameObject);
	}
}
