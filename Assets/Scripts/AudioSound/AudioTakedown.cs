﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTakedown : MonoBehaviour {
	public ThingSet playerSet;
	public FloatReference numberOfSigns;
	public FloatReference clearedNumberOfSigns;
	GameObject player;
/*
PCTakedownBuildUp1
PCTakedownBuildUp2
PCTakedownBuildUp3
PCStopTakedownBuildUp
PCTakedownFinish
 */
	void Start()
	{
		player = playerSet.GetItems()[0].gameObject;
	}
	public void SignDashing()
	{
		float value = clearedNumberOfSigns /numberOfSigns;
		//Debug.Log(value);
		if (value > 0.70f)
		{
			AkSoundEngine.PostEvent("PCTakedownBuildUp3",player);

		} else if (value > 0.40f)
		{
			AkSoundEngine.PostEvent("PCTakedownBuildUp2",player);

		} else {
			AkSoundEngine.PostEvent("PCTakedownBuildUp1",player);

		}
	}
	public void KillCanceled()
	{
		AkSoundEngine.PostEvent("PCStopTakedownBuildUp",player);

	}
	public void KillFinished()
	{
		AkSoundEngine.PostEvent("PCTakedownFinish",player);
	}
}
