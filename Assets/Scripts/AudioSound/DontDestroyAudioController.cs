﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyAudioController : MonoBehaviour 
{
	void Awake() 
	{	
		Debug.Log("Dont destroy");
		DontDestroyOnLoad(gameObject);
	}
	// Use this for initialization
	void Start () 
	{		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void DestroyUIAudio(float seconds)
	{
		StartCoroutine(DestroyAfter(seconds));
		// TODO Stop interactive 
		AkSoundEngine.PostEvent("StopEldritchPresence",gameObject);
	}

	IEnumerator DestroyAfter( float seconds )
    {
		yield return new WaitForSeconds(seconds);
		Destroy(gameObject);
	}
}
