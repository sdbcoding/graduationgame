﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnCollison : MonoBehaviour {

    public GameObject obj;
    public string eventName;

	// Use this for initialization
	void Start () {
		if (obj == null)
        {
            obj = gameObject;
        }

        if (eventName == "")
        {
            Debug.Log("please set event name");
        }
	}

	/// <summary>
	/// OnCollisionEnter is called when this collider/rigidbody has begun
	/// touching another rigidbody/collider.
	/// </summary>
	/// <param name="other">The Collision data associated with this collision.</param>
	void OnCollisionEnter(Collision other)
	{
		AkSoundEngine.PostEvent(eventName, obj);
	}
}
