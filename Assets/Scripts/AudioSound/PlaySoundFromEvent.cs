﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundFromEvent : MonoBehaviour {

    public GameObject obj;
    public string eventName;
    public bool playOnStart = false;

	// Use this for initialization
	void Start () {
		if (obj == null)
        {
            obj = gameObject;
        }

        if (eventName == "")
        {
            Debug.Log("please set event name");
        }
        if(playOnStart){
            PlaySound();
        }
	}
	
	// Update is called once per frame
	public void PlaySound () 
    {
        AkSoundEngine.PostEvent(eventName, obj);
    }
}
