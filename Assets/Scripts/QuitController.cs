﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class QuitController : MonoBehaviour {

	private ModalPanel confirmWindow;
    public GameEvent escapePressed;
    
	void Start () {
		
	}
	
	void Update () 
    {
        if (Input.GetKeyDown(KeyCode.Escape) )
        {
            escapePressed.Raise();
            Debug.Log(SceneManager.GetActiveScene().name);
            // DisplayQuitConfirmWindow();
        }
	}

	public void DisplayQuitConfirmWindow()
    {
        if( confirmWindow == null )
        {
            confirmWindow = ModalPanel.Instance();
            confirmWindow.Choice("",QuitGame,CloseConfirmWindow);
            confirmWindow.GetComponentInChildren<LocalizedText>().key = "confirmation";
            confirmWindow.GetComponentInChildren<LocalizedText>().UpdateText();
        }
        else
        {
            confirmWindow.gameObject.SetActive(true);
        }
    }


	void CloseConfirmWindow()
    {
        confirmWindow.gameObject.SetActive(false);
    }

	void QuitGame()
    {
        Application.Quit();
	}
}
