﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDashNoise : OnUpdate {

	public FloatReference playerDashNoiseRange;
	List<Vector3> entryPoints;
	List<Vector3> exitPoints;
	public List<GameObject> contacts;
	public ThingSet playerSet;
	public ThingSet setToLookFor;
	public LayerMask layerMask;
	PlayerWorldPosition playerWorldPosition;
	bool isRunning;
	
	int noiseID;
	// Use this for initialization
	void Start () {
		if (playerSet == null)
		{
			Debug.Log("Player set, not set");
		}
		playerWorldPosition = playerSet.GetItems()[0].GetComponent<PlayerWorldPosition>();
		entryPoints = new List<Vector3>();
		exitPoints = new List<Vector3>();
		contacts = new List<GameObject>();
	}
	public void StartDashing()
	{
		isRunning = true;
		NoiseMaker.IncreaseNoiseID();
		noiseID = NoiseMaker.noiseId;
	}
	public void StopDashing()
	{
		isRunning = false;
	}

	public override void UpdateEventRaised()
	{
		if (isRunning)
		{
			if (!playerWorldPosition.IsInVoid())
			{
				Collider[] collidersInSphere = Physics.OverlapSphere(playerWorldPosition.transform.position,playerDashNoiseRange.Value,layerMask);
				List<GameObject> newContacts = new List<GameObject>();
				if (collidersInSphere.Length == 0)
				{
					return;
				}
				for (int i = 0; i < collidersInSphere.Length; i++)
				{
					
					Thing[] thingsOnCollider = collidersInSphere[i].transform.GetComponents<Thing>();
					if (thingsOnCollider.Length == 0)
					{
						continue;
					}
					for (int j = 0; j < thingsOnCollider.Length; j++)
					{
						if (ThingSet.Equals(thingsOnCollider[j].RunTimeSet,setToLookFor))
						{
							//We have an enemy here
							newContacts.Add(collidersInSphere[i].gameObject);
						}
					}
				}
				CheckForDifferences(newContacts);
			} else {
				if (contacts.Count != 0)
				{
					//End all
					isRunning = false;
					for (int i = contacts.Count -1; i >= 0; i--)
					{
						exitPoints[i] = playerWorldPosition.transform.position;
						CalculateSoundOrigin(i);
					}
				}
			}

		}
	}

	void CheckForDifferences(List<GameObject> newContacts)
	{
		//Check for anyone that is no longer there
		//Check for any new gameobjects
		for (int i = contacts.Count -1; i >= 0; i--)
		{
			bool isOnList = false;
			int positionToRemove = 0;
			for (int j = newContacts.Count -1; j >= 0; j--)
			{
				if (GameObject.Equals(contacts[i],newContacts[j]))
				{
					//contact is no longer there. End the sound contact
					isOnList = true;
					positionToRemove = j;
				}
			}
			if (isOnList)
			{
				//The gameobject is still within the sphere. Removed as it is not a new contact
				newContacts.RemoveAt(positionToRemove);
			} else
			{
				//The gameobject is no longer within range. Exitpoint is set, and sound origin position is calculated
				exitPoints[i] = playerWorldPosition.transform.position;
				CalculateSoundOrigin(i);
			}
		}
		//If there is still objects on newcontacts they are new and are added as contacts
		for (int i = 0; i < newContacts.Count; i++)
		{
			contacts.Add(newContacts[i]);
			entryPoints.Add(playerWorldPosition.transform.position);
			exitPoints.Add(playerWorldPosition.transform.position);
			MakeNoiseAt(newContacts[i],playerWorldPosition.transform.position);
		}
	}

	void CalculateSoundOrigin(int positionInLists)
	{
		Vector3 originPosition = FindNearestPointOnLine(entryPoints[positionInLists],exitPoints[positionInLists],contacts[positionInLists].transform.position);
		//Debug.Log("Position of sound: " +originPosition +" from entry: " + entryPoints[positionInLists] + " and exit: " +exitPoints[positionInLists]);
		MakeNoiseAt(contacts[positionInLists],originPosition);
		RemoveFromAllListAt(positionInLists);
	}
	void RemoveFromAllListAt(int positionInLists)
	{
		entryPoints.RemoveAt(positionInLists);
		exitPoints.RemoveAt(positionInLists);
		contacts.RemoveAt(positionInLists);
	}
	void MakeNoiseAt(GameObject obj, Vector3 positionOfNoise)
	{
		NoiseReceiver receiver = obj.GetComponent<NoiseReceiver>();
		if (receiver == null)
		{
			Debug.Log("Thing on enemy set list does not have a noise receiver!");
			return;
		}
		float distance = Vector3.Distance(positionOfNoise,obj.transform.position);
		receiver.ReceiveNoise(positionOfNoise,distance,noiseID);
		//Generate debug position
	}

	public Vector3 FindNearestPointOnLine(Vector3 origin, Vector3 end, Vector3 point)
	{
		//Get heading
		Vector3 heading = (end - origin);
		float magnitudeMax = heading.magnitude;
		heading.Normalize();

		//Do projection from the point but clamp it
		Vector3 lhs = point - origin;
		float dotP = Vector3.Dot(lhs, heading);
		dotP = Mathf.Clamp(dotP, 0f, magnitudeMax);
		return origin + heading * dotP;
	}

	//----------------------
	/*
	void MakeNoiseAt(GameObject obj)
	{
		for (int i = contacts.Count -1; i >= 0; i--)
		{
			if (GameObject.Equals(contacts[i],obj))
			{
				NoiseReceiver receiver = obj.GetComponent<NoiseReceiver>();
				if (receiver != null)
				{
					Vector3 middlePoint = (entryPoints[i] + exitPoints[i])/2;
					float distance = Vector3.Distance(middlePoint,obj.transform.position);
					receiver.ReceiveNoise(middlePoint,distance);
					Debug.Log("Making noise");
				}
				Debug.Log(i + " with Contacts: " + contacts.Count + " entry: " + entryPoints.Count + " exit: " + exitPoints.Count);
				for (int j = 0; j < contacts.Count; j++)
				{
					Debug.Log("GameObject Name: " + contacts[j].name);
				}
				contacts.RemoveAt(i);
				entryPoints.RemoveAt(i);
				exitPoints.RemoveAt(i);
				Debug.Log("Finished");

			}
		}
	}

	public void FinishedDash(Vector3 pointOfVoidEntry)
	{
		coll.enabled = false;
		for (int i = exitPoints.Count; i < entryPoints.Count; i++)
		{
			exitPoints.Add(pointOfVoidEntry);
		}
		for (int i = contacts.Count -1; i >= 0; i--)
		{
			MakeNoiseAt(contacts[i]);
		}
		entryPoints = new List<Vector3>();
		exitPoints = new List<Vector3>();
		contacts = new List<GameObject>();
	}
	public void FinishedDash()
	{
		for (int i = exitPoints.Count; i < entryPoints.Count; i++)
		{
			exitPoints.Add(transform.position);
		}
		for (int i = contacts.Count -1; i >= 0; i--)
		{
			exitPoints[i] = transform.position;
			MakeNoiseAt(contacts[i]);
		}
		entryPoints = new List<Vector3>();
		exitPoints = new List<Vector3>();
		contacts = new List<GameObject>();
	}
	
	/*
	void OnCollisionEnter(Collision collision)
	{
		entryPoints.Add( collision.contacts[0].point);
		contacts.Add(collision.collider.gameObject);
		Debug.Log("Collision Enter");
	}
	void OnCollisionExit(Collision collision)
	{
		exitPoints.Add( collision.contacts[0].point);
		MakeNoiseAt(collision.collider.gameObject);
		if (collision.collider.gameObject.layer == voidLayer)
		{
			//FinishedDash(collision.contacts[0].point);
		}
		Debug.Log("Collision Exit");
	} 
	void OnTriggerEnter(Collider other)
	{
		Thing[] things = other.gameObject.GetComponents<Thing>();
		for (int i = 0; i < things.Length; i++)
		{
			if (ThingSet.Equals(VoidZonesSet,things[i].RunTimeSet))
			{
				//other is in the void
				//coll.enabled = false;
			} else if (ThingSet.Equals(enemyHearingObjectSet,things[i].RunTimeSet))
			{
				//other is an enemyHearingObject
				entryPoints.Add( transform.position);
				exitPoints.Add(transform.position);
				contacts.Add(other.gameObject);
				Debug.Log("Got an enemy thingset");
			}
		}
		//Debug.Log(other.gameObject.layer + " void: " + voidLayer);
	}
	public void StopDash()
	{
		FinishedDash();

	}

	void OnTriggerExit(Collider other)
	{
		
		Thing[] things = other.gameObject.GetComponents<Thing>();
		for (int i = 0; i < things.Length; i++)
		{
			if (ThingSet.Equals(VoidZonesSet,things[i].RunTimeSet))
			{
				//other is in the void
				//Should activate the collider
				//coll.enabled = true;
			} else if (ThingSet.Equals(enemyHearingObjectSet,things[i].RunTimeSet))
			{
				//other is an enemyHearingObject
				for (int j = 0; j < contacts.Count; j++)
				{
					if (GameObject.Equals(contacts[j],other.gameObject))
					{
						exitPoints[j] = transform.position;
						MakeNoiseAt(other.gameObject);

					}
				}
			}
		}

	}
	public void BeginDash()
	{
		coll.enabled = true;
	}
	 */
}
