﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLost : OnUpdate {
    public ThingSet playerSet;
    public GameEvent playerKilled;
    private Transform playerTransform;
    bool isDead = false;
    bool finishedDisplayingDeath = false;
    public GameObject deathPanel;
    public GameEvent pauseGame;

	// Use this for initialization
	void Start () {
        if (playerSet.GetItems().Count != 0)
        {
            playerTransform = playerSet.GetItems()[0].transform;
            //Debug.LogError("The player's transform is required on this game object.");
        }
    }

    public override void UpdateEventRaised()
    {
        if (isDead && !finishedDisplayingDeath)
        {
            playerTransform.localScale -= new Vector3(0.01f, 0.01f, 0.01f);

            if (playerTransform.localScale == Vector3.zero)
            {
                deathPanel.SetActive(true);
                deathPanel.GetComponent<FadeInController>().FadeIn();
                finishedDisplayingDeath = true;
                pauseGame.Raise();
            }
        }
    }

    public void Killed()
    {
        if (!isDead)
        {
            isDead = true;
            playerKilled.Raise();
            AkSoundEngine.PostEvent("PCDeath", gameObject);
        }        
    }
}
