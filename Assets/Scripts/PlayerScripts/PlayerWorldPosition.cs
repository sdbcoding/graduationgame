﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWorldPosition : MonoBehaviour {

	public bool isInVoid = false;
	public LayerMask voidLayer;

	public bool IsInVoid()
	{
		
		return isInVoid;
	}

	void OnTriggerEnter(Collider other)
	{
		//Debug.Log("Enter: " + other.gameObject.name);
		//Debug.Log("Void layer: " + voidLayer.value);

		if (LayerMask.LayerToName( other.gameObject.layer) == "Void")
		{
			isInVoid = true;
		}
	}
	void OnTriggerStay(Collider other)
	{
		//Debug.Log("Stay: " + other.gameObject.name);

		if (LayerMask.LayerToName( other.gameObject.layer) == "Void")
		{
			isInVoid = true;
		}
	}
	void OnTriggerExit(Collider other)
	{
		//Debug.Log("Exit: " + other.gameObject.name);

		if (LayerMask.LayerToName( other.gameObject.layer) == "Void")
		{
			isInVoid = false;
		}
	}
	
}
