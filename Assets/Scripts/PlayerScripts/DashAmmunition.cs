﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashAmmunition : OnUpdate {

    public FloatVariable maxDashPercentage;
    public FloatVariable startDashPercentage;
    public FloatVariable currentDashPercentage;
    public FloatVariable dashReloadAmount;
    public FloatVariable dashCost;
    public GameEvent startDash;
    public GameEvent dashFailed;

	// Use this for initialization
	void Start ()
    {
        //Debug.Log(startDashPercentage.GetValue());
        currentDashPercentage.SetValue(startDashPercentage.GetValue());
	}

    public override void UpdateEventRaised()
    {
        if (currentDashPercentage.GetValue() < maxDashPercentage.GetValue())
        {
            ReloadSomeAmount();
        }
    }

    private void ReloadSomeAmount()
    {
        var currentDashPercentageValue = currentDashPercentage.GetValue();
        float newAmount = currentDashPercentageValue + (dashReloadAmount.GetValue() * Time.deltaTime );
        if (newAmount > maxDashPercentage.GetValue())
        {
            newAmount = maxDashPercentage.GetValue();
        }
        currentDashPercentage.SetValue(newAmount);
    }

    public void DashFired()
    {
        var currentDashPercentageValue = currentDashPercentage.GetValue();
        currentDashPercentage.SetValue(currentDashPercentageValue - dashCost.GetValue());
    }

    public void IsThereAmmunition()
    {
        if (dashCost.GetValue() <= currentDashPercentage.GetValue())
        {
            startDash.Raise();
            DashFired();
        } else {
            dashFailed.Raise();
        }
    }
}
