﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TouchInputGetter : OnUpdate {

	public Vector3Variable groundPositionOfTouch;
	private Camera mainCamera;
	public GameObjectSet objectsTouched;
	public FloatReference maxDistanceOfRay;
	public LayerMask layerMaskToRaycastThrough;
	public GameEvent successfulTouchEvent;
	public FloatReference yValue;
	public FloatReference maxDistanceBeforeTapIsIgnored;
	public FloatReference timeBeforeItsNoLongerTap;
	List<AllowedTouch> allowedTouches;
	public GraphicRaycaster uiRaycaster;
	PointerEventData pointerEventData;
	public EventSystem eventSystem;

    struct AllowedTouch
	{
		public int fingerID;
		public float timeBegan;
		public Vector2 beginningPosition;
		public AllowedTouch(int finger,float time, Vector2 position)
		{
			fingerID = finger;
			timeBegan = time;
			beginningPosition = position;
		}
	}

	void Start()
	{
		allowedTouches = new List<AllowedTouch>();
		mainCamera = Camera.main;
	}
	
	public override void UpdateEventRaised()
	{
		for (int i = 0; i < Input.touchCount; i++)
		{
			if (Input.touches[i].phase == TouchPhase.Began)
			{
				CheckValidityOfNewInput(Input.touches[i]);
			} else if(Input.touches[i].phase == TouchPhase.Ended)
			{
				CheckEndOfTouch(Input.touches[i]);
			}
		}
		
	}

	void CheckValidityOfNewInput(Touch touch)
	{
		if (!EventSystem.current.IsPointerOverGameObject(touch.fingerId))
		{
			allowedTouches.Add(new AllowedTouch(touch.fingerId,Time.fixedUnscaledTime,touch.position));
		}
		/* 
		Vector3 newRaycastOrigin = touch.position;
		newRaycastOrigin.z = -1;
		RaycastHit hit = new RaycastHit();
		if (Physics.Raycast(newRaycastOrigin,Vector3.forward,out hit,5))
		{
			allowedTouches.Add(touch.fingerId);
			Debug.Log("Got correct touch");
		}
		if (hit.collider != null)
		{
			Debug.Log(hit.collider.gameObject.name);
		}
		*/
	}
	void CheckEndOfTouch(Touch touch)
	{
		for (int i = 0; i < allowedTouches.Count; i++)
		{
			if (touch.fingerId == allowedTouches[i].fingerID)
			{
				if (Time.fixedUnscaledTime - timeBeforeItsNoLongerTap < allowedTouches[i].timeBegan)
				{
					float distance = Vector2.Distance(touch.position,allowedTouches[i].beginningPosition);
					//Debug.Log(distance +" up against: " + maxDistanceBeforeTapIsIgnored.Value);
					if (distance< maxDistanceBeforeTapIsIgnored.Value)
					{
						//Debug.Log("Success");
						//Successful tap!
						FindGroundAndGameObjectsFromHits(RaycastFromScreenPosition(touch.position));
						UIRaycasting(touch.position);
						SuccessfulTouch();
					}
				}
				//Touch was allowed but failed requirements and is deleted. Or successful and just deleted
				allowedTouches.RemoveAt(i);
				return;
			}
		}

	}

	void UIRaycasting(Vector2 touchPosition)
	{
		List<RaycastResult> results = new List<RaycastResult>();
		pointerEventData = new PointerEventData(eventSystem);
		pointerEventData.position = touchPosition;
		uiRaycaster.Raycast(pointerEventData,results);
		//Debug.Log("Size: " + results.Count);
		for (int i = 0; i < results.Count; i++)
		{
			//Debug.Log(results[i].gameObject.name);
			objectsTouched.Add(results[i].gameObject);
		}
	}
	RaycastHit[] RaycastFromScreenPosition(Vector2 screenPosition)
	{
		RaycastHit[] hits;
		Ray positionInWorldSpace = mainCamera.ScreenPointToRay(screenPosition);
		hits = Physics.RaycastAll(positionInWorldSpace,maxDistanceOfRay, layerMaskToRaycastThrough);
		return hits;
	}
	void FindGroundAndGameObjectsFromHits(RaycastHit[] hits)
	{
		objectsTouched.SetItems( new List<GameObject>());
		for (int i = 0; i < hits.Length; i++)
		{
			objectsTouched.Add(hits[i].collider.gameObject);
			//Debug.Log(hits[i].collider.gameObject.name);
			if (hits[i].collider.gameObject.layer == LayerMask.NameToLayer("Ground"))
			{
				Vector3 yCorrectedHitPoint = hits[i].point;
				yCorrectedHitPoint.y = yValue;
				groundPositionOfTouch.SetValue(yCorrectedHitPoint);
				
			}
		}
	}
	void SuccessfulTouch()
	{
		successfulTouchEvent.Raise();
		//Debug.Log("Successful touch");
	}

}
