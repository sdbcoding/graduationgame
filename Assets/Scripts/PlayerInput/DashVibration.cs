﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashVibration : MonoBehaviour {

	public FloatReference vibrationStartStrenght;
	public FloatReference vibrationStopStrenght;
	public FloatReference vibrationTime;
	float currentTime;
	bool isVibrating = false;
	
	// Update is called once per frame
	void Update () {
		if (isVibrating)
		{
			currentTime += Time.deltaTime;
			float vibrationStrenght = Mathf.Lerp(vibrationStartStrenght,vibrationStopStrenght,currentTime/vibrationTime);
			if (currentTime > vibrationTime)
			{
				StopVibration();
			}
			long[] pat = new long[] {0,100,5,90,15,80,30,50,50,20};
			Vibration.Vibrate(pat, -1);
			//Vibration.Vibrate((long)vibrationTime.Value *100);
			//Handheld.Vibrate();
			isVibrating = false;
		}
	}
	public void StartVibration()
	{
		currentTime = 0;
		isVibrating = true;
	}
	public void StopVibration()
	{
		isVibrating = false;
	}
	public void DashFailed()
	{
		Vibration.Vibrate(80);
	}
}
