﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(NoiseMaker))]
public class DashNoiseMaker : OnUpdate {
	public NoiseMaker noiseMaker;
	public FloatReference dashNoiseRange;
	public ThingSet playerSet;
	private Transform playerTransform;
	private PlayerWorldPosition playerWorldPosition;
	bool isRunning = false;
	void Start()
	{
		if (noiseMaker == null)
		{
			noiseMaker = GetComponent<NoiseMaker>();
		}
		if (playerSet.GetItems().Count != 0)
        {
            playerTransform = playerSet.GetItems()[0].transform;
			playerWorldPosition = playerSet.GetItems()[0].GetComponent<PlayerWorldPosition>();
			if (playerWorldPosition == null)
			{
				Debug.Log("PlayerWorldPosition not on playerobject");

			}
			//Debug.Log("Player transform missing from DashNoiseMaker");
		}
		//Debug.Log(playerTransform.position);
	}	
	public override void UpdateEventRaised()
	{
		if (isRunning)
		{
			if (!playerWorldPosition.IsInVoid())
			{
				noiseMaker.MakeNoise(playerTransform.position,dashNoiseRange.Value);

			}
		}
	}

	public void DashStart()
	{
		isRunning = true;
		NoiseMaker.IncreaseNoiseID();
	}
	public void DashStop()
	{
		isRunning = false;
	}
}
