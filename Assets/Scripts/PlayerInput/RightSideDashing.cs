﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightSideDashing : OnUpdate {

	List<int> activeTouchIds;
	List<Vector2> startPositions;
	public ThingSet playerSet;
	public FloatReference maxDistance;
	public FloatReference minDistance;
	public FloatReference deadZoneInPixels;
	Transform playerTransform;
	public Vector3Variable locationToDashTo;
	public GameEvent startDashing;
    public GameEvent checkDashAmo;

	void Awake()
	{
		activeTouchIds = new List<int>();
		startPositions = new List<Vector2>();
		
	}

    private void Start()
    {
        playerTransform = playerSet.GetItems()[0].transform;
    }

    public override void UpdateEventRaised()
	{
		for (int i = 0; i < Input.touchCount; i++)
		{
			if (Input.touches[i].phase == TouchPhase.Began)
			{
				BeginTouch(Input.touches[i]);
			} else if(Input.touches[i].phase == TouchPhase.Ended)
			{
				EndTouch(Input.touches[i]);
			}
		}
	}
	void EndTouch(Touch touch)
	{
		for (int i = 0; i < activeTouchIds.Count; i++)
		{
			if (activeTouchIds[i] == touch.fingerId)
			{
				SetupDashing(startPositions[i],touch.position);
				activeTouchIds.RemoveAt(i);
				startPositions.RemoveAt(i);
			}
		}
	}
	void BeginTouch(Touch touch)
	{
		if (touch.position.x > Screen.width/2)
		{
			activeTouchIds.Add(touch.fingerId);
			startPositions.Add(touch.position);
		}
	}
	void SetupDashing(Vector2 startPosition,Vector2 endPosition)
	{
		float distance = Vector3.Distance(startPosition,endPosition);
		if(deadZoneInPixels > distance)
		{
			return;
		}
		Vector2 vec2direction = endPosition - startPosition;
		Vector3 normalizedDirection = new Vector3(vec2direction.x,0.0f,vec2direction.y).normalized;
		float magnitude = Mathf.Clamp(distance,minDistance,maxDistance);
		normalizedDirection = Quaternion.Euler(new Vector3(0,45,0)) * normalizedDirection;
		Vector3 newNormalizedDirection = normalizedDirection + playerTransform.position;

		Vector3 newLocation = (normalizedDirection * (magnitude)) + playerTransform.position;
		newLocation.y = 0.5f;
		GameObject temp = new GameObject();
		temp.transform.position = newLocation;

		RaycastHit hit;
		Physics.Raycast(newLocation,Vector3.down,out hit,2.0f);
		Debug.Log(newLocation + " player position: " + playerTransform.position +" magnitude is: " +magnitude + " with normalizaed Direction: " + normalizedDirection + " together: " +(normalizedDirection*magnitude));
		if (hit.collider == null)
		{
			Debug.Log("It's null");
			return;
		}

		Debug.Log(LayerMask.LayerToName(hit.collider.gameObject.layer));
		if (LayerMask.LayerToName(hit.collider.gameObject.layer) == "Ground")
		{
			
			locationToDashTo.SetValue(newLocation);
            //startDashing.Raise();
            checkDashAmo.Raise();
		}
	}

}
