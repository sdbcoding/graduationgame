﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActionButton : MonoBehaviour {

	public GameObjectVariable killableEnemy;
	public PlayerKillTimer killTimer;    
    public GameEvent startAiming;
    public GameEvent stopAiming;
    private bool isKilling = false;
    private bool isDashing = false;

    public void PressButton()
	{
        if (isDashing)
        {
            return;
        }

		if (killableEnemy.Value != null)
		{
            isKilling = true;
			//There is an enemy to kill
			//TODO: Activate sound for kill;
			killTimer.InnitiateKill();
		} else {
            //Should start the dash aiming stuff
            startAiming.Raise();
		}
	}

	public void ReleaseButton()
	{
        if (isDashing)
        {
            return;
        }

        killTimer.ButtonReleased();

        if (!isKilling)
        {
            stopAiming.Raise();
        }
        
        isKilling = false;
	}

    public void StartDashing()
    {
        isDashing = true;
    }

    public void StopDashing()
    {
        isDashing = false;
    }
}
