﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickMovementControls : MonoBehaviour {

	public LeftJoystick leftJoystick; // the game object containing the LeftJoystick script
    //public float moveSpeed = 6.0f; // movement speed of the player character
    //public int rotationSpeed = 8; // rotation speed of the player character
    //public Animator animator; // the animator controller of the player character
    private Vector3 leftJoystickInput; // holds the input of the Left Joystick
    public Vector3Variable playerInput;

    void Start()
    {
        if (leftJoystick == null)
        {
            Debug.LogError("The left joystick is not attached.");
        }

        
    }

    void Update()
    {
    }

    void FixedUpdate()
    {
        playerInput.SetValue( Vector3.zero);
        // get input from both joysticks
        leftJoystickInput = leftJoystick.GetInputDirection();

        float xMovementLeftJoystick = leftJoystickInput.x; // The horizontal movement from joystick 01
        float zMovementLeftJoystick = leftJoystickInput.y; // The vertical movement from joystick 01	

        // if there is no input on the left joystick
        if (leftJoystickInput == Vector3.zero)
        {
            //animator.SetBool("isRunning", false);
        }
  
        // if there is only input from the left joystick
        if (leftJoystickInput != Vector3.zero)
        {
            // calculate the player's direction based on angle
            float tempAngle = Mathf.Atan2(zMovementLeftJoystick, xMovementLeftJoystick);
            xMovementLeftJoystick *= Mathf.Abs(Mathf.Cos(tempAngle));
            zMovementLeftJoystick *= Mathf.Abs(Mathf.Sin(tempAngle));

            leftJoystickInput = new Vector3(xMovementLeftJoystick, 0, zMovementLeftJoystick);
            leftJoystickInput = transform.TransformDirection(leftJoystickInput);
            //leftJoystickInput *= moveSpeed;

            // rotate the player to face the direction of input
            Vector3 temp = transform.position;
            temp.x += xMovementLeftJoystick;
            temp.z += zMovementLeftJoystick;
            Vector3 lookDirection = temp - transform.position;
            if (lookDirection != Vector3.zero)
            {
                //rotationTarget.localRotation = Quaternion.Slerp(rotationTarget.localRotation, Quaternion.LookRotation(lookDirection), rotationSpeed * Time.deltaTime);
            }
            /* 
            if (animator != null)
            {
                //animator.SetBool("isRunning", true);
            }
            */
            // move the player
            //rigidBody.transform.Translate(leftJoystickInput * Time.fixedDeltaTime);
            playerInput.SetValue(leftJoystickInput);
        }
    }
}
