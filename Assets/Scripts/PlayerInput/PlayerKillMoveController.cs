﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerKillMoveController : MonoBehaviour {
	public GameObjectSet gameOjbectsTouched;
	public ThingSet enemies;
	public ThingSet killableEnemies;
	public GameObjectVariable killThisEnemy;
	public GameEvent killMoveEvent;
	public bool WasKillMove()
	{
		//TODO a system that takes the closest enemy instead of the first encountered
		for (int i = 0; i < gameOjbectsTouched.GetItems().Count; i++)
		{
			Thing[] things = gameOjbectsTouched.GetItems()[i].GetComponents<Thing>();
			for (int j = 0; j < things.Length; j++)
			{
				if (ThingSet.Equals(things[j].RunTimeSet, enemies))
				{
					if (CanIKillEnemy(gameOjbectsTouched.GetItems()[i]))
					{
						Debug.Log(gameOjbectsTouched.GetItems()[i]);
						killThisEnemy.SetValue(gameOjbectsTouched.GetItems()[i]);
						killMoveEvent.Raise();
						return true;

					}
				}
				Debug.Log("It did not equal");
			}

		}
		return false;
	}
	bool CanIKillEnemy(GameObject enemy)
	{
		for (int i = 0; i < killableEnemies.GetItems().Count; i++)
		{
			if(GameObject.Equals(enemy,killableEnemies.GetItems()[i].gameObject))
			{
				return true;
			}
		}
		return false;
	}
}
