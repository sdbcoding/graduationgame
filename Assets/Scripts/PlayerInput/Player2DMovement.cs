﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(CharacterController))]
public class Player2DMovement : OnUpdate {
	public Vector3Variable playerInput;
	public FloatReference yValue;
	float speed;
	public FloatReference acceleration;
	public FloatReference drag;
	public FloatReference maxSpeed;
	CharacterController controller;
	Vector3 currentDirection;
	Vector3 yCorrectedPlayerInput;
	Vector3 yCorrectedZero;
    private bool isAiming = false;
    private bool isDashing = false;
    public NavMeshAgent agent;
    public Vector3Variable positionToMoveTo;
	bool isRunning;
	public bool withMagnitude = false;

    void Start()
	{
		yCorrectedZero = Vector3.zero;
		yCorrectedZero.y = yValue;
		currentDirection = yCorrectedZero;
		currentDirection = Vector3.zero;
		if (controller == null)
		{
			controller = GetComponent<CharacterController>();
		}
		if (agent == null)
		{
        	agent = GetComponent<NavMeshAgent>();
		}
		agent.enabled = false;
		
    }
	public override void UpdateEventRaised()
	{
		if (!isRunning) {return;}
		float distance = Vector3.Distance(transform.position,positionToMoveTo.Value);
		if (distance < 0.01f)
		{
			agent.isStopped = true;
			agent.ResetPath();
		}
		
		yCorrectedPlayerInput = new Vector3(playerInput.Value.x,0,playerInput.Value.z);
		Vector3 directionFromCamera = Camera.main.ScreenToWorldPoint(yCorrectedPlayerInput);
		//yCorrectedPlayerInput = (Quaternion. directionFromCamera).normalized;
		//Debug.Log(yCorrectedPlayerInput + " <-before after -> " +transform.InverseTransformDirection(yCorrectedPlayerInput));
		CalculateCurrentDirection();
		CalculateCurrentSpeed();
		MovePlayerAccordingToVector3();
	}
	void CalculateCurrentSpeed()
	{
		float distance = Vector3.Distance(Vector3.zero,yCorrectedPlayerInput);
		//Debug.Log("distance: " + distance);
		if (distance == 0)
		{
			speed -= drag;
		}
		if (withMagnitude)
		{
			speed = maxSpeed;
		} else {
			speed += acceleration * distance;
		}
		if (speed > maxSpeed)
		{
			speed = maxSpeed;
		}
		if (speed < 0)
		{
			speed = 0;
		}
	}

	void CalculateCurrentDirection()
	{
		if (withMagnitude)
		{
			currentDirection = yCorrectedPlayerInput;
		} else {
			currentDirection += yCorrectedPlayerInput;
			currentDirection = currentDirection.normalized;
		}
	}

	void MovePlayerAccordingToVector3()
	{
		if (currentDirection != Vector3.zero && !isDashing)
		{	
			//Debug.Log("Current direction: " + currentDirection);
			//Vector3 screenToWorld = Camera.main.ScreenToWorldPoint( currentDirection).normalized;
			//Debug.Log(screenToWorld);
			Vector3 newLookDirection =  currentDirection+ transform.position;
			newLookDirection.y = transform.position.y;
			transform.LookAt(newLookDirection);
			transform.RotateAround(transform.position,Vector3.up,45);

			//transform.position += transform.forward * speed * Time.deltaTime;
            if (!isAiming)
            {
				float magniture = currentDirection.magnitude;
				//Debug.Log("Mag: " + magniture);
				if (withMagnitude)
				{
                	controller.Move(transform.forward * Mathf.Pow(speed * 2,magniture) * Time.deltaTime);
				} else {
                	controller.Move(transform.forward * speed  * Time.deltaTime);

				}

            }			
		}
	}

    public void StartAiming()
    {
        isAiming = true;
    }

    public void StopAiming()
    {
        isAiming = false;
    }

    public void StartDashing()
    {
        isDashing = true;
        agent.enabled = false;
    }

    public void StopDashing()
    {
        isDashing = false;
        agent.enabled = true;
        AkSoundEngine.PostEvent("WhooshStop", gameObject);
    }
	public void PlayerHaveSpawned()
	{
		isRunning = true;
		agent.enabled = true;

	}

    public void MoveWithNavMesh()
    {
		if (!isRunning) {return;}
        if (!isDashing)
        {
			agent.isStopped = false;

			//Debug.Log("Destination received");
			Vector3 newPosition = positionToMoveTo.Value;
			newPosition.y = transform.position.y;
			positionToMoveTo.SetValue(newPosition);
            agent.SetDestination(positionToMoveTo.Value);
        }
        else
        {
            Debug.Log("ISDASHING!!!");
        }
    }

    public virtual void PauseNavAgent()
    {
        agent.isStopped = true;
    }
    public virtual void ResumeNavAgent()
    {
        agent.isStopped = false;
    }
}
