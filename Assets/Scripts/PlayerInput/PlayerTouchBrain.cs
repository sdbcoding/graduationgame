﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTouchBrain : MonoBehaviour {
	public PlayerKillMoveController killMoveController;
    public TouchDashController dashController;
	public TakedownSystemController takedownSystemController;
	public void OnSuccessfulTouch()
	{
		if (takedownSystemController != null)
		{
			if (takedownSystemController.WasPartOfTakeDown())
			{
				return;
			}

		}
		if (killMoveController != null)
		{
			if (killMoveController.WasKillMove())
			{
				return;
			}

		}
		if (dashController != null)
		{
			if (dashController.IsTouchADash())
			{
				return;
			}

		}
		

	}
}
