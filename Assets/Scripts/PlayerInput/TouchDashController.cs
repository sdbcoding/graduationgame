﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchDashController : MonoBehaviour
{

    public Vector3Variable groundPositionOfTouch;
    public ThingSet playerSet;
    public FloatReference dashDistanceAllowed;
    public Vector3Variable voidPositionToDashToo;
    public GameEvent dashStart;
    public GameEvent checkDashAmo;
    public LayerMask wallMask;
    public FloatReference voidCharacterOffset;
    Transform playerTransform;
    Vector3 prevGround;
    bool canDash = true;
    void Start()
    {
        prevGround = groundPositionOfTouch.Value;
        playerTransform = playerSet.GetItems()[0].transform;
    }
    public bool IsTouchADash()
    {
        if (!canDash) { return false; }
        //Check if groundPosition is different from last time. New ground was not found in the touch
        if (Vector3.Equals(prevGround, groundPositionOfTouch.Value)) { return false; }
        Vector3 yPlayerPosition = playerTransform.position;
        Vector3 yGroundPosition = groundPositionOfTouch.Value;
        yPlayerPosition.y = 0.5f;
        yGroundPosition.y = 0.5f;

        float distance = Vector3.Distance(yPlayerPosition, yGroundPosition);
        if (distance > dashDistanceAllowed.Value) { return false; }
        //Debug.Log("Before: " + yGroundPosition);
        yGroundPosition = CalculateCorrectVoidPosition(yGroundPosition);
        //Debug.Log("After: " + yGroundPosition);

        voidPositionToDashToo.SetValue(yGroundPosition);

        Vector3 newForward = groundPositionOfTouch.Value - playerTransform.position;
        newForward.y = 0.0f;
        playerTransform.forward = newForward;

        //dashStart.Raise();
        checkDashAmo.Raise();

        prevGround = groundPositionOfTouch.Value;
        return true;
    }

    public Vector3 CalculateCorrectVoidPosition(Vector3 edgePointOfCollider)
    {
        float lengtOfRaycast = 0.5f;
        RaycastHit hit = new RaycastHit();

        if (Physics.Raycast(edgePointOfCollider, Vector3.forward, out hit, lengtOfRaycast, wallMask))
        {
            //Debug.Log("forward: " + hit.collider.gameObject.name);
            float distance = Vector3.Distance(hit.point, edgePointOfCollider);
            //Debug.Log("Forward: " + distance);
            edgePointOfCollider += Vector3.back * (lengtOfRaycast - distance);
        }

        if (Physics.Raycast(edgePointOfCollider, Vector3.right, out hit, lengtOfRaycast, wallMask))
        {
            //Debug.Log("right: " + hit.collider.gameObject.name);
            float distance = Vector3.Distance(hit.point, edgePointOfCollider);
            //Debug.Log("right: " + distance);
            edgePointOfCollider += Vector3.left * (lengtOfRaycast - distance);
        }

        if (Physics.Raycast(edgePointOfCollider, Vector3.back, out hit, lengtOfRaycast, wallMask))
        {
            //Debug.Log("back: " + hit.collider.gameObject.name);
            float distance = Vector3.Distance(hit.point, edgePointOfCollider);
            //Debug.Log("back: " + distance);
            edgePointOfCollider += Vector3.forward * (lengtOfRaycast - distance);
        }

        if (Physics.Raycast(edgePointOfCollider, Vector3.left, out hit, lengtOfRaycast, wallMask))
        {
            //Debug.Log("left: " + hit.collider.gameObject.name);
            float distance = Vector3.Distance(hit.point, edgePointOfCollider);
            //Debug.Log("left: " + distance);
            edgePointOfCollider += Vector3.right * (lengtOfRaycast - distance);
        }

        return edgePointOfCollider;
    }
    public void StartDashing()
    {
        canDash = false;
    }
    public void StopDashing()
    {
        canDash = true;

    }
}
