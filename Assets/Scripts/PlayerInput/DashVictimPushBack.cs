﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashVictimPushBack : OnUpdate {

	//public UpdateEvent updateEvent;
	public ThingSet enemies;
	public ThingSet playerSet;
	Transform playerTransform;
	public FloatReference dashPushBackRange;
	public FloatReference speed;
	public FloatReference distance;
	public FloatReference stunTime;
	public LayerMask mask;
	public bool isTimeBased;
	bool isRunning;
	public bool isRigigBody = false;
	public bool isSameDirectionAsDash = false;
	public Vector3Variable dashEndPosition;
	public float forceAmount = 10.0f;
	List<GameObject> enemiesPushed;

	// Use this for initialization
	void Start () {
		playerTransform = playerSet.GetItems()[0].transform;
		enemiesPushed = new List<GameObject>();
	}
	
	// Update is called once per frame
	//void Update () {
		public override void UpdateEventRaised(){
		if (!isRunning){return;}
		List<GameObject> enemiesWithinRange = EnemiesWithinRange();
		if (enemiesWithinRange.Count == 0) {return;}
		for (int i = 0; i < enemiesWithinRange.Count; i++)
		{
			bool shouldContinue = true;
			for (int j = 0; j < enemiesPushed.Count; j++)
			{
				if (GameObject.Equals(enemiesPushed[j],enemiesWithinRange[i]));
				shouldContinue = false;
			}
			if (!shouldContinue) { continue;}
			enemiesPushed.Add(enemiesWithinRange[i]);
			if (isRigigBody)
			{
				PushBackEnemyWithRigigbody(enemiesWithinRange[i]);
			} else {
				PushBackEnemy(enemiesWithinRange[i]);
				
			}
		}
	}
	

	void PushBackEnemy(GameObject enemy)
	{
		EnemyPushedBack pushedBack = enemy.GetComponent<EnemyPushedBack>();
		if (pushedBack == null)
		{
			pushedBack = enemy.AddComponent<EnemyPushedBack>();
		}
		Vector3 direction = enemy.transform.position - playerTransform.position;
		direction.y = 0;
		direction = direction.normalized;
		pushedBack.SetupPushBack(updateEvent,direction,distance,speed,stunTime,isTimeBased,mask,enemy.GetComponent<ParticleSystem>());
	}

	void PushBackEnemyWithRigigbody(GameObject enemy)
	{
		EnemyRigigbodyPushBack pushedBack = enemy.GetComponent<EnemyRigigbodyPushBack>();
		if (pushedBack == null)
		{
			pushedBack = enemy.AddComponent<EnemyRigigbodyPushBack>();
		}
		Vector3 force;
		if (isSameDirectionAsDash)
		{
			force = dashEndPosition.Value - playerTransform.position;
		} else {
			force = enemy.transform.position - playerTransform.position;
		}
		force.y = 0;
		force = force.normalized;
		force = force * forceAmount;
		pushedBack.SetupPushBack(force,enemy.GetComponent<ParticleSystem>());
	}

	List<GameObject> EnemiesWithinRange()
	{
		List<GameObject> objectsSpotted = new List<GameObject>();
		Collider[] collidersInSphere = Physics.OverlapSphere(playerTransform.position,dashPushBackRange.Value,-1,QueryTriggerInteraction.Collide);
		
		if (collidersInSphere.Length == 0)
		{
			Debug.Log("No colliders within sphere");
			return objectsSpotted;
		}


        for (int i = 0; i < collidersInSphere.Length; i++)
		{
			
				//Debug.Log(collidersInSphere[i].gameObject.name);

			//Checks if the found collider is withint th e
			//float angle = Vector3.Angle(transform.forward,(collidersInSphere[i].transform.position -transform.position).normalized);
			Vector3 raycastDirection;
            Vector3 yCorrectedSpherePosition = collidersInSphere[i].transform.position;
            yCorrectedSpherePosition.y = playerTransform.position.y;

            raycastDirection = yCorrectedSpherePosition - playerTransform.position;
			
			Thing[] thingsOnCollider = collidersInSphere[i].transform.GetComponents<Thing>();
			if (thingsOnCollider.Length == 0)
			{
				continue;
			}
			for (int j = 0; j < thingsOnCollider.Length; j++)
			{
				if (ThingSet.Equals(thingsOnCollider[j].RunTimeSet,enemies))
				{
					//TODO implment a raycast to make sure the player is not behing a wall
					//Debug.Log("Trying for: " + collidersInSphere[i].gameObject.name);
					RaycastHit hit;
					Physics.Raycast(playerTransform.position,raycastDirection,out hit,50);

					if (hit.collider == null)
					{
						continue;
					}
					if(GameObject.Equals(hit.collider.gameObject,collidersInSphere[i].gameObject))
					{
						objectsSpotted.Add( collidersInSphere[i].gameObject);
						//Debug.Log("Got enemy");
					}

				}
			}
		}
		return objectsSpotted;

	}

	public void DashStart()
	{
		isRunning = true;
		enemiesPushed = new List<GameObject>();
	}
	public void DashStop()
	{
		isRunning = false;
	}
}
