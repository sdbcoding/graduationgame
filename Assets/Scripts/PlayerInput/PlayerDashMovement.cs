﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDashMovement : OnUpdate {

    public bool isDashTimeRelated = true;
    public FloatReference dashMovementSpeed;
    public FloatReference dashTime;
    public ThingSet playerSet;
    private Transform playerTransform;
    public Vector3Variable positionToMoveTo;
    public GameEvent stopDashing;
    float distancePrFrame;
    bool isDead = false;

    bool dashPlayer = false;

    void Start()
    {
        if (playerSet.GetItems().Count != 0)
        {
            playerTransform = playerSet.GetItems()[0].transform;
        
            //Debug.LogError("The player's transform is required on this game object.");
        }

        if (positionToMoveTo == null)
        {
            Debug.LogError("The vector3 for position of touches is required");
        }

        if (stopDashing == null)
        {
            Debug.LogError("The event for stop dashing is required");
        }
    }

    public override void UpdateEventRaised()
    {
        if (dashPlayer && !isDead)
        {
            MovePlayer();
        }
    }

    public void StartDashingPlayer()
    {
        dashPlayer = true;
        float distance = Vector3.Distance(playerTransform.position,positionToMoveTo.Value);
        distancePrFrame = distance / dashTime;
        //TODO: Maybe som better lerping
        //playerTransform.gameObject.GetComponent<CapsuleCollider>().enabled = false;
        //Debug.Log("Start dashing");
    }

    public void StopDashingPlayer()
    {
        dashPlayer = false;
        // playerTransform.gameObject.GetComponent<CapsuleCollider>().enabled = true;
        stopDashing.Raise();
    }

    public void PlayerKilled()
    {
        isDead = true;
    }
        

    private void MovePlayer()
    {
        if (isDashTimeRelated)
        {
            
            playerTransform.position = Vector3.MoveTowards(playerTransform.position, positionToMoveTo.Value, distancePrFrame *Time.deltaTime);

        } else {
            playerTransform.position = Vector3.MoveTowards(playerTransform.position, positionToMoveTo.Value, dashMovementSpeed * Time.deltaTime);

        }
        //Debug.Log("playerTran: " + playerTransform.position + " postitionToMove: " + positionToMoveTo.Value);

        if (playerTransform.position == positionToMoveTo.Value)
        {
            StopDashingPlayer();
        }
    }





}
