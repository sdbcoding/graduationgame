﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillMove : MonoBehaviour
{

    //public ThingSet killableEnemiesList;
    public GameObjectVariable clickedEnemy;
    public Vector3Variable killPosition;
    
    /*
    public void DokillMove()
    {
        Debug.Log(killableEnemiesList.Items.Count);
        foreach (Thing item in killableEnemiesList.Items)
        {
            Debug.Log("hello");

            if (ReferenceEquals(clickedEnemy.Value, item.gameObject))
            {
                item.gameObject.SetActive(false);
            }
               
        }
    }
    */
    public void DoKillMove()
    {
        //Debug.Log("Doing Kill Move");
        killPosition.SetValue(clickedEnemy.Value.transform.position);
        
        clickedEnemy.Value.SetActive(false);   
    }
}

