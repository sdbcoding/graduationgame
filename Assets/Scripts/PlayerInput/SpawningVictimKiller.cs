﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawningVictimKiller : MonoBehaviour {
	public GameObjectSet objectsTouched;
	public ThingSet enemies;
	public GameEvent playerSpawning;
	public Vector3Variable spawnPosition;
	public GameObjectVariable enemyToKill;
    public Vector3Variable killPosition;
	public GameObject combatMusicPlace;

    public void SuccesfulTouch()
	{
		if(WasKillMove())
		{
			Kill();
			Destroy(gameObject);
		}
	}
	void Kill()
	{
		spawnPosition.SetValue(enemyToKill.Value.transform.position);
		playerSpawning.Raise();
		AkSoundEngine.PostEvent("PCTakedownBuildUp",enemyToKill.Value);
		AkSoundEngine.PostEvent("CombatMusic",combatMusicPlace);
        //killPosition.Value = spawnPosition.Value;
        //killPosition.Raise();
        enemyToKill.Value.AddComponent<EnemyDisabler>();
	}

	public bool WasKillMove()
	{
		//TODO a system that takes the closest enemy instead of the first encountered
		for (int i = 0; i < objectsTouched.GetItems().Count; i++)
		{
			Thing[] things = objectsTouched.GetItems()[i].GetComponents<Thing>();
			for (int j = 0; j < things.Length; j++)
			{
				if (ThingSet.Equals(things[j].RunTimeSet, enemies))
				{
					enemyToKill.SetValue(objectsTouched.GetItems()[i]);
					return true;
				}
				Debug.Log("It did not equal");
			}
			if (objectsTouched.GetItems()[i].gameObject.name =="SpawnCollider")
			{
					enemyToKill.SetValue(objectsTouched.GetItems()[i].transform.parent.gameObject);
					return true;
			}

		}
		return false;
	}

}
