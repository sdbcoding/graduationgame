﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(NoiseMaker))]
public class PlayerKillTimer : OnUpdate {

	public GameObjectVariable enemyToKill;
	public GameEvent killMoveFinished;
	public GameEvent killMoveCanceled;
	public NoiseMaker noiseMaker;
	public ThingSet playerThingSet;
	Thing enemyThingPlayer;
	bool isKilling = false;
	float currentTime;
	float timeToReach;
	float startNoiseRadius;
	float currentRadius;
	float percentageDone;
	void Awake()
	{
		if(noiseMaker == null)
		{
			noiseMaker.GetComponent<NoiseMaker>();
		}
	}
	public void InnitiateKill()
	{
		isKilling = true;
		EnemyKillInformation killInformation = enemyToKill.Value.GetComponent<EnemyKillInformation>();
		if (killInformation == null)
		{
			KillCompleted();
			return;
		}
		timeToReach = killInformation.GetTakedownTime();
		startNoiseRadius = killInformation.GetNoiseRadius();
		currentRadius = startNoiseRadius;
		currentTime = 0;
		percentageDone = 0;
		enemyThingPlayer = enemyToKill.Value.gameObject.AddComponent<Thing>();
		enemyThingPlayer.SetThingSet(playerThingSet);
	}
	public override void UpdateEventRaised()
	{
		if (!isKilling) {return;}
		currentTime += Time.deltaTime;
		percentageDone = currentTime / timeToReach;
		currentRadius = startNoiseRadius * (1-percentageDone);
		
		MakeNoise();
		if (currentTime > timeToReach)
		{
			KillCompleted();
		}
	}
	public void MakeNoise()
	{
		noiseMaker.MakeNoise(enemyToKill.Value.transform.position,currentRadius);
	}
	public void KillCompleted()
	{
		isKilling = false;
		Destroy(enemyThingPlayer);
		killMoveFinished.Raise();
		Debug.Log("Completed");

	}
	public void CancelKill()
	{
		isKilling = false;
		killMoveCanceled.Raise();
		Debug.Log("Cancel");
		Destroy(enemyThingPlayer);
	}
	public void ButtonReleased()
	{
		if (!isKilling) {return;}
		CancelKill();
		
	}
}
