﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashAimingSystem : OnUpdate {
	public ThingSet playerSet;
	private Transform playerTransform;
    private LineRenderer aimingIndicator;
    public GameObject endOfAim;
    public GameObject startOfAim;
    private bool isAiming = false;
    private bool isDashing = false;
    public DashController dashController;
    public LayerMask voidMask;
    public Vector3Variable playerMovementDirection;
    private Vector3 indicatedEndPosition;
    public FloatReference heightOfAiming;
    public FloatReference offsetOfAimingArrowFromBody;
    public FloatReference yValue;

    // Use this for initialization
    void Start () {
		if (playerSet.GetItems().Count != 0)
		{
			playerTransform = playerSet.GetItems()[0].transform;
            aimingIndicator = playerTransform.GetComponent<LineRenderer>();
            aimingIndicator.enabled = false;
            endOfAim.SetActive(false);
            startOfAim.SetActive(false);

        }
	}

    public override void UpdateEventRaised()
    {
        if (!isDashing && isAiming)
        {
            if (!(playerMovementDirection.Value == Vector3.zero))
            {
                Aim();
            }
            else
            {
                CancelAiming();
            }
        }
    }

    private void CancelAiming()
    {
        indicatedEndPosition = playerTransform.position;
        aimingIndicator.enabled = false;
        endOfAim.SetActive(false);
        startOfAim.SetActive(false);
    }

    public void StartAiming()
    {
        isAiming = true;
        indicatedEndPosition = playerTransform.position;
    }

    public void StopAiming()
    {
        isAiming = false;
        aimingIndicator.enabled = false;
        endOfAim.SetActive(false);
        startOfAim.SetActive(false);

        var canDash = dashController.TryDash(indicatedEndPosition);
    }

    public void StartDashing()
    {
        isDashing = true;
    }

    public void StopDashing()
    {
        isDashing = false;
    }

    public void ShowAiming()
    {
        Vector3 normVector = (indicatedEndPosition - playerTransform.position).normalized;
        Vector3 aimStartPosition = playerTransform.position + offsetOfAimingArrowFromBody * normVector;
        Vector3 lineAimStartPosition = playerTransform.position + (0.5f+offsetOfAimingArrowFromBody) * normVector;

        aimingIndicator.SetPosition(0, new Vector3(lineAimStartPosition.x, heightOfAiming, lineAimStartPosition.z));
        aimingIndicator.SetPosition(1, new Vector3(indicatedEndPosition.x, heightOfAiming, indicatedEndPosition.z));
        endOfAim.transform.position = new Vector3(indicatedEndPosition.x, heightOfAiming, indicatedEndPosition.z);
        startOfAim.transform.position = new Vector3(aimStartPosition.x, heightOfAiming, aimStartPosition.z);
    }

    private void Aim()
    {
        RaycastHit hit = new RaycastHit();
        // Debug.Log("AIM");
        //Debug.DrawRay(playerTransform.position, playerTransform.forward, Color.red, 5f);
        var playerPosition = new Vector3(playerTransform.position.x, yValue, playerTransform.position.z);

        if (Physics.Raycast(playerPosition, playerTransform.forward, out hit, 500f, voidMask))
        {
           // Debug.Log("HITAIM");
            aimingIndicator.enabled = true;
            endOfAim.SetActive(true);
            startOfAim.SetActive(true);
            Vector3 normVector = (hit.point - playerTransform.position).normalized;

            indicatedEndPosition = dashController.CalculateCorrectVoidPosition(hit.point + 0.1f * normVector);

            if (dashController.IsLegalEndPosition(indicatedEndPosition))
            {
             //   Debug.Log("SHOW AIM");
                ShowAiming();
            }
            else
            {
               // Debug.Log("CANCEL AIM");
                CancelAiming();
            }
        }
        else
        {
            //Debug.Log("DID NOT HIT ANY LAYER");

            CancelAiming();
        }
    }
}
