﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckDistanceToEnemy : MonoBehaviour {


    [SerializeField]
    public ThingSet enemyList;
    [SerializeField]
    public List<int> enemyIndex = new List<int>();
    [SerializeField]
    public ThingSet killableEnemiesList;
    [SerializeField]
    public FloatVariable maxKillDistance;
    private Transform playerTransform;
    public ThingSet playerSet;

    //[SerializeField]
    //private float terrorLevel;
    [SerializeField]
    public FloatVariable terrorLevelThreshold;
    public GameObjectVariable enemyToKill;
    bool isRunning = true;

    void Start ()
    {
        if (playerSet.GetItems().Count != 0)
        {
            playerTransform = playerSet.GetItems()[0].transform;   
        }
    }

    public void PauseDetection()
    {
        isRunning=false;
        killableEnemiesList.GetItems().Clear();
    }
    public void StartDetection()
    {
        isRunning=true;
    }
    private void Update()
    {
        if (!isRunning){return;}
        //SetAllEnemiesToRed();
        FindAllInSetWithinRange();
        ChangeAllKillableEnemiesToRed();

        //FindClosesWithinRange();
    }
    void SetAllEnemiesToRed()
    {
        for (int i = 0; i < enemyList.GetItems().Count; i++)
        {
            ColorChanger.ChangeColor(enemyList.GetItems()[i].gameObject, Color.red);
        }
    }


    public void FindAllInSetWithinRange()
    {
        killableEnemiesList.GetItems().Clear();
        for (int i = 0; i < enemyList.GetItems().Count; i++)
        {
            if(IsOverTerrorlevelThreshold(enemyList.GetItems()[i].gameObject))
            {
                float distance = Vector3.Distance(enemyList.GetItems()[i].transform.position, playerTransform.position);
                if (distance < maxKillDistance.GetValue()  )
                {
                    killableEnemiesList.Add(enemyList.GetItems()[i]);
                }
            }
        }

    }
    void ChangeAllKillableEnemiesToRed()
    {
        for (int i = 0; i < killableEnemiesList.GetItems().Count; i++)
        {
            //ColorChanger.ChangeColor(killableEnemiesList.Items[i].gameObject, Color.green);    
        }
    }
    bool IsOverTerrorlevelThreshold(GameObject enemy)
    {
        //Terrorlevel doesn't impact possibility of kill
        return true;
        EnemyTerrorLevel terrorScript = enemy.GetComponent<EnemyTerrorLevel>();
        if (terrorScript == null)
        {
            return false;
        }
        if (terrorScript.GetTerrorLevel() >= terrorLevelThreshold.GetValue())
        {
            return true;
        }
        return false;
    }

    public void FindClosesWithinRange()
    {
        if (killableEnemiesList.GetItems().Count == 0)
        {
            enemyToKill.ClearValue();
            return;
        }
        float closestDistance = Vector3.Distance(killableEnemiesList.GetItems()[0].transform.position,playerTransform.position);
        int closestEnemy = 0;
        for (int i = 1; i < killableEnemiesList.GetItems().Count; i++)
        {
            float distance = Vector3.Distance(killableEnemiesList.GetItems()[i].transform.position,playerTransform.position);
            if (distance < closestDistance)
            {
                closestDistance = distance;
                closestEnemy = i;
            }
        }

        //Closest enemy is i        
        //ColorChanger.ChangeColor(killableEnemiesList.GetItems()[closestEnemy].gameObject, Color.green);
        enemyToKill.SetValue(killableEnemiesList.GetItems()[closestEnemy].gameObject);
    }
    

}
