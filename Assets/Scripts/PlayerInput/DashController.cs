﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashController : MonoBehaviour
{

    public ThingSet playerSet;
    private Transform playerTransform;
    public GameEvent startDashing;
    public LayerMask mask;
    public LayerMask voidMask;
    public LayerMask wallMask;
    public Vector3Variable positionTouched;
    public Vector3Variable voidPositionToGoTo;
    public FloatReference yValue;
    public FloatReference voidCharacterOffset;

    void Start()
    {
        if (playerSet.GetItems().Count != 0)
        {
            playerTransform = playerSet.GetItems()[0].transform;
            //Debug.LogError("The player's transform is required on this game object.");
        }

        if (positionTouched == null)
        {
            Debug.LogError("The vector3 for position of touch is required");
        }

        if (voidPositionToGoTo == null)
        {
            Debug.LogError("The vector3 for calculated position is required");
        }

        if (startDashing == null)
        {
            Debug.LogError("The event for start dashing is required");
        }
    }

    public bool IsLegalEndPosition(Vector3 positionTouched)
    {
        var playerPosition = new Vector3(playerTransform.position.x, yValue, playerTransform.position.z);
        float maxDistance = Vector3.Distance(playerPosition, positionTouched);

        var direction = positionTouched - playerPosition;
        

        RaycastHit[] hits = Physics.RaycastAll(playerPosition, direction, maxDistance, mask);
        // Debug.DrawRay(playerTransform.position, direction, Color.red, 5f);

        if (hits.Length > 0)
        {
            //Debug.Log("HITS LENGTH");
            bool legalEndPosition = true;

            bool hitRoom = false;
            bool hitVoid = false;

            for (var i = 0; i < hits.Length; i++)
            {
                if (hits[i].collider.gameObject.tag == "room")
                {
                    //Debug.Log("HIT ROOM");
                    // if already gone through a room it's an illegal end position
                    if (hitRoom)
                    {
                        legalEndPosition = false;
                    }

                    hitRoom = true;
                }
                else if (hits[i].collider.gameObject.tag == "void")
                {
                    hitVoid = true;
                }
                else
                {
                    // set if player is not allowed to go through something specific
                    // legalEndPosition = false;
                }
            }

            // player is only allowed through one and only one room to be able to dash
            if (legalEndPosition && hitVoid && hitRoom)
            {
                return true;
            }
        }

        return false;
    }

    public bool TryDash(Vector3 positionTouched)
    {
        if (IsLegalEndPosition(positionTouched))
        {
            voidPositionToGoTo.SetValue(new Vector3(positionTouched.x, playerTransform.position.y, positionTouched.z));

            startDashing.Raise();
            return true;
        }

        return false;
    }

    public Vector3 CalculateCorrectVoidPosition(Vector3 edgePointOfCollider)
    {
        RaycastHit hit = new RaycastHit();

        if (Physics.Raycast(edgePointOfCollider, Vector3.forward, out hit, 0.2f, wallMask))
        {
            // Debug.Log("forward: " + hit.collider.gameObject.name);
            edgePointOfCollider += Vector3.back * voidCharacterOffset;
        }

        if (Physics.Raycast(edgePointOfCollider, Vector3.right, out hit, 0.2f, wallMask))
        {
            // Debug.Log("right: " + hit.collider.gameObject.name);
            edgePointOfCollider += Vector3.left * voidCharacterOffset;
        }

        if (Physics.Raycast(edgePointOfCollider, Vector3.back, out hit, 0.2f, wallMask))
        {
            // Debug.Log("back: " + hit.collider.gameObject.name);
            edgePointOfCollider += Vector3.forward * voidCharacterOffset;
        }

        if (Physics.Raycast(edgePointOfCollider, Vector3.left, out hit, 0.2f, wallMask))
        {
            // Debug.Log("left: " + hit.collider.gameObject.name);
            edgePointOfCollider += Vector3.right*voidCharacterOffset;
        }

        return edgePointOfCollider;
    }
}
