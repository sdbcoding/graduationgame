﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNavMeshAgentCaller : MonoBehaviour {

	public GameEvent movePlayerByNavMesh;
	

	public void OnSuccessfulTouch()
	{

		movePlayerByNavMesh.Raise();
	}
}
