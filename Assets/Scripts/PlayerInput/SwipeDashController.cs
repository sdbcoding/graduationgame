﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeDashController : OnUpdate {


    List<int> activeTouchIds;
    List<Vector2> startPositions;
    List<float> startTimes;
    public ThingSet playerSet;
    public float minSwipeDistance;
    public float maxSwipeTime;
    public FloatReference maxDistance;
    public FloatReference minDistance;
    public FloatReference deadZoneInPixels;
    public FloatReference acceptableForwardDashDistance;
    public LayerMask maskOfWalls;
    Transform playerTransform;
    public Vector3Variable locationToDashTo;
    public GameEvent checkDashAmo;

    bool isRunning;
    private void Start()
    {
        activeTouchIds = new List<int>();
        startPositions = new List<Vector2>();
        startTimes = new List<float>();
        playerTransform = playerSet.GetItems()[0].transform;
    }

    public override void UpdateEventRaised()
    {
        if(!isRunning){return;}
        for (int i = 0; i < Input.touchCount; i++)
        {
            if (Input.touches[i].phase == TouchPhase.Began)
            {
                BeginTouch(Input.touches[i]);
            }
            else if (Input.touches[i].phase == TouchPhase.Moved)
            {
                CheckTouch(Input.touches[i]);
            } else if (Input.touches[i].phase == TouchPhase.Ended)
            {
                EndTouch(Input.touches[i]);
            }
        }
    }
    void CheckTouch(Touch touch)
    {
        for (int i = 0; i < activeTouchIds.Count; i++)
        {
            if (activeTouchIds[i] == touch.fingerId)
            {
                Vector2 screenPoint = new Vector2(touch.position.x / Screen.width, touch.position.y / Screen.width);
                Vector2 screenToPoint = new Vector2(startPositions[i].x / Screen.width, startPositions[i].y / Screen.width);
                float distance = Vector2.Distance(screenPoint, screenToPoint);
                if (distance > minSwipeDistance && Time.fixedUnscaledTime - startTimes[i] < maxSwipeTime)
                {
                    SetupDashing(startPositions[i], touch.position);
                    EndTouch(touch);
                }
            }
        }
    }
    void EndTouch(Touch touch)
    {
        for (int i = 0; i < activeTouchIds.Count; i++)
        {
            if (activeTouchIds[i] == touch.fingerId)
            {
                activeTouchIds.RemoveAt(i);
                startPositions.RemoveAt(i);
                startTimes.RemoveAt(i);
            }
        }
    }
    void BeginTouch(Touch touch)
    {
        if (touch.position.x > Screen.width /2)
        {
            activeTouchIds.Add(touch.fingerId);
            startPositions.Add(touch.position);
            startTimes.Add(Time.fixedUnscaledTime);

        }
    }
    void SetupDashing(Vector2 startPosition, Vector2 endPosition)
    {
        //Debug.Log(startPosition);
        //Debug.Log(endPosition);
        float distance = Vector3.Distance(startPosition, endPosition);
        if (deadZoneInPixels > distance)
        {
            return;
        }
        Vector2 vec2direction = endPosition - startPosition;
        Vector3 normalizedDirection = new Vector3(vec2direction.x, 0.0f, vec2direction.y).normalized;
        float magnitude = Mathf.Clamp(distance, minDistance, maxDistance);
        normalizedDirection = Quaternion.Euler(new Vector3(0, 45, 0)) * normalizedDirection;
        Vector3 newNormalizedDirection = normalizedDirection + playerTransform.position;

        Vector3 newLocation = (normalizedDirection * (magnitude)) + playerTransform.position;
        newLocation.y = 0.5f;
        //GameObject temp = new GameObject();
        //temp.transform.position = newLocation;

        RaycastHit hit;
        Physics.Raycast(newLocation, Vector3.down, out hit, 2.0f);
        //Debug.Log(newLocation + " player position: " + playerTransform.position + " magnitude is: " + magnitude + " with normalizaed Direction: " + normalizedDirection + " together: " + (normalizedDirection * magnitude));
        if (hit.collider == null)
        {
            
            //Theres is no ground underneath you dashing position
            if (RaycastForward(newLocation,normalizedDirection))
            {
                return;
            }
            RaycastBackwards(newLocation,normalizedDirection);
            
            //Debug.Log("");
            return;
        }

        //Debug.Log(LayerMask.LayerToName(hit.collider.gameObject.layer));
        if (LayerMask.LayerToName(hit.collider.gameObject.layer) == "Ground")
        {

            StartDashing(newLocation);
        }
    }
    bool RaycastForward(Vector3 position, Vector3 direction)
    {
        RaycastHit hit;
        Physics.Raycast(position,direction,out hit,acceptableForwardDashDistance,maskOfWalls);
        // Debug.DrawRay(position,direction,Color.red,10.0f);
        // Debug.DrawRay(position,direction,Color.red,10.0f);

        if (hit.collider != null)
        {
            // Debug.Log("RaycastForward hit: " + hit.collider.gameObject.name);
            StartDashing(hit.point + direction * 0.5f);
            return true;
        }
        return false;
    }
    void RaycastBackwards(Vector3 position, Vector3 direction)
    {
        RaycastHit hit;
        Physics.Raycast(position,-direction,out hit,Vector3.Distance(position,playerTransform.position),maskOfWalls);
        // Debug.DrawRay(position,-direction,Color.red,10.0f);

        if(hit.collider == null)
        {
            // Debug.Log("Backwards is null, lul not possible");
            return;
        }
            // Debug.Log("RaycastForward hit: " + hit.collider.gameObject.name);

        StartDashing(hit.point + (-direction * 0.5f));
    }

    void StartDashing(Vector3 position)
    {
        locationToDashTo.SetValue(position);
            //startDashing.Raise();
        checkDashAmo.Raise();
    }

    public void PlayerSpawned()
    {
        isRunning = true;
    }
}