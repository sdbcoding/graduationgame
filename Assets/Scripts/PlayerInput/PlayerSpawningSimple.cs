﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawningSimple : OnUpdate {

	public Vector3Variable playerSpawningPosition;
	public GameObjectVariable enemyThatsKilled;
	public FloatReference playerSpawnTime;
	bool isGrowing;
	float currentValue = 0.01f;
	float currentTime;
    public GameObject blobbyBoii;
    public GameObject blobbyBoiiSpawn;

    // Use this for initialization
    void Start () {
		//transform.localScale = new Vector3(0.01f,0.01f,0.01f);
	}
	public void SpawnPlayer()
	{
		Vector3 newPosition = playerSpawningPosition.Value;
		//Vector3 newPosition = enemyThatsKilled.Value.transform.position;
		newPosition.y = 0.1f;
		transform.position = newPosition;
        
        isGrowing = true;
	}

	public override void UpdateEventRaised()
	{
		if (!isGrowing) {return;}
		currentTime += Time.deltaTime;
		currentValue = currentTime / playerSpawnTime.Value;
		//Debug.Log(currentValue);

        if(currentValue > 0.5f)
        {
            blobbyBoiiSpawn.SetActive(true);
        }

		if (currentValue > 1.0f)
		{
			transform.localScale = Vector3.one;
            // this.SetActive(false);
            blobbyBoiiSpawn.SetActive(false);
            blobbyBoii.SetActive(true);
			Destroy(this);


            return;
		}
		//transform.localScale = new Vector3(currentValue,currentValue,currentValue);
	}
}
