﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MouseInputGetter : OnUpdate {

	public Vector3Variable groundPositionOfTouch;
	private Camera mainCamera;
	public GameObjectSet objectsTouched;
	public FloatReference maxDistanceOfRay;
	public LayerMask layerMaskToRaycastThrough;
	public GameEvent successfulTouchEvent;
	public FloatReference yValue;
	
	public GraphicRaycaster uiRaycaster;
	PointerEventData pointerEventData;
	public EventSystem eventSystem;
	public bool isRunning = true;

	void Start()
	{
		mainCamera = Camera.main;
		if (uiRaycaster == null)
		{
			uiRaycaster = GetComponent<GraphicRaycaster>();
		}
	}
	
	public override void UpdateEventRaised()
	{
		if (!isRunning) {return;}
		if (Input.GetMouseButtonDown(0))
		{
			CheckIfGround();
		}
    }
	
	void CheckIfGround()
	{
		
		FindGroundAndGameObjectsFromHits(RaycastFromScreenPosition(Input.mousePosition));
        UIRaycasting();
        SuccessfulTouch();

    }
	void UIRaycasting()
	{
		List<RaycastResult> results = new List<RaycastResult>();
		pointerEventData = new PointerEventData(eventSystem);
		pointerEventData.position = Input.mousePosition;
		uiRaycaster.Raycast(pointerEventData,results);
		//Debug.Log("Size: " + results.Count);
		for (int i = 0; i < results.Count; i++)
		{
			//Debug.Log(results[i].gameObject.name);
			objectsTouched.Add(results[i].gameObject);
			
		}
	}

	RaycastHit[] RaycastFromScreenPosition(Vector2 screenPosition)
	{
		RaycastHit[] hits;
		Ray positionInWorldSpace = mainCamera.ScreenPointToRay(screenPosition);
		hits = Physics.RaycastAll(positionInWorldSpace,maxDistanceOfRay, layerMaskToRaycastThrough);
		return hits;
	}
	void FindGroundAndGameObjectsFromHits(RaycastHit[] hits)
	{
		objectsTouched.SetItems( new List<GameObject>());
		for (int i = 0; i < hits.Length; i++)
		{
			objectsTouched.Add(hits[i].collider.gameObject);
			//Debug.Log(hits[i].collider.gameObject.name);
			if (hits[i].collider.gameObject.layer == LayerMask.NameToLayer("Ground"))
			{
				Vector3 yCorrectedHitPoint = hits[i].point;
				yCorrectedHitPoint.y = yValue;
				groundPositionOfTouch.SetValue(yCorrectedHitPoint);
				
			}
		}
	}
	void SuccessfulTouch()
	{
		successfulTouchEvent.Raise();
		//Debug.Log("Successful touch");
	}
}
