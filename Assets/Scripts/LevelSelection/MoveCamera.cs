﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : OnUpdate
{
    private Vector2 worldStartPoint;
    public Camera mainCamera;
    public FloatReference minY;
    public FloatReference maxY;
    bool isRunning = false;

    public override void UpdateEventRaised()
    {
        if (!isRunning)
        {
            return;
        }

        if (Input.touchCount == 1)
        {
            Touch currentTouch = Input.GetTouch(0);

            if (currentTouch.phase == TouchPhase.Began)
            {
                worldStartPoint = GetWorldPoint(currentTouch.position);
            }

            if (currentTouch.phase == TouchPhase.Moved)
            {
                Vector2 worldDelta = GetWorldPoint(currentTouch.position) - worldStartPoint;
                var changeCamera = worldDelta.y;
                
                if (changeCamera > 0)
                {
                    if ((mainCamera.transform.position.y) < minY)
                    {
                        changeCamera = 0;
                    }
                    else if ((mainCamera.transform.position.y - changeCamera) < minY)
                    {

                        changeCamera = mainCamera.transform.position.y - minY;
                    }
                }
                else
                {
                    if ((mainCamera.transform.position.y) > maxY )
                    {
                        changeCamera = 0;
                    }
                    else if ((changeCamera + mainCamera.transform.position.y) > maxY)
                    {

                        changeCamera = maxY - mainCamera.transform.position.y;
                    }
                }
                
                //if ((mainCamera.transform.position.y > minY && worldDelta.y > 0) || (mainCamera.transform.position.y < maxY && worldDelta.y < 0))
                //{
                mainCamera.transform.Translate(0, -changeCamera, 0);
                //}                
            }
        }
    }

    // convert screen point to world point
    private Vector2 GetWorldPoint(Vector2 screenPoint)
    {
        RaycastHit hit;
        Physics.Raycast(mainCamera.ScreenPointToRay(screenPoint), out hit);
        return hit.point;
    }

    public void SetRunning()
    {
        isRunning = true;
    }

    public void StopRunning()
    {
        isRunning = false;
    }
}
