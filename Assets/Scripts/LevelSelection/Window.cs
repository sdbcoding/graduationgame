﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Window : MonoBehaviour {

    public Material originalMaterial;

    public void SetMaterialToOriginal()
    {
        GetComponent<MeshRenderer>().material = originalMaterial;
    }
}
