﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetToDefault : MonoBehaviour {

    public SettingsControllerLanguage languageController;
    public SettingsControllerSound soundController;
    public GameEvent resetDefault;

	public void ResetDefult()
    {
        // Levels
        PlayerPrefs.DeleteAll();
        resetDefault.Raise();

        // Sound
        soundController.ResetDefaultSound();

        // Language
        languageController.ResetDefaultLanguage();
    }
}
