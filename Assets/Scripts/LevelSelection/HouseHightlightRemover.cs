﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseHightlightRemover : MonoBehaviour {

	public ThingSet houses;
	bool[] showHightlight;
	Level[] levels;
	bool isFirstFrame = true;
	bool shouldIshow; 

	void Awake()
	{
		shouldIshow = ShowMainMenuStaticClass.showMainMenu;
	}
	
	
	// Update is called once per frame
	void Update () {
		if (isFirstFrame)
		{
			isFirstFrame = false;
			FindHousesWithHightlight();
			if(shouldIshow)
			{
				RemoveHighLight();
			} else {
				ShowHightLight();
				Camera.main.transform.GetChild(0).gameObject.SetActive(true);
			}
		}
	}
	void FindHousesWithHightlight()
	{
		showHightlight = new bool[houses.GetItems().Count];
		levels = new Level[houses.GetItems().Count];
		for (int i = 0; i < showHightlight.Length; i++)
		{
			levels[i] = houses.GetItems()[i].GetComponent<Level>(); 
  			showHightlight[i] = !(levels[i].levelStatus == 0);
		}
	}
	
	public void RemoveHighLight()
	{
		for (int i = 0; i < levels.Length; i++)
		{
			levels[i].RemoveHightlight();
		}
	}
	public void ShowHightLight()
	{
		for (int i = 0; i < levels.Length; i++)
		{
			if(showHightlight[i])
			{
				levels[i].ShowHighlight(levels[i].levelStatus);
			}
		}
	}
}
