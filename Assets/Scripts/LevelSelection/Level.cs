﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Level : MonoBehaviour, IPointerClickHandler{

    public int levelStatus;
    public GameObject[] levelThatThisUnlocks;
    public SceneSet sceneSet;
    public SceneSetVariable sceneSetLoader;
    public LoadSceneFromScript sceneLoader;
    public bool isUnlockedFromStart;
    public GameEvent startGame;
    public GameObject[] windows;
    public Material blackWindow;

    const int locked = 0;
    const int unlocked = 1;
    const int completed = 2;
    
    void Start()
    {
        //PlayerPrefs.DeleteAll();
        SetPlayerPrefs();
    }

    public void SetPlayerPrefs()
    {
        string levelName = gameObject.name;

        if (PlayerPrefs.HasKey(levelName))
        {
            SetLevelStatus(PlayerPrefs.GetInt(levelName));
        }
        else
        {
            if (isUnlockedFromStart)
            {
                SetLevelStatus(unlocked);
            }
            else
            {
                SetLevelStatus(locked);
            }
        }

        string lastCompletedLevel = PlayerPrefs.GetString("LastCompletedLevel");

        if (!(lastCompletedLevel == "") && lastCompletedLevel == gameObject.name)
        {
            PlayerPrefs.SetString("LastCompletedLevel", "");

            Debug.Log(lastCompletedLevel);
            if( lastCompletedLevel == "level04" )
            {
                LoadFinalCredits();
            }   

            if (levelStatus == completed)
            {
                return;
            }

            SetLevelStatus(completed);

            foreach (GameObject nextLevel in levelThatThisUnlocks)
            {
                if (nextLevel.GetComponent<Level>().levelStatus == locked)
                {
                    nextLevel.GetComponent<Level>().SetLevelStatus(unlocked);
                }
            }      
        }
    }

    private void LoadFinalCredits()
    {
        Debug.Log("Final Credits called");
        AkSoundEngine.PostEvent("PauseAll",gameObject);
        // Debug.Log(Resources.Load("Credits/FinalCreditsSet"));
        sceneSetLoader.SetValue(Resources.Load("Credits/FinalCreditsSet") as SceneSet);
        sceneLoader.LoadScene();
    }

    public void LoadLevel()
    {
        sceneSetLoader.SetValue(sceneSet);
        sceneLoader.LoadScene();
        startGame.Raise();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!(levelStatus == locked))
        {
            AkSoundEngine.PostEvent("UIStartGame",gameObject);
            LoadLevel();
        } else {
            AkSoundEngine.PostEvent("UINoClick",gameObject);
        }
    }

    public void SetLevelStatus(int status)
    {
        levelStatus = status;
        PlayerPrefs.SetInt(gameObject.name, status);

        if (status == locked)
        {
            GetComponent<MeshRenderer>().enabled = false;
        }
        else
        {
            GetComponent<MeshRenderer>().enabled = true;
        }

        foreach (GameObject window in windows)
        {
            if (status == unlocked)
            {
                window.GetComponent<Window>().SetMaterialToOriginal();
            }
            else
            {
                window.GetComponent<MeshRenderer>().material = blackWindow;
            }
        }
    }
    public void ShowHighlight(int status)
    {
        GetComponent<MeshRenderer>().enabled = true;

        if (status == unlocked)
        {
            foreach (GameObject window in windows)
            {
                window.GetComponent<Window>().SetMaterialToOriginal();
            }
        }
        else
        {
            foreach (GameObject window in windows)
            {
                window.GetComponent<MeshRenderer>().material = blackWindow;
            }
        }
    }
    public void RemoveHightlight()
    {
        foreach (GameObject window in windows)
        {
            window.GetComponent<MeshRenderer>().material = blackWindow;
        }
        GetComponent<MeshRenderer>().enabled = false;
    }
}
