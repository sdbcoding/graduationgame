﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowFinalScoreInLevel : MonoBehaviour {

    public FloatReference finalScore;

    void OnEnable()
    {
        GetComponent<Text>().text = finalScore.Value.ToString();
    }
    
}
