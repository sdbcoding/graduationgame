﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempPointOfInterest : MonoBehaviour {

	public ThingSet enemies;
	float maxDistance = 2.5f;
	bool isCompleted = false;
	bool isAnimating = true;
	public GameEvent pointOfInterestCompleted;
	public FloatVariable score;
	public FloatReference poiScoreValue;
    public MouthBlendShape mbs;
	GameObject enemy;
	


	//Animation
	float currentTime = 0;
	//-----Movement
	Vector3 enemyStartPosition;
	int movementCurrentStage = 0;
	float moveOnTopTime = 0.5f;
	float stayOnTopTime = 0.7f;
	float fallDownTime = 0.2f;
	
	//-----Rotation
	int rotationCurrentStage = 0;
	
	// Update is called once per frame
	void LateUpdate () {
		if (isCompleted) {
			if (!isAnimating) {return;}
				Animating();
			
				return;
			}
		for (int i = 0; i < enemies.GetItems().Count; i++)
		{
			Vector3 newEnemyPosition = enemies.GetItems()[i].transform.position;
			newEnemyPosition.y = transform.position.y;
			float distance = Vector3.Distance(newEnemyPosition,transform.position);
			//Debug.Log(distance);
			if (distance < maxDistance)
			{
				EnemyRigigbodyPushBack rigigbodyPushBack = enemies.GetItems()[i].GetComponent<EnemyRigigbodyPushBack>();
				if (rigigbodyPushBack != null)
				{
					FinishPoint(enemies.GetItems()[i].gameObject);
					return;

				}
			}
		}
	}
	void FinishPoint(GameObject enemy)
	{
		isCompleted = true;
		score.ApplyChange(poiScoreValue.Value);
		pointOfInterestCompleted.Raise();
		// GetComponentInChildren<RitualUIObject>().Activate();
        AkSoundEngine.PostEvent("WormMouthKill", gameObject);
        //this.enabled = false;
		this.enemy = enemy;
		enemyStartPosition = enemy.transform.position;
		StopEnemy();
    }
	void StopEnemy()
	{
		enemy.AddComponent<EnemyDisabler>();
		enemy.GetComponent<EnemyBrain>().enabled = false;
		enemy.GetComponent<MMAnimationSelector>().PushBack();
		enemy.GetComponent<MMAnimationSelector>().Pause();
		enemy.GetComponent<MMAnimationSelector>().enabled = false;
		enemy.GetComponent<Animator>().enabled = false;
		enemy.transform.GetComponentInChildren<DetatchedHead>().enabled = false;
		enemy.transform.GetComponentInChildren<VictimHeadAnimatorControl>().enabled = false;
		
	}
	void Animating()
	{
		//Move the character slightly above the mouth. Rotate him. Then let him fall fast
		currentTime += Time.deltaTime;
		
		//Movement
		switch (movementCurrentStage)
		{
			case 0:
			MoveToOverMouth();
			break;
			case 1:
			StayOnTop();
			break;
			case 2:
			MoveDown();
			break;
			case 3:
			FinishAnimation();
			return;
		}


		Rotation();

		return;
		switch (rotationCurrentStage)
		{
			case 0:
			break;
			case 1:
			break;
			case 2:
			break;
		}
	}
	void NextStage()
	{
		movementCurrentStage++;
		currentTime = 0;
	}
	void Rotation()
	{
		//Debug.Log(enemy.transform.rotation);
		//Quaternion newRotation = Quaternion.
		//Debug.Log("New rotation: " +  newRotation.eulerAngles);
		//enemy.transform.rotation = newRotation;
		enemy.transform.Rotate(Vector3.up,2);
		//Debug.Log(enemy.transform.rotation);

	}
	void MoveToOverMouth()
	{
		Vector3 positionToMoveTo = transform.position;
		positionToMoveTo.y += 1;
		Vector3 newPosition = Vector3.Slerp(enemyStartPosition,positionToMoveTo,currentTime/moveOnTopTime);
		if (currentTime > moveOnTopTime)
		{
			NextStage();
			return;
		}
		enemy.transform.position = newPosition;
	}
	void StayOnTop()
	{
		Vector3 positionToMoveTo = transform.position;
		positionToMoveTo.y += 1;
		enemy.transform.position = positionToMoveTo;
		enemyStartPosition = enemy.transform.position;
		if(currentTime > stayOnTopTime)
		{
			NextStage();
		}
	}
	void MoveDown()
	{
		Vector3 positionToMoveTo = transform.position;
		positionToMoveTo.y -= 2;
		Vector3 newPosition = Vector3.Lerp(enemyStartPosition,positionToMoveTo,currentTime/fallDownTime);
		if (currentTime > fallDownTime)
		{
			NextStage();
			return;
		}
		enemy.transform.position = newPosition;
	}

	void FinishAnimation()
	{
		isAnimating = false;
		enemy.SetActive(false);
        mbs.enabled=true;
	}
	public bool GetIsCompleted()
	{
		//Debug.Log(isCompleted);
		return isCompleted;
	}
	
}
