﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RitualUIObject : MonoBehaviour {

	RectTransform rect;
	Image image;
	public void Activate()
	{
		rect = GetComponent<RectTransform>();
		image = GetComponent<Image>();
		InvokeRepeating("Grow",Time.deltaTime,Time.deltaTime);
	}

	void Grow()
	{
		Vector3 newPosition = rect.position;
		newPosition.y += Time.deltaTime * 1.5f;
		rect.position = newPosition;
		rect.sizeDelta = new Vector2(rect.sizeDelta.x *1.05f,rect.sizeDelta.y * 1.05f);
		Color newColor = image.color;
		newColor.a -= Time.deltaTime *0.75f;
		image.color = newColor;
		if (newColor.a <= 0.0f)
		{
			CancelInvoke("Grow");
			gameObject.SetActive(false);
			//transform.parent.parent.gameObject.SetActive(false);
		}
	}
}
