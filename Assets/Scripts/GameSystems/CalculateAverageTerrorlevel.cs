﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalculateAverageTerrorlevel : MonoBehaviour {
	public FloatVariable averageTerrorLevel;
	public FloatReference maxTerrorLevel;
	public ThingSet enemies;
	private int enemyCount = 0;
	private float maxPossibleTerrorLevel; 

	void Start()
	{
		maxPossibleTerrorLevel = maxTerrorLevel * enemies.GetItems().Count;
	}
	
	void Update () {
		CheckTerrorLevel();
	}
	void CheckTerrorLevel()
	{
		float combinedValue = 0.0f;
		for (int i = 0; i < enemies.GetItems().Count; i++)
		{
			if( enemies.GetItems()[i].GetComponent<EnemyTerrorLevel>())
			{
				combinedValue += enemies.GetItems()[i].GetComponent<EnemyTerrorLevel>().GetTerrorLevel();
				enemyCount++;
			}				
		}
		float enemiesMissing = (maxPossibleTerrorLevel / maxTerrorLevel) - enemies.GetItems().Count;

		combinedValue += enemiesMissing * maxTerrorLevel;
		/* TODO: Now the level divides between the number of enemies in Enemy ThingSet. 
		But just in case we make enemies without terror levels, we should use enemyCount instead*/
		
		averageTerrorLevel.SetValue((combinedValue / maxPossibleTerrorLevel)*100);
		AkSoundEngine.SetRTPCValue("RTPCTerrorLevel", averageTerrorLevel.GetValue() );
		//Debug.Log(averageTerrorLevel.GetValue());
	}
}
