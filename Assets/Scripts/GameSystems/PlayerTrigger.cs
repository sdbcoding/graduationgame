﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Events;

public class PlayerTrigger : MonoBehaviour {
	[Range(0,100)]
	public float triggerRange;
	public bool isTriggerOnce = true;
	public UnityEvent response;
	[Space]
	public ThingSet playerSet;
	Transform playerTransform;

	// Use this for initialization
	void Start () {
		playerTransform = playerSet.GetItems()[0].transform;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 playerTrans = playerTransform.position;
		playerTrans.y = transform.position.y;
		float distance = Vector3.Distance(playerTrans,transform.position);
		if (distance < triggerRange)
		{
			if (isTriggerOnce)
			{
				response.Invoke();
				Destroy(this);
				return;
			}
		}
	}
}
