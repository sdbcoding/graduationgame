﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class TutorialOverlay : MonoBehaviour,IPointerClickHandler {
	public FloatReference fadeInTimeForOverlay;
	public Sprite[] images;
	int currentPosition;
	public GameEvent pauseGame;
	public GameEvent startGame;
	public UnityEvent closeResponse;
	bool canDisable = false;
	public void OnEnable()
	{
		GetComponent<Image>().sprite = images[0];
		AlphaChanger changed = gameObject.AddComponent<AlphaChanger>();
		changed.StartChange(1,fadeInTimeForOverlay.Value,FadeInIsDone);
		pauseGame.Raise();
	}
	public void FadeInIsDone()
	{
		canDisable = true;
	}

    public void OnPointerClick(PointerEventData eventData)
    {
        if (canDisable)
		{
				currentPosition ++;
			if (images.Length> currentPosition)
			{
				//change the image;
				GetComponent<Image>().sprite = images[currentPosition];
				return;
			}
			startGame.Raise();
			closeResponse.Invoke();
			Destroy(gameObject);
		}
    }
}
