﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialDisablers : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GameObject.Find("WinChecker").SetActive(false);
		GameObject.Find("LoseChecker").SetActive(false);
		Camera.main.gameObject.GetComponent<CameraCinematicController>().enabled = false;
	}
	
	
}
