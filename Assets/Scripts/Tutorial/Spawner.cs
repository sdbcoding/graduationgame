﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

	GameObject objectToSpawn;
	public GameObject startingEnemy;
	public TempPointOfInterest poiPoint;
	public GameEvent wonGame;
	Vector3 spawnPosition;
	public GameObject spawnIndicator;
	void Awake()
	{
		Debug.Log("Enemy 3 awake");
		spawnPosition = startingEnemy.transform.position;
		spawnIndicator.SetActive(false);
		objectToSpawn =  Instantiate(startingEnemy,spawnPosition,Quaternion.identity);
		startingEnemy.SetActive(true);
		Debug.Log("Enemy 3 set to active true");
	}

	public void SpawnItem()
	{
		Debug.Log("New Enemy 3 spawned");
		if (!poiPoint.GetIsCompleted())
		{
			GameObject tempObj = Instantiate(objectToSpawn,spawnPosition,Quaternion.identity);
			tempObj.SetActive(true);
		} else {
			wonGame.Raise();
		}
	}
}
