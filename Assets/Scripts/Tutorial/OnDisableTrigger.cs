﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnDisableTrigger : MonoBehaviour {

	public UnityEvent response;
	
	void OnDisable()
	{
		response.Invoke();
	}
}
