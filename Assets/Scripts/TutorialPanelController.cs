﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialPanelController : MonoBehaviour {

    public GameObject tutoiralPanel;
    public GameObject rightButton;
    public GameObject leftButton;
    public GameObject[] circles;
    public GameObject[] pages;

    Transform leftButtonText;
    Transform rightButtonText;

    int indexOfVisiblePage = 0;

    public FloatReference iconMoveSpeed;
    public FloatReference iconHeight;

    void Start()
    {
        indexOfVisiblePage = 0;
        leftButton.SetActive(false);
        rightButton.SetActive(true);

        leftButtonText = leftButton.transform.GetChild(0);
        rightButtonText = rightButton.transform.GetChild(0);

        for (var i=0; i < pages.Length; i++)
        {
            if (i==0)
            {
                circles[i].GetComponent<Image>().color = Color.red;
                pages[i].SetActive(true);
            }
            else
            {
                circles[i].GetComponent<Image>().color = Color.white;
                pages[i].SetActive(false);
            }
        }        
    }

    void Update()
    {        
        float newY = iconHeight.Value*Mathf.Sin(Time.time * iconMoveSpeed.Value)*Time.deltaTime;
        leftButtonText.position = new Vector3(leftButtonText.position.x, leftButtonText.position.y + newY, leftButtonText.position.z);
        rightButtonText.position = new Vector3(rightButtonText.position.x, rightButtonText.position.y + newY, rightButtonText.position.z);
    }

    public void OnLeftButtonClicked()
    {
        SetButtonsActive();
        ChangePage(indexOfVisiblePage, false, Color.white);

        indexOfVisiblePage--;

        ChangePage(indexOfVisiblePage, true, Color.red);

        if (indexOfVisiblePage == 0)
        {
            leftButton.SetActive(false);
        }

    }

    public void OnRightButtonClicked()
    {
        SetButtonsActive();
        ChangePage(indexOfVisiblePage, false, Color.white);

        indexOfVisiblePage++;

        ChangePage(indexOfVisiblePage, true, Color.red);

        if (indexOfVisiblePage == (pages.Length-1))
        {
            rightButton.SetActive(false);
        }
    }

    private void ChangePage(int index, bool activity, Color color)
    {
        circles[index].GetComponent<Image>().color = color;
        pages[index].SetActive(activity);
    }

    private void SetButtonsActive()
    {
        rightButton.SetActive(true);
        leftButton.SetActive(true);
    }

    public void ToggleTutoiralPanel(){
        tutoiralPanel.SetActive(!tutoiralPanel.activeSelf);
    }
}
