﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DashSlider : OnUpdate {

    public FloatReference currentDashPercentage;
    public FloatReference maxDashPercentage;
    Slider slider;
    

    private void Start()
    {
        slider = GetComponent<Slider>();
        slider.maxValue = maxDashPercentage;
        slider.minValue = 0;
    }

    public override void UpdateEventRaised()
    {
        slider.value = currentDashPercentage.Value;
    }
}
