﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;



namespace Utility {

	public class TreeNode : IComparable<TreeNode>
	{
		public Frame frame;
		public float distance;

		public TreeNode(Frame _frame, float _distance)
		{
			frame = _frame;
			distance = _distance;
		}

		public int CompareTo(TreeNode node)
		{
			return distance.CompareTo(node.distance);
		}
	}

	/// HyperRect is used in order to maintain the zones each node will look at
	public struct HyperRect
	{
		public float[] maxPoint;
		public float[] minPoint;
		public int Dimension;


		public static HyperRect CreateInfinite(int dim)
		{
			var rect = default(HyperRect);
			rect.Dimension = dim;
			rect.minPoint = new float[dim];
			rect. maxPoint = new float[dim];

			for ( int i = 0; i < dim; i++)
			{
				rect.minPoint[i] = float.MinValue;
				rect.maxPoint[i] = float.MaxValue;
			}

			return rect;
		}
		public HyperRect Clone()
		{
			var rect = default(HyperRect);
			rect.minPoint = this.minPoint;
			rect.maxPoint = this.maxPoint;
			rect.Dimension = this.Dimension;
			return rect;
		}

		public float[] ClosestCorner(float[] target)
		{
			if(this.Dimension != target.Length)
			{
				throw new System.ArgumentException("Target dimension needs to be the same as rect");
			}

			var closest = new float[this.Dimension];
			
			// Based on https://github.com/MathFerret1013/Supercluster.KDTree/blob/master/KDTree/HyperRect.cs
			for (var i = 0; i < target.Length; i++)
			{
				// closest[<i] is the closer point to the target for that dim
				if (this.minPoint[i].CompareTo(target[i]) > 0)
				{
					closest[i] = this.minPoint[i];
				}
				else if (this.maxPoint[i].CompareTo(target[i]) < 0)
				{
					closest[i] = this.maxPoint[i];
				}
				else
				{
					closest[i] = target[i];
				}
			}

			return closest;
		}
	}


	public class KDTree
	{

		readonly int Dimension;
		public Frame[] InfoList;
		public KDTree(Frame[] infoList)
		{
			InfoList = infoList;
			Dimension = infoList[0].dataPoint.Length;
		}

		public static int Left(int index)
		{
			return index * 2 + 1;
		}

		public static int Right(int index)
		{
			return index * 2 + 2;
		}

		public static float DistanceSquared(float[] pointA, float[] pointB)
		{
			if(pointA.Length != pointB.Length)
			{
				throw new System.IndexOutOfRangeException("Points should have the same dimension");
			}
			float sum = 0;
			for(int i = 0; i < pointA.Length; i++)
			{
				sum += (pointA[i] - pointB[i]) * (pointA[i] - pointB[i]);
			}

			return sum;
		}

		public List<TreeNode> Nearest(float[] target, int k)
		{
			List<TreeNode> nearest = new List<TreeNode>();
			HyperRect rect = HyperRect.CreateInfinite(this.Dimension);
			Nearest(nearest, 0, this.Dimension, target, rect, k);
			return nearest;
		}

		private void Nearest(List<TreeNode> nearests, int nodeIdx, int dimension, float[] target, HyperRect rect, int k)
		{

			if(nodeIdx > InfoList.Length -1 || InfoList[nodeIdx] == null)
			{
				return;
			}
			Frame current = InfoList[nodeIdx];

			var dim = dimension % this.Dimension;

			var leftRect = rect.Clone();
			leftRect.maxPoint[dim] = current.dataPoint[dim];

			var rightRect = rect.Clone();
			rightRect.minPoint[dim] = current.dataPoint[dim];

			var compare = target[dim].CompareTo(current.dataPoint[dim]);

			var nearerRect = compare <= 0 ? leftRect : rightRect;
            var furtherRect = compare <= 0 ? rightRect : leftRect;

			int nearerNodeIdx = compare <= 0 ? Left(nodeIdx) : Right(nodeIdx);
            int furtherNodeIdx = compare <= 0 ? Right(nodeIdx) : Left(nodeIdx);

			// Go down the nearer side
			this.Nearest(nearests, nearerNodeIdx, dimension+1, target, nearerRect, k);


			float CurrentDistance = KDTree.DistanceSquared(current.dataPoint, target);
			// Check the other side
			var closestCorner = furtherRect.ClosestCorner(target);
			var distanceToFurther = KDTree.DistanceSquared(closestCorner, target);

			// Check further branch if not full or if further is closer that furthest NN
			if(nearests.Count <= k)
			{
				Nearest(nearests, furtherNodeIdx, dimension+1, target, furtherRect, k);
			} else {
				TreeNode lastNode = nearests[k-1];
				if(distanceToFurther < lastNode.distance)
				{
					Nearest(nearests, furtherNodeIdx, dimension+1, target, furtherRect, k);
				}
			}

			// Add current if it's nearer or nearest not full
			if(nearests.Count < k)
			{
				nearests.Add(new TreeNode(current, CurrentDistance));
				nearests.Sort();
			} else
			{
				TreeNode lastNode = nearests[k-1];
				if(lastNode.distance > CurrentDistance)
				{
					nearests[k-1] = new TreeNode(current, CurrentDistance);
					nearests.Sort();
				}
			}
		}

	}

}
