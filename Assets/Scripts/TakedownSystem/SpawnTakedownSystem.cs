﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTakedownSystem : MonoBehaviour {
	public FloatVariable numberOfSigns;
	public FloatVariable clearedNumberOfSigns;
	public GameObjectVariable enemyToKill;
	public GameObject[] signs;
	public GameObject takedownCircle;
	
	public void ActivateTakedownSystem()
	{
		Vector3 newtakedownSystemPosition = enemyToKill.Value.transform.position;
		newtakedownSystemPosition.y = 0.1f;
		takedownCircle.GetComponent<RectTransform>().position = newtakedownSystemPosition;
		SetupNumberOfSign();
		SetupTakedownCircle();
		
	}
	void SetupTakedownCircle()
	{
		takedownCircle.SetActive(true);
		List<int> circleNumbers = new List<int>();
		for (int i = 0; i < signs.Length; i++)
		{
			signs[i].SetActive(false);
			if (i <numberOfSigns.GetValue())
			{
				circleNumbers.Add(i);
			}
		}
		
		int[] numbers = circleNumbers.ToArray();
		
		numbers = RandomNumbers(numbers,6);
		//ArrayUtility.Shuffle(numbers);
		/* for (int i = 0; i < numbers.Length; i++)
		{
			Debug.Log(numbers[i]);
		}*/

		for (int i = 0; i < numberOfSigns.GetValue(); i++)
		{
			signs[numbers[i]].SetActive(true);
		}
		clearedNumberOfSigns.SetValue(0);

	}

	int[] RandomNumbers(int[] numbersToRandomize, int maxpossibleValue)
	{
		int[] backup = numbersToRandomize;
		for (int i = 0; i < numbersToRandomize.Length; i++)
		{
			numbersToRandomize[i] = -1;
		}
		int currentNumber = 0;
		bool shouldBreak = false;
		int beanCounter = 0;
		do
		{
			beanCounter++;
			if (beanCounter > 50)
			{
				return backup;
			}

			int newNumber = Random.Range(0,maxpossibleValue);
			bool numberIsValid = true;
			for (int i = 0; i < numbersToRandomize.Length; i++)
			{
				if (i == currentNumber)
				{
					continue;
				}
				if (numbersToRandomize[i] == newNumber)
				{
					numberIsValid = false;
				} 
			}
			if (numberIsValid)
			{
					numbersToRandomize[currentNumber] = newNumber;
				currentNumber++;
			}
			if (currentNumber == numbersToRandomize.Length)
			{
				shouldBreak = true;
			}
		} while(!shouldBreak);
		return numbersToRandomize;
	}
	public void DisalbeAllSigns()
	{
		takedownCircle.SetActive(false);
		for (int i = 0; i < signs.Length; i++)
		{
			signs[i].GetComponent<TakedownSign>().Reset();
		}
	}

	void SetupNumberOfSign()
	{
		EnemyTerrorLevel terrorScript = enemyToKill.Value.GetComponent<EnemyTerrorLevel>();
		numberOfSigns.SetValue(GetRequiredNumberOfSigns(terrorScript));
	}

	int GetRequiredNumberOfSigns(EnemyTerrorLevel terrorScript)
	{
		int terrorLevel = (int)terrorScript.GetTerrorLevel();
		if (terrorLevel == 0)
		{
			return 6;
		} else if (terrorLevel == 1)
		{
			return 4;
		} else if (terrorLevel == 2)
		{
			return 3;
		}
		return 3;
	}
	

}
