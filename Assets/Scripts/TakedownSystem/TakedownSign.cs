﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TakedownSign : MonoBehaviour,IPointerClickHandler {

	public Vector3Variable positionOfDashMovement;
	public FloatVariable clearedNumberOfSigns;
	public GameEvent dashStart;
	public Image image;
	bool isActive = false;
	bool isCleared = false;
	public ImageSet colourSet;
	public void Reset()
	{
		isActive = false;
		isCleared = false;
		image.sprite = colourSet.GetItems()[0];
	}
	public void TouchedSign()
	{	
		if (!isActive) {return;}
		positionOfDashMovement.SetValue(transform.position);
		isCleared = true;
		isActive = false;
		image.sprite = colourSet.GetItems()[2];

		clearedNumberOfSigns.ApplyChange(1);
		dashStart.Raise();
	}
	public void SetActive()
	{
		isActive = true;
		image.sprite = colourSet.GetItems()[1];

	}
	public bool GetIsActive()
	{
		return isActive;
	}
	public bool GetIsCleared()
	{
		return isCleared;
	}

    public void OnPointerClick(PointerEventData eventData)
    {
        TouchedSign();
    }
}
