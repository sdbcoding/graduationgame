﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemyDisabler : MonoBehaviour {

	public GameObjectVariable enemyToDisable;
	public ThingSet setToAddToEnemy;
	EnemyDisabler disableScript;
	Thing thingAddedToEnemy;
	
	public void DisableEnemy()
	{
		disableScript = enemyToDisable.Value.AddComponent<EnemyDisabler>();
		thingAddedToEnemy = enemyToDisable.Value.AddComponent<Thing>();
		thingAddedToEnemy.SetThingSet(setToAddToEnemy);
	}
	public void KillEnemy()
	{
		enemyToDisable.Value.SetActive(false);
	}
	public void ActivateEnemy()
	{
		if (disableScript != null)
		{
			disableScript.ActivateEnemy();
		}
		if (thingAddedToEnemy != null)
		{
			Destroy(thingAddedToEnemy);
			thingAddedToEnemy = null;
		}
	}
}
