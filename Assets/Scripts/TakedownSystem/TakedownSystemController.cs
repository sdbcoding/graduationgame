﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakedownSystemController : MonoBehaviour {

	public GameObjectSet touchedGameObjects;
	//public Vector3Variable positionOfTouch;
	//public GameEvent cancelTakedown;
	//Vector3 prevGround;

	void Start()
	{
		//prevGround = positionOfTouch.Value;
	}
	public bool WasPartOfTakeDown()
	{
		return CheckIfRuneWasPartOfTouch();
	}

	bool CheckIfRuneWasPartOfTouch()
	{
		for (int i = 0; i < touchedGameObjects.GetItems().Count; i++)
		{
			//Debug.Log(touchedGameObjects.Items[i].name);
			TakedownSign sign = touchedGameObjects.GetItems()[i].GetComponent<TakedownSign>();
			if (sign != null)
			{
				//PlayerClickedOn a sign
				//sign.TouchedSign();
				return SuccesfullTakedownTouch();
			}
			if (string.Equals(touchedGameObjects.GetItems()[i].name,"TakedownZone"))
			{
				//Debug.Log("TakedownZone clicked!");
				//click was within a takedown zone, and control should still be with TakedownSystem;
				return SuccesfullTakedownTouch();
			}
			
		}
		
		return false;
	}

	bool SuccesfullTakedownTouch()
	{
		//prevGround = positionOfTouch.Value;
		return true;
	}

}
