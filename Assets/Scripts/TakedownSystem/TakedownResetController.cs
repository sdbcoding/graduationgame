﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakedownResetController : MonoBehaviour {

	public GameEvent startDashing;
	public GameEvent killFinished;
	public GameEvent killCanceled;
    public GameEvent signDashStarted;

	bool isKilling = false;
	bool signHaveDashed = false;
	bool fatalityReady = false;
	public void KillingStarts()
	{
		isKilling = true;
	}
	public void KillingStops()
	{
		isKilling = false;
	}

	public void SignDashing()
	{
		signHaveDashed = true;
		startDashing.Raise();
        signDashStarted.Raise();


    }
	public void DashingStarts()
	{
		if (isKilling)
		{
			if (signHaveDashed)
			{
				signHaveDashed = false;
                
			} else if (fatalityReady) {
				fatalityReady = false;
				killFinished.Raise();
			} else {
				killCanceled.Raise();
			}
		}
	}
	public void FatalityInnitialised()
	{
		fatalityReady = true;
	}
}
