﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(NoiseMaker))]
public class TakedownNoiseMaker : OnUpdate {

	public FloatReference noiseDistance;
	public GameObjectVariable enemyToKill;
	public NoiseMaker noiseMaker;
	bool isMakingNoise = false;
	int noiseID;
	void Start()
	{
		if (noiseMaker == null)
		{
			noiseMaker = GetComponent<NoiseMaker>();
		}

	}
	public void StartNoise()
	{
		isMakingNoise = true;
		NoiseMaker.IncreaseNoiseID();
		noiseID = NoiseMaker.noiseId;
	}
	public void StopNoise()
	{
		isMakingNoise = false;
	}
	
	
	public override void UpdateEventRaised()
	{
		if (isMakingNoise)
		{
			MakeSomeNoise();
		}
	}
	void MakeSomeNoise()
	{
		noiseMaker.MakeNoise(enemyToKill.Value.transform.position,noiseDistance.Value,noiseID);
	}
}
