﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakedownPatternController : MonoBehaviour {

	public ThingSet signs;
	public FloatReference clearedNumberOfSigns;
	public FloatReference numberOfSigns;
	public GameEvent fatality;
	public void SignCleared()
	{
		
		if (clearedNumberOfSigns.Value == numberOfSigns.Value)
		{
			FatalityReady();
		}
		SetNextSignAsActive();
		
	}
	void SetNextSignAsActive()
	{
		//Debug.Log("Setting next sign");
		for (int i = 0; i < signs.GetItems().Count; i++)
		{
			TakedownSign sign = signs.GetItems()[i].GetComponent<TakedownSign>();
			if(sign == null)
			{
				Debug.Log("A sign misses its TakedownSign script");
				continue;
			}
			if (sign.GetIsCleared())
			{
				//Debug.Log("Sign is cleared");

				continue;
			}
			if (sign.GetIsActive())
			{
				//Debug.Log("Sign is Active");

				continue;
			}
			//Debug.Log("SettingItToActive");
			sign.SetActive();
			return;
		}

	}

	void FatalityReady()
	{
		fatality.Raise();
	}

}
