﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerOnlyCamera : MonoBehaviour {
	Camera playerCamera;

	// Use this for initialization
	void Awake () {
		playerCamera = GetComponent<Camera>();
		DisableCamera();
	}
	
	public void ActivateCamera()
	{
		playerCamera.enabled = true;
	}
	public void DisableCamera()
	{
		playerCamera.enabled = false;
	}
}
