﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class LoadModal : MonoBehaviour {

	public string question;
	
	public UnityEvent yesEvent;
	public UnityEvent noEvent;

	public void LaunchModal()
	{
		ModalPanel.Instance().Choice(question,YesAction,NoAction);
	}
	public void YesAction()
	{
		yesEvent.Invoke();
	}
	public void NoAction()
	{
		noEvent.Invoke();
	}
}
