﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeInController : MonoBehaviour {

    public void FadeIn()
    {
        gameObject.GetComponent<CanvasRenderer>().SetAlpha(0f);
        StartCoroutine(FadeCanvasGroup(0, 1, .5f));
    }

    public IEnumerator FadeCanvasGroup(float start, float end, float lerpTime = 1)
    {
        float _timeStartedLerping = Time.time;
        float timeSinceStarted = Time.time - _timeStartedLerping;
        float percentageComplete = timeSinceStarted / lerpTime;

        while (true)
        {
            timeSinceStarted = Time.time - _timeStartedLerping;
            percentageComplete = timeSinceStarted / lerpTime;

            float currentValue = Mathf.Lerp(start, end, percentageComplete);

            gameObject.GetComponent<CanvasRenderer>().SetAlpha(currentValue);

            if (percentageComplete >= 1) break;

            yield return new WaitForFixedUpdate();
        }

        print("done");
    }
}
