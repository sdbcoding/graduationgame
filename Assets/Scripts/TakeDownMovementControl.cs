﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDownMovementControl : MonoBehaviour
{

    Transform startMarker;
    Transform endMarker;
    public GameObjectVariable killableEnemy;
    public ThingSet player;
    Vector3 normStart;
    Vector3 normStartNegative;
    Vector3 normEnd;
    Vector3 normEndNegative;
    public float speed = 15.0F;
   public GameObject[] smokeBulletsArray;
    public bool isDashing = false;

    public GameObject smokeBulletPrefab;
    int amountOfSmokeBullets = 2;


    public void DashCheck()
    {

        if (isDashing)
        {
            BeginKillMove();
            isDashing = false;
        }
    }
    public void SetDashingTrue()
    {

        isDashing = true;
    }

   public  void BeginKillMove()
    {

        endMarker = killableEnemy.Value.transform;
        startMarker = player.GetItems()[0].transform;
        smokeBulletsArray = new GameObject[amountOfSmokeBullets];

        //vector between enemy and Horror, Cross it to find normal vector and normalize it. Add it to Horror start position to offset the hands
        normStart = startMarker.position+Vector3.Normalize(Vector3.Cross(startMarker.position - endMarker.position, Vector3.up));
        normStartNegative = startMarker.position - Vector3.Normalize(Vector3.Cross(startMarker.position - endMarker.position, Vector3.up));

        normEnd = endMarker.position +  Vector3.Normalize(Vector3.Cross(startMarker.position - endMarker.position, Vector3.up));
        normEndNegative = endMarker.position- Vector3.Normalize(Vector3.Cross(startMarker.position - endMarker.position, Vector3.up));
        
        for (int i = 0; i < amountOfSmokeBullets/2; i++)
        {
            smokeBulletsArray[i]= Instantiate(smokeBulletPrefab, transform.position, Quaternion.identity);
            smokeBulletsArray[i].GetComponent<TakeDownMovementInstance>().normStart = normStart;
            smokeBulletsArray[i].GetComponent<TakeDownMovementInstance>().normEnd = normEnd;
            smokeBulletsArray[i].GetComponent<TakeDownMovementInstance>().endMarker = endMarker;
            smokeBulletsArray[i].GetComponent<TakeDownMovementInstance>().armIndex = true;
            smokeBulletsArray[i].GetComponent<TakeDownMovementInstance>().speed= Random.Range(2,5);
        }

        for (int i = amountOfSmokeBullets / 2; i < amountOfSmokeBullets; i++)
        {
            smokeBulletsArray[i] = Instantiate(smokeBulletPrefab, transform.position, Quaternion.identity);
            smokeBulletsArray[i].GetComponent<TakeDownMovementInstance>().normStart = normStartNegative;
            smokeBulletsArray[i].GetComponent<TakeDownMovementInstance>().normEnd = normEndNegative;
            smokeBulletsArray[i].GetComponent<TakeDownMovementInstance>().endMarker = endMarker;
            smokeBulletsArray[i].GetComponent<TakeDownMovementInstance>().armIndex = false;
            smokeBulletsArray[i].GetComponent<TakeDownMovementInstance>().speed = Random.Range(2, 5);
        }
    }

   

  

}
