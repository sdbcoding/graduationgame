﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavigationController : MonoBehaviour {

	[SerializeField]
	GameObject player;
	NavMeshAgent agent;

	int i;
	Vector3 currentTarget;
	int pathLength;
	[SerializeField]
	Vector3Set playerPath;

	void Awake () 
	{
		agent = player.GetComponent<NavMeshAgent>();
		//path = drawController.path;
		i = 0;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		//currentTarget = playerPath.path[i];
		if (playerPath.path != null && playerPath.path.Count > 0)
		{
			Debug.Log("There is path");

			// If player reaches its destination (or gets close)
			if( Vector3.Distance(currentTarget, player.transform.position) < 1f && i < playerPath.path.Count )
			{
				Debug.Log("Target reached");
				i++;		

				// When end point of path reached, stop
				if( i == playerPath.path.Count )
				{
					agent.isStopped = true;
					//playerPath.path = new List<Vector3>();
					Debug.Log("Stop");
					return;
				}
					
				currentTarget = playerPath.path[i];
				agent.SetDestination(currentTarget);
				Debug.Log("Go to next");
			}
		}
	}

	public void OnPathUpdated()
	{
		Debug.Log("Path updated");
		i = 0;
		currentTarget = playerPath.path[i];
		agent.SetDestination(currentTarget);
		agent.isStopped = false;
	}
}
