﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DrawInputController : MonoBehaviour {

	[SerializeField]
	GameObject player;
	[SerializeField]
	int maxPathSize = 100;
    int layer_mask;
	NavMeshAgent agent;
	LineRenderer lr;
	[SerializeField]
	Vector3Set playerPath;
	[SerializeField]
	GameEvent OnPathChanged;
	bool insideSweetSpot;

    void Awake()
    {
        layer_mask = LayerMask.GetMask("Ground");
		agent = player.GetComponent<NavMeshAgent>();
		lr = GetComponent<LineRenderer>();
		insideSweetSpot = false;
		//path = agent.path.corners;
    }
    void FixedUpdate()
    {
		if (playerPath.path != null && playerPath.path.Count > 0)
		{
			//Debug.Log("There is path");
			lr.positionCount = playerPath.path.Count;
			for (int i = 0; i < playerPath.path.Count; i++)
			{
//                Debug.Log(path[i]);
				lr.SetPosition(i, playerPath.path[i]);
			}
		}

        if ( Input.touchCount > 0 )
        {
			//Debug.Log("Touch detected");
			if( Input.GetTouch(0).phase == TouchPhase.Began )
			{
				Debug.Log("Reset");
				playerPath.path = new List<Vector3>();
				RaycastHit hit;            
				if ( Physics.Raycast(Camera.main.ScreenPointToRay(Input.GetTouch(0).position), out hit, 100, layer_mask) ) 
				{
					Debug.Log("Touch Began");
					if( Vector3.Distance( hit.point, player.transform.position) < 1f )
					{
						Debug.Log("Inside sweet spot");
						insideSweetSpot = true;
						playerPath.path.Add(hit.point);
					}
					else
					{
						Debug.Log("Outside sweet spot");
						insideSweetSpot = false;
					}		
				}    
			}
			else if( Input.GetTouch(0).phase == TouchPhase.Moved && insideSweetSpot )
			{
				RaycastHit hit;            
				if ( Physics.Raycast(Camera.main.ScreenPointToRay(Input.GetTouch(0).position), out hit, 100, layer_mask) ) 
				{
					//Debug.Log(hit.point);
					if( playerPath.path.Count < maxPathSize )
					{
						playerPath.path.Add(hit.point);
						//agent.SetDestination(hit.point);
					}
						
				}     
			}
			else if( Input.GetTouch(0).phase == TouchPhase.Ended && insideSweetSpot )    
			{
				Debug.Log("Player path size: " + playerPath.path);
				OnPathChanged.Raise();
			}
        }

		// Click events, not used now
        /*if ( Input.GetMouseButtonDown(0) ) 
   		{
            //Debug.Log("Click detected");

            RaycastHit hit;            
            if ( Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100, layer_mask) ) 
            {
                //Debug.Log(hit.point);
                playerPath.path.Add(hit.point);
            }            
        }*/
    }
}
