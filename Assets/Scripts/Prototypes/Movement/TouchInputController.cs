﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchInputController : MonoBehaviour {

	[SerializeField]
	GameObject player;
    public float speed = 0.1F;
    public bool movementEnabled = false;
    Vector3 inputPosition;
    int layer_mask;

    void Awake()
    {
        layer_mask = LayerMask.GetMask("Ground");
    }
    void Update()
    {
        if ( Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved )
        {
            Debug.Log("Movement detected");
            // Get movement of the finger since last frame

            RaycastHit hit;
            
            if ( Physics.Raycast(Camera.main.ScreenPointToRay(Input.GetTouch(0).position), out hit, 100, layer_mask) ) 
            {
                Debug.Log(hit.point);
                inputPosition = hit.point;
            }        
            movementEnabled = true;
        }

        if ( Input.GetMouseButtonDown(0) ) 
    {
            Debug.Log("Movement detected");
            RaycastHit hit;
            
            if ( Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100, layer_mask) ) 
            {
                Debug.Log(hit.point);
                inputPosition = hit.point;
            }            
            movementEnabled = true;
        }

        if ( movementEnabled )
        {
            Debug.Log("Moving");
             // Move object across XY plane
            player.transform.position = Vector3.MoveTowards( player.transform.position, inputPosition, speed);

            if ( player.transform.position.x == inputPosition.x && player.transform.position.z == inputPosition.z )
            {
                Debug.Log("Stop");
                movementEnabled = false;
            }
             
        }
        
    }
}