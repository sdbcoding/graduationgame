﻿using UnityEngine;
using System.Collections;

public class CompleteCameraController : MonoBehaviour 
{    
    [Range(0,100)]
    public float followSpeed = 99;
    public float yValue = 22;

    public ThingSet playerSet;       //Public variable to store a reference to the player game object
    public Vector3Variable offset;         //Private variable to store the offset distance between the player and camera
    public GameObjectVariable enemyToKill;
    Transform playerTransform;
    Transform transforfollowing;
    bool isFollowingPlayer = false;
    public Vector3Variable overviewPosition;

    // Use this for initialization
    void Start () 
    {
        //Calculate and store the offset value by getting the distance between the player's position and camera's position.
        //offset = transform.position - player.transform.position;
        //Debug.Log(offset);
        playerTransform = playerSet.GetItems()[0].transform;
        transforfollowing = playerTransform;
        if (overviewPosition.Value != Vector3.zero)
            transform.position = overviewPosition.Value;
    }
    
    // LateUpdate is called after Update each frame
    void LateUpdate () 
    {
        if (!isFollowingPlayer) {return;}
        Vector3 newPosition = Vector3.Lerp(transform.position, transforfollowing.position + offset.Value, 1 - Mathf.Pow(1 - followSpeed * .01f, Time.deltaTime));
        float newYValue = transform.position.y;
        if (newYValue < yValue)
        {
            newYValue = transform.position.y + 0.1f;

        }
        transform.position = new Vector3(newPosition.x, newYValue, newPosition.z);
        // Set the position of the camera's transform to be the same as the player's, but offset by the calculated offset distance.
        //transform.position = player.transform.position + offset;
    }
    public void StartTakedown()
    {
        transforfollowing = enemyToKill.Value.transform;
    }
    public void StopTakedown()
    {
        transforfollowing = playerTransform;
    }
    public void PlayerSpawn()
    {
        isFollowingPlayer = true;
    }
}