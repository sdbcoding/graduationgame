﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchCameraController : MonoBehaviour 
{
	[SerializeField]
	LayerMask layer_mask;
	[SerializeField]
	float angularSpeed = 1f;
	[SerializeField]
	float sensibility = 0.001f;
	Vector2 origin;
	int touchInputCount = 0;
	Touch touchInput;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if ( Input.touchCount > touchInputCount )
        {
			touchInput = Input.GetTouch(touchInputCount); 

			if( touchInput.phase == TouchPhase.Began )
				origin = touchInput.position;
			else if ( touchInput.phase == TouchPhase.Moved )
			{
				if ( origin.x > touchInput.position.x )
					transform.Rotate( 0f, Vector2.Distance( origin, touchInput.position ) * sensibility * angularSpeed, 0f);
				else
					transform.Rotate( 0f, Vector2.Distance( origin, touchInput.position ) * sensibility * (-angularSpeed), 0f);
            }       
		}
	}

	public void SetDragLeftJoystick()
	{
		touchInputCount = 1;
	}

	public void SetReleaseLeftJoystick()
	{
		touchInputCount = 0;
	}
}
