﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSCameraController : MonoBehaviour 
{
    public Camera camera;
	public float angularSpeed = 10.0f;
	float pitch;
	float yaw;

    [SerializeField]
    RightJoystick rightJoystick;

    private Vector3 rightJoystickInput;

    // Use this for initialization
    void Start () 
    {
    }

    void FixedUpdate() 
    {
		//this will make the camera look "inwards" towards Pivot
        //camera.transform.LookAt(transform); 

        rightJoystickInput = rightJoystick.GetInputDirection();  

        // If there is input from the right joystick
        if ( rightJoystickInput != Vector3.zero )
        {
			pitch = -rightJoystickInput.x; // The horizontal movement from joystick 02
			yaw = rightJoystickInput.y; // The vertical movement from joystick 02

			transform.Rotate((Vector3.down * pitch) * angularSpeed);
			//transform.Rotate((Vector3.left * yaw) * angularSpeed);
        }
    }
}
