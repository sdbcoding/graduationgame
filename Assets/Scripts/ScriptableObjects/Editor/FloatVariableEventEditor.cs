﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FloatVariable))]

public class FloatVariableEventEditor : Editor {

	public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUI.enabled = Application.isPlaying;
        
        FloatVariable f = target as FloatVariable;
        if (GUILayout.Button("Raise"))
            f.Raise();
    }
}
