﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="Boolean Set", menuName = "Sets/Boolean Set", order = 3)]


public class BooleanSet : RunTimeSets<bool> {
	
	
	public bool Reset;
	public bool startValue;
	
	public void OnEnable()
	{
		
		if (Reset)
		{
			
			for (int i = 0; i < Items.Count; i++)
			{
				Items[i] = startValue;
			}
		}
	} 

	
	
	public void ChangeBool(int playerNumber)
	{
		Items[playerNumber] = !Items[playerNumber];
		Raise();
	}
	
}
