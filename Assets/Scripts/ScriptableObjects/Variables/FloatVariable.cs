﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
[CreateAssetMenu(fileName ="Variables", menuName = "Variables/Float Variable", order = 4)]
public class FloatVariable : GameEvent 
{    
    [SerializeField]
    private float Value;

    public void SetValue(float value)
    {
        Value = value;
        Raise();
    }

    public void SetValue(FloatVariable value)
    {
        Value = value.Value;
        Raise();

    }

    public float GetValue()
    {
        return Value;
    }

    public void ApplyChange(float amount)
    {
        Value += amount;
        Raise();
    }

    public void ApplyChange(FloatVariable amount)
    {
        Value += amount.Value;
        Raise();
    }

}
