﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName ="Variables", menuName = "Variables/String Variable", order = 1)]

public class StringVariable : GameEvent {

	public string Value;

    public void SetValue(string value)
    {
        Value = value;
        Raise();

    }

    public void SetValue(StringVariable value)
    {
        Value = value.Value;
        Raise();

    }

    public void ApplyChange(string amount)
    {
        Value += amount;
        Raise();
    }

    public void ApplyChange(StringVariable amount)
    {
        Value += amount.Value;
        Raise();
    }
}
