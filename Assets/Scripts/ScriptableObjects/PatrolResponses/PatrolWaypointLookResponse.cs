﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class PatrolWaypointLookResponse : ScriptableObject {

	public float waitTimeBeforeLook = 1;
	public float[] lookAngles;
	public float[] timeToLookAtAngle;

	public float[] waitTimesBeforeNextLookAngle;
}
