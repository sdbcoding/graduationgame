﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDownMovementInstance : MonoBehaviour
{

    public Transform endMarker;
    public Vector3 normStart;
    public Vector3 normEnd;
    public float speed = 15.0F;
    private float startTime;
    float journeyLength;
    public float fracJourney;
    public bool armIndex;// true=positive false=negative

    void Start()
    {
        startTime = Time.time;
        journeyLength = Vector3.Distance(normStart, normEnd);
    }

 
    void Update()
    {
        
        float distCovered = (Time.time - startTime) * speed;
         fracJourney = distCovered / journeyLength;
        if (fracJourney < 1)
        {
            transform.position = Vector3.Lerp(normStart, normEnd, fracJourney);
        }
        else
        {
            if (armIndex)
            {
                transform.RotateAround(endMarker.position, endMarker.up, -2);
                transform.Translate(0, 0.01f, 0);
            }
            else
            {
                transform.RotateAround(endMarker.position, endMarker.up, 2);
                transform.Translate(0, 0.01f, 0);

            }
        }
        
    }

    public void EndKillMove()
    {
        
            GetComponent<ParticleSystem>().Stop();
        
    }

    public void CancelKillMove()
    {

      
           Destroy(gameObject);

        
    }
}
