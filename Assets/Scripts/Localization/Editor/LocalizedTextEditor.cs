﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class LocalizedTextEditor : EditorWindow
{
    public EditorLocalizationData localizationData;
    Vector2 scrollPos;

    [MenuItem ("Window/Localized Text Editor")]
    static void Init()
    {
        EditorWindow.GetWindow (typeof(LocalizedTextEditor)).Show ();
    }

    private void OnGUI()
    {
        EditorGUILayout.BeginVertical();
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Width(position.width), GUILayout.Height(position.height));
        if (localizationData != null) 
        {
            SerializedObject serializedObject = new SerializedObject (this);
            SerializedProperty serializedProperty = serializedObject.FindProperty ("localizationData");
            EditorGUILayout.PropertyField (serializedProperty, true);
            serializedObject.ApplyModifiedProperties ();

            if (GUILayout.Button ("Save data")) 
            {
                SaveGameData ();
            }
        } 
         
        if (GUILayout.Button ("Load data")) 
        {
            LoadGameData ();
        }
        /*
        if (GUILayout.Button ("Create new data")) 
        {
            CreateNewData ();
        }
        */
        EditorGUILayout.EndScrollView ();
        EditorGUILayout.EndVertical();
    }

    private void LoadGameData()
    {
        Object[] obj = Resources.LoadAll("LocalizationData");
        //Debug.Log(obj[0].name);
        TextAsset[] assets = new TextAsset[obj.Length];
        for (int i = 0; i < obj.Length; i++)
        {
            //Debug.Log(obj[i]);
            assets[i] = obj[i] as TextAsset;
        }
        //TextAsset[] assets = Resources.LoadAll("LocatizalitonData");
        //Debug.Log(assets.Length);
        //string filepath = ((TextAsset)Resources.Load("LocalizationData")).ToString();
        LocalizationData[] unCombined = new LocalizationData[assets.Length];

        for (int i = 0; i < unCombined.Length; i++)
        {
            unCombined[i] = JsonUtility.FromJson<LocalizationData> (assets[i].ToString());
        }
        if (unCombined.Length == 0) {return;}
        
        //Setting up the keys
        EditorLocalizationData templocalizationData = new EditorLocalizationData();
        //Debug.Log(unCombined[0].items.Length);
        templocalizationData.items = new EditorLocalizationItem[unCombined[0].items.Length];
        for (int i = 0; i < unCombined[0].items.Length; i++)
        {
            EditorLocalizationItem tempItems = new EditorLocalizationItem();
            tempItems.key = "";
            tempItems.values = new string[unCombined.Length];
            //Debug.Log(tempItems.key);
            //for (int i = 0; i < unCombined[0].items.Length; i++)
            {
                templocalizationData.items[i] = tempItems;
            }
            
        }
        
        
        //Debug.Log(localizationData.items[0]);
        for (int i = 0; i < templocalizationData.items.Length; i++)
        {
            
            templocalizationData.items[i].key = unCombined[0].items[i].key;
            //localizationData.items[i].values = new string[unCombined.Length];

            for (int j = 0; j < unCombined.Length; j++)
            {
                templocalizationData.items[i].values[j] = unCombined[j].items[i].value;
            }
            //Debug.Log(templocalizationData.items[i].key);
        }
        for (int i = 0; i < templocalizationData.items.Length; i++)
        {
            //Debug.Log(templocalizationData.items[i].key);
            
        }
        localizationData = templocalizationData;

        /*

            if (!string.IsNullOrEmpty (filepath)) 
            {
                //string dataAsJson = File.ReadAllText (filepath);

                //Debug.Log(dataAsJson);
                localizationData = JsonUtility.FromJson<LocalizationData> (filepath);
            }
             */
    }

    private void SaveGameData()
    {
        Object[] obj = Resources.LoadAll("LocalizationData");
        for (int i = 0; i < obj.Length; i++)
        {
            string filePath = "Assets/Resources/LocalizationData/" +obj[i].name +".json";
            LocalizationData tempData = new LocalizationData();
            tempData.items = new LocalizationItem[localizationData.items.Length];
            for (int j = 0; j < tempData.items.Length; j++)
            {
                LocalizationItem tempItems = new LocalizationItem();
                tempItems.key = "";
                tempItems.value = "";
                //Debug.Log(tempItems.key);
                //for (int i = 0; i < unCombined[0].items.Length; i++)
                tempData.items[j] = tempItems;
                tempData.items[j].key = localizationData.items[j].key;
                tempData.items[j].value = localizationData.items[j].values[i];
            }
            /* 
            for (int j = 0; j < tempData.items.Length; j++)
            {
                Debug.Log(tempData.items[j].key);
                Debug.Log(tempData.items[j].value);
            }
            */
            string dataAsJson = JsonUtility.ToJson(tempData);
            File.WriteAllText (filePath, dataAsJson);
        }

        /* 
        //string filePath = EditorUtility.SaveFilePanel ("Save localization data file", Application.streamingAssetsPath, "", "json");
        string filePath = "Assets/Resources/LocalizationData.json";

        if (!string.IsNullOrEmpty(filePath))
        {
            string dataAsJson = JsonUtility.ToJson(localizationData);
            File.WriteAllText (filePath, dataAsJson);
            LoadGameData();
        }
        */
    }


}