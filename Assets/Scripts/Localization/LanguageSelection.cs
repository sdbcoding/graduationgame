﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class LanguageSelection : MonoBehaviour {
	public UnityEvent chosenLanguage;
	public Image english;
	public Image danish;
	
	void Start()
	{

		if (PlayerPrefs.HasKey("preferedLanguageFile"))
		{
			LanguageSelected(PlayerPrefs.GetString("preferedLanguageFile"));
			return;
		}
		
	}
	public void DanishLanguage()
	{
		
		LanguageSelected("Danish");
	}
	public void EnglishLanguage()
	{
		LanguageSelected("English");
	}
	void LanguageSelected(string language)
	{
        LocalizationController.Instance().LoadLocalizedText(language);
		chosenLanguage.Invoke();
	}

}
