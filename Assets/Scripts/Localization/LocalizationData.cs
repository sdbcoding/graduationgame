﻿[System.Serializable]
public class LocalizationData 
{
    public LocalizationItem[] items;
}
[System.Serializable]

public class EditorLocalizationData
{
    public EditorLocalizationItem[] items;
}

[System.Serializable]
public class LocalizationItem
{
    public string key;
    public string value;
}
[System.Serializable]
public class EditorLocalizationItem
{
    public string key;
    public string[] values;


}