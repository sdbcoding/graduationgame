﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour {

    public string key;
    

    // Use this for initialization
    void Start () 
    {
        UpdateText();
        LocalizationController.Instance().SubscribeAsActiveTextObject(this);
    }

    public void UpdateText()
    {
        Text text = GetComponent<Text> ();
        text.text = LocalizationController.Instance().GetLocalizedValue (key);
    }

    void OnDestroy()
    {
        if (LocalizationController.IsInstanceCreated())
        {
            LocalizationController.Instance().UnSubscribeAsActiveTextObject(this);
        }
    }
}