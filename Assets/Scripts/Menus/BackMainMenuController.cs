﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackMainMenuController : MonoBehaviour 
{
	public GameObject backButton;
    public GameEvent goBackToMainMenu;
    void Start()
    {
        if (!ShowMainMenuStaticClass.showMainMenu)
        {
            backButton.SetActive(true);
        }
    }

    public void EscapePressedHandler()
    {
        goBackToMainMenu.Raise();
    }

    public void enableBackButton()
	{
		backButton.SetActive(true);
        ShowMainMenuStaticClass.showMainMenu = true;

    }
	public void disableBackButton()
	{
        //goBackToMainMenu.Raise();
        backButton.SetActive(false);
	}
    

}
