﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseController : MonoBehaviour {

    public GameEvent pauseUpdateLoop;
    public GameEvent startUpdateLoop;
    public GameObject fadedBackground;

    public SceneSet sceneSet;
    public SceneSetVariable sceneSetLoader;
    public LoadSceneFromScript sceneLoader;

    public void OnEscapeButtonPressed()
    {
        if (fadedBackground.activeInHierarchy)
        {
            LoadLevel();
        }
        else
        {
            pauseUpdateLoop.Raise();
            fadedBackground.SetActive(true);
        }
    }

    public void OnPauseButtonClicked()
    {
        if (fadedBackground.activeInHierarchy)
        {
            startUpdateLoop.Raise();
            fadedBackground.SetActive(false);
        }
        else
        {
            pauseUpdateLoop.Raise();
            fadedBackground.SetActive(true);
        }
    }

    public void LoadLevel()
    {
        ShowMainMenuStaticClass.showMainMenu = false;
        sceneSetLoader.SetValue(sceneSet);
        sceneLoader.LoadScene();
    }
}
