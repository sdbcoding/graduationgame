﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsControllerSound : MonoBehaviour {

	public Slider volumeSliderMusic;
    public Toggle toggleMusic;
    public Slider volumeSliderSFX;
    public Toggle toggleSFX;

    public int musicVolume;
    public int sfxVolume;

	// Use this for initialization
	void Start () {
		volumeSliderMusic.value = PlayerPrefs.GetInt("MusicVolume", musicVolume) ;
        volumeSliderSFX.value = PlayerPrefs.GetInt("SFXVolume", sfxVolume);
        toggleMusic.isOn = (PlayerPrefs.GetInt("MusicToggle", 0) != 1) ;
        toggleSFX.isOn = (PlayerPrefs.GetInt("SFXToggle", 0) != 1);
	}
	
	public void ChangeMusicValue()
    {
        PlayerPrefs.SetInt("MusicVolume", (int)volumeSliderMusic.value);
        if ((PlayerPrefs.GetInt("MusicToggle")!=1))
        {
           
            AkSoundEngine.SetRTPCValue("RTPCMusicVolume", volumeSliderMusic.value);
        }
    }

    public void SwitchMusic()
    {

        if (toggleMusic.isOn)
        {
            
            AkSoundEngine.SetRTPCValue("RTPCMusicVolume", volumeSliderMusic.value);
            //Debug.Log("Music muted");
            PlayerPrefs.SetInt("MusicToggle",0);
            //Debug.Log("It should be 0: "+PlayerPrefs.GetInt("MusicToggle"));
        }
        else
        {
            AkSoundEngine.SetRTPCValue("RTPCMusicVolume", 0);
            //Debug.Log("Music unmuted");
            PlayerPrefs.SetInt("MusicToggle", 1);
            //Debug.Log("It should be 1: "+PlayerPrefs.GetInt("MusicToggle"));
        }
    }

    public void ChangeSFXValue()
    {
        PlayerPrefs.SetInt("SFXVolume", (int)volumeSliderSFX.value);
        if ((PlayerPrefs.GetInt("SFXToggle") != 1))
        {
            
            AkSoundEngine.SetRTPCValue("RTPCSFXVolume", volumeSliderSFX.value);
        }        
    }

    public void SwitchSFX()
    {
        if (toggleSFX.isOn)
        {
            AkSoundEngine.SetRTPCValue("RTPCSFXVolume", volumeSliderSFX.value);
            //Debug.Log("SFX muted");
            PlayerPrefs.SetInt("SFXToggle", 0);
        }
        else 
        {
            AkSoundEngine.SetRTPCValue("RTPCSFXVolume", 0);
            //Debug.Log("SFX unmuted");
            PlayerPrefs.SetInt("SFXToggle", 1);
        }
    }

    public void ResetDefaultSound()
    {
        // Player preferences
		PlayerPrefs.SetInt("MusicVolume", musicVolume) ;
        PlayerPrefs.SetInt("SFXVolume", sfxVolume);
        PlayerPrefs.SetInt("MusicVolume", musicVolume);        
        PlayerPrefs.SetInt("MusicToggle", 0);
        PlayerPrefs.SetInt("SFXToggle", 0);
        
        // Wwise shit
        AkSoundEngine.SetRTPCValue("RTPCMusicVolume", sfxVolume);
        AkSoundEngine.SetRTPCValue("RTPCSFXVolume", sfxVolume);

        // Menu items
        volumeSliderMusic.value = musicVolume;
        volumeSliderSFX.value = sfxVolume;
        toggleMusic.isOn = true;
        toggleSFX.isOn = true;
    }
}
