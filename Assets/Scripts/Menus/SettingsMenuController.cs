﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsMenuController : MonoBehaviour {

	[SerializeField]
	GameObject settingsMenu;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void toggleSettings()
	{
		settingsMenu.SetActive(!settingsMenu.activeSelf);
	}

}
