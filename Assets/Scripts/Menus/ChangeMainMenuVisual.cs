﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMainMenuVisual : MonoBehaviour {

	public void ShowMainMenu()
    {
        ShowMainMenuStaticClass.showMainMenu = true;
    }

    public void DontShowMainMenu()
    {
        ShowMainMenuStaticClass.showMainMenu = false;
    }
}
