﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsControllerLanguage : MonoBehaviour 
{

	[SerializeField]
    Image danish;
    [SerializeField]
    Image english;

    public string preferedLanguage;
    
    /*[SerializeField]
    Object[] languageFiles;
    private string[] languageFilePath = new string[2];*/

    void Awake() 
    {
        /*for (int i = 0; i < languageFiles.Length; i++)
        {
            languageFilePath[i] = UnityEditor.AssetDatabase.GetAssetPath(languageFiles[i]);
            Debug.Log(languageFilePath[i]);
        }  */          
    }

    void Start()
    {
        if (PlayerPrefs.HasKey("preferedLanguageFile"))
        {
            if (PlayerPrefs.GetString("preferedLanguageFile") == "Danish")
            {
                ChangeLanguage("dk");
                return;
            }
        }
        ChangeLanguage("gb");
    }

    public void ChangeLanguage(string language)
    {
        if (language == "gb")
        {
            danish.color = Color.grey;
            english.color = Color.white;
            LocalizationController.Instance().LoadLocalizedText("English");
        } 
		else 
        {
            danish.color = Color.white;
            english.color = Color.grey;
            LocalizationController.Instance().LoadLocalizedText("Danish");
        }
        // AkSoundEngine.PostEvent( "play_click", gameObject);
    }

    public void ResetDefaultLanguage()
    {
        PlayerPrefs.GetString("preferedLanguageFile", preferedLanguage);
        if( preferedLanguage == "gb" )
        {
            LocalizationController.Instance().LoadLocalizedText("English");            
            danish.color = Color.grey;
            english.color = Color.white;
        }
        else
        {
            LocalizationController.Instance().LoadLocalizedText("Danish");                   
            danish.color = Color.white;
            english.color = Color.grey;
        }
    }
}
