﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainButtonsController : MonoBehaviour {

	public GameObject mainButtons;
    public GameObject mainMenuCanvas;
    public GameEvent playButtonPressed;

    void Start()
    {
        if (!ShowMainMenuStaticClass.showMainMenu)
        {
            mainMenuCanvas.SetActive(false);
        }
    }

    public void toggleMainButtons()
	{
		// Debug.Log("Hit");
		mainButtons.SetActive(!mainButtons.activeSelf);
	}

    public void PlayButtonWasPressed()
    {
        playButtonPressed.Raise();
        ShowMainMenuStaticClass.showMainMenu = false;
    }

}
