﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsScroll : MonoBehaviour {
	[SerializeField]
	GameObject creditsText;
	public float scrollingSpeed;
	private bool scrolling = false;
	void StartScroll()
	{
		// creditsText.transform.parent.gameObject.SetActive(true);
		Vector3 newStartingPosition = creditsText.transform.position;
		// newStartingPosition.x += creditsText.transform.x;
		newStartingPosition.y = 0;
		creditsText.transform.position = newStartingPosition;
		InvokeRepeating("Scrolling",Time.fixedDeltaTime,Time.fixedDeltaTime);
	}

	void Scrolling()
	{
		creditsText.transform.position += Vector3.up * scrollingSpeed;
	}

	void StopScrolling()
	{
		CancelInvoke("Scrolling");
		// creditsText.transform.parent.gameObject.SetActive(false);
		// AkSoundEngine.PostEvent("play_click_back", gameObject);
	}

	public void toggleScrolling()
	{
		// Debug.Log(scrolling);

		if( scrolling )
		{
			scrolling = false;
			StopScrolling();
		}
		else
		{
			scrolling = true;
			StartScroll();
		}
	}
}
