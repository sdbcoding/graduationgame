﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseChecker : MonoBehaviour {

	public GameEvent loseGame;
	public ThingSet enemies;
	public ThingSet pointOfInterest;
	int prevEnemyCount;
	public FloatReference numberOfCompletedPoints;
	public void EnemiesChange () {
		
        if (pointOfInterest.GetItems().Count - numberOfCompletedPoints.Value > enemies.GetItems().Count &&prevEnemyCount> enemies.GetItems().Count)
        {
            //Debug.Log("pointofInterest: " + pointOfInterest.GetItems().Count + " completed: " + numberOfCompletedPoints.Value  + " enemies: " + enemies.GetItems().Count);
            loseGame.Raise();
        }
        prevEnemyCount = enemies.GetItems().Count;
    }
}
