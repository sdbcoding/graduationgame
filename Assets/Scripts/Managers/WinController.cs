﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinController : MonoBehaviour {

	public StringVariable openScene;
    public SceneSet sceneSet;
    public SceneSetVariable sceneSetLoader;
    public LoadSceneFromScript sceneLoader;
    public GameObject winPanel;
    public GameEvent pauseGame;
    public GameEvent updateScore;
    public float delayBeforePanel = 1.0f;
        
    public void Win()
	{
        //Debug.Log(openScene.Value);
		PlayerPrefs.SetString("LastCompletedLevel",openScene.Value);

        AkSoundEngine.PostEvent("PCWin", gameObject);

        pauseGame.Raise();


        updateScore.Raise();
        Invoke("ShowPanel",delayBeforePanel);
    }
    

    void ShowPanel()
    {
        winPanel.SetActive(true);
    }

    public void LoadLevel()
    {
        sceneSetLoader.SetValue(sceneSet);
        sceneLoader.LoadScene();
    }
}
