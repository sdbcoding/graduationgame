﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PermenantSetContainer : MonoBehaviour {
	public StringSet[] stringSets;
	public BooleanSet[] booleanSets;
	public ColourSet[] colourSets;

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(gameObject);
	}
	
}
