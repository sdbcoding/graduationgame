﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {

#if UNITY_EDITOR
    public UnityEditor.SceneAsset[] scenesToIgnore;

    void OnValidate()
    {
        scenePaths = new string[scenesToIgnore.Length];
        for (int i = 0; i < scenesToIgnore.Length; i++)
        {
            scenePaths[i] = UnityEditor.AssetDatabase.GetAssetPath(scenesToIgnore[i]);
        }
    }
#endif
    public string[] scenePaths;
	public SceneSet[] sceneSets;
	SceneSet currentOpenScene;
	public SceneSetVariable sceneSetVariableToLoad;
	public StringVariable currenOpenSceneName;
	public LoadSceneSetFromEvent sceneLoader;
	// Use this for initialization
	void Start () {
		
		if (sceneSets == null)
		{
			Debug.Log("No scenesSets are added");
		}
		//Finding the scenepath to all open scenes
		string[] currenScenePaths = new string[SceneManager.sceneCount];
		for (int i = 0; i < currenScenePaths.Length; i++)
		{
			currenScenePaths[i] = SceneManager.GetSceneAt(i).path;
		}
		for (int i = 0; i < currenScenePaths.Length; i++)
		{
			//Checking if that scenepath is equal to any scenepath that is in the ignore scene array
			bool isNotIgnored = true;
			for (int j = 0; j < scenePaths.Length; j++)
			{
				if (Scene.Equals(scenePaths[j],currenScenePaths[i]))
				{
					isNotIgnored = false;
				}
			}
			if (!isNotIgnored) { continue; }
			//Checks through every sceneSet to see if the scenepath that passed the 
			for (int j = 0; j < sceneSets.Length; j++)
			{
				for (int k = 0; k < sceneSets[j].scenePaths.Length; k++)
				{
					if(string.Equals( sceneSets[j].scenePaths[k],currenScenePaths[i]))
					{
						//We got a match and this sceneset has the currently open scene;
						currentOpenScene = sceneSets[j];
						currenOpenSceneName.SetValue(SceneManager.GetSceneAt(i).name);
						sceneSetVariableToLoad.SetValue(currentOpenScene);
						return;
					}
					
				}
			}
		}
	}

	public void ResetLevel()
	{
		if (currentOpenScene == null)
		{
			Debug.Log("No matching sceneSet found");
			return;
		}
		//sceneLoader.scene = currentOpenScene;
		AkSoundEngine.PostEvent("StopEldritchPresence",gameObject);
		sceneLoader.LoadSceneSet();
	}
	
	
}
