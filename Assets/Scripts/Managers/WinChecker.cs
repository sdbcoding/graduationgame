﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinChecker : MonoBehaviour {
	
	public ThingSet enemies;
	public GameEvent winGame;
    public ThingSet pointOfInterest;
    public FloatVariable numberOfCompletedPoints;
    public FloatVariable score;
    public FloatReference enemiesSurviveScoreValue;
    
    void Start()
    {
        numberOfCompletedPoints.SetValue(0);
    }
	

    public void SetChange()
    {

        numberOfCompletedPoints.SetValue(0);
        for (int i = 0; i < pointOfInterest.GetItems().Count; i++)
        {
            if (pointOfInterest.GetItems()[i].GetComponent<TempPointOfInterest>().GetIsCompleted())
            {
                numberOfCompletedPoints.ApplyChange(1);
            }
           
        }
       
        if (numberOfCompletedPoints.GetValue() == pointOfInterest.GetItems().Count)
        {
            for (int i = 0; i < enemies.GetItems().Count; i++)
            {
                score.ApplyChange(enemiesSurviveScoreValue.Value);
            }
            winGame.Raise();
            
        }
        //Debug.Log(numberOfCompletedPoints.GetValue() );

    }
}
