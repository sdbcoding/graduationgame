﻿using UnityEngine;

public abstract class Manager<T> : MonoBehaviour where T : Manager<T>
{
    private static T instance { get; set; }
/*
    private void Awake()
    {
        Instance = Instance == null ? this as T : Instance;
        Debug.Log(this.GetType());
        if (Instance != this)
        {
            Destroy(this);
            return;
        }
        onAwake();
    }
     */
     public static bool IsInstanceCreated()
      {
          if (instance == null)
          {
              return false;
          }
          return true;
      }
    public static T Instance()
    {
        if (instance == null)
        {
            instance = FindObjectOfType(typeof (T)) as T;
            if (instance == null)
            {
                GameObject temp = new GameObject();
                temp.name = "ManagerObject";
                instance = temp.AddComponent<T>();
                DontDestroyOnLoad(temp);
            }

        }
        return instance;
    }

}
