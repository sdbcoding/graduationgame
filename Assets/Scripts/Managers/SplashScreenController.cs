﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SplashScreenController : MonoBehaviour {

	float currentTime;
	public UnityEvent[] events;
	public float[] timeEachEvenFires;
	bool[] haveFired;
	bool switchSound = true;
	void Awake()
	{
		if (timeEachEvenFires.Length != events.Length)
		{
			Debug.Log("Not all events have a time stamp");
			Destroy(this);
			return;
		}
		haveFired = new bool[timeEachEvenFires.Length];
	}
	
	// Update is called once per frame
	void Update () {
		currentTime += Time.deltaTime;
		for (int i = 0; i < timeEachEvenFires.Length; i++)
		{
			if (haveFired[i])
			{
				continue;
			}
			if (currentTime > timeEachEvenFires[i])
			{
				haveFired[i] = true;
				events[i].Invoke();
				if(i < timeEachEvenFires.Length -2)
				{
					// StartCoroutine(PlayNPCSound());
					AkSoundEngine.PostEvent("Thunder",gameObject);					
				}
			}
		}

	}

	IEnumerator PlayNPCSound()
	{
		// Debug.Log("Thunderstruck!");
		AkSoundEngine.PostEvent("Thunder",gameObject);
		yield return new WaitForSeconds(1f);

		switchSound = !switchSound;
		if( switchSound )
			AkSoundEngine.PostEvent("NPCScare",gameObject);
		else
			AkSoundEngine.PostEvent("NPCSearch",gameObject);
	}
}
