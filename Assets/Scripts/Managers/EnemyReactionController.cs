﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyReactionController : MonoBehaviour {

	public ThingSet enemies;
	public Reaction followPlayerReaction;
	public Reaction[] reactionsAdded;
	void Start()
	{
		// Debug.Log(reactionsAdded.Length);
	}
	public void PlayerHaveSpawned()
	{
		//Debug.Log("BeingCalled" + enemies.GetItems().Count);
		for (int i = 0; i < enemies.GetItems().Count; i++)
		{
			ReactionController reactionController = enemies.GetItems()[i].GetComponent<ReactionController>();
			if (reactionController != null)
			{
				reactionController.SetCurrentReation(followPlayerReaction);
				for (int j = 0; j < reactionsAdded.Length; j++)
				{
					reactionController.AddNewReaction(reactionsAdded[j]);
				}
			}
		}
	}
}
