﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LoseController : MonoBehaviour {

	// public UnityEvent response;
	public GameObject losePanel;
	public GameEvent pauseGame;
	public void LoseGame()
	{
		//response.Invoke();       
        AkSoundEngine.PostEvent("PCDeath", gameObject); 
		pauseGame.Raise();
		losePanel.SetActive(true);

	}
}
