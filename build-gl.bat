@echo off

REM unset errorlevel, as it's not a true environment variable
SET errorlevel=
REM Wait for unity to finish building, then if there was an error, treat it as the error for this script
start /wait "" "C:\Program Files\Unity\Hub\Editor\2018.2.6f1\Editor\Unity.exe" ^
-batchmode ^
-logfile "C:\Users\qotsafan1\Desktop\logfile.log" ^
-projectPath %CI_PROJECT_DIR% ^
-executeMethod Mindhunter.Pipeline.BuildAndroid ^
-mindhunterBuildPath Build\%CI_COMMIT_REF_NAME% ^
-mindhunterBranchName %CI_COMMIT_REF_NAME%
REM if errorlevel 1 is true when errorlevel >= 1, so will catch any (positive) error code
if errorlevel 1 (
    echo there was an error: Error code %errorlevel%
    exit /B %errorlevel%
)